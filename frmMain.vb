Imports Newtonsoft.Json
Imports System.Threading
Imports System.IO
Imports System.Collections.Specialized
Imports Microsoft.PointOfService

Public Class frmMain
    Dim thStorage As Thread
    Dim thDoCommands As Thread
    Dim thShowAdd As Thread

    Private Sub frmMain_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        If thStorage IsNot Nothing Then thStorage.Abort()
        If thDoCommands IsNot Nothing Then thDoCommands.Abort()
        If thShowAdd IsNot Nothing Then thShowAdd.Abort()
        Cursor.Show()
    End Sub



    Private Sub topBar()
        Dim swfFiles As String()
        Try
            If (Directory.Exists(Application.StartupPath & "\skins\logo") = False) Then
                Directory.CreateDirectory(Application.StartupPath & "\skins\logo")
            End If

            swfFiles = Directory.GetFiles(Application.StartupPath & "\skins\logo\", "*.swf")
        Catch ex As Exception
            swfFiles = New String() {}
        End Try
        If (swfFiles.Length > 0) Then
            Dim counter As Integer = 0

            While (True)
                Try
                    AxShockwaveFlash1.LoadMovie(0, swfFiles(counter))
                Catch ex As Exception

                End Try
                counter += 1
                If (counter = swfFiles.Length) Then
                    counter = 0
                End If
                Thread.Sleep(20000)
            End While
        End If
    End Sub

    Private Sub frmMain_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        thShowAdd = New Thread(AddressOf topBar)
        thShowAdd.Start() 'Запускаем поток для воспроизведения рекламы
        thShowAdd.Name = "showAdd"
        thShowAdd.IsBackground = True

        AxShockwaveFlash1.Height = 200
        AxShockwaveFlash1.Width = 1500

        repairBase()

        GetTerminalSettings()
        StatusMessage(0) = "Терминал №" & GlobalVars.terminal_id & ", Адрес: " & GlobalVars.TerminalAddress(0) & ", Call center: " & GlobalVars.callcenter
        StatusMessage(1) = "Terminal #" & GlobalVars.terminal_id & ", Address: " & GlobalVars.TerminalAddress(1) & ", Call center: " & GlobalVars.callcenter
        StatusMessage(2) = "Терминал №" & GlobalVars.terminal_id & ", Суроға: " & GlobalVars.TerminalAddress(2) & ", Call center: " & GlobalVars.callcenter

        SelectCodeMessage(0) = "Выберите код услуги"
        SelectCodeMessage(1) = "Select the service code"
        SelectCodeMessage(2) = "Кодро интихоб кунед"

        ccode.port = GlobalVars.com_n
        ccode.Reset()

        TerminalState()

        fillLastUsedServices()
        fillGroups()
        fillRightPanel()
        lblStatus.Text = StatusMessage(langInt)

        If File.Exists(Application.StartupPath & "/2.bat") Then Shell("""" & Application.StartupPath & "\2.bat""", AppWinStyle.MinimizedNoFocus)
        If GlobalVars.KillExplorer Then Shell("taskkill /F /IM explorer.exe", AppWinStyle.MinimizedFocus)
        SetStartUp()

        Dim sql As String = "UPDATE plateji SET status=0 WHERE status=-1"
        Execute(sql)

        thStorage = New Thread(AddressOf Storage)
        thStorage.Start() 'Запускаем поток для соединения с сервером
        thStorage.Name = "localStorageOperatrion"
        thStorage.IsBackground = True

        thDoCommands = New Thread(AddressOf DoCommands)
        thDoCommands.IsBackground = True
        thDoCommands.Name = "executeServerCommands"
        thDoCommands.Start()

        'Cursor.Hide()

    End Sub

    Private Sub SetStartUp()
        Dim regSave As Microsoft.Win32.RegistryKey
        regSave = Microsoft.Win32.Registry.LocalMachine.CreateSubKey("software\Microsoft\Windows\CurrentVersion\run\")
        Dim appPath As String = Application.StartupPath & "\terminal.exe"
        regSave.SetValue("TERMINAL", appPath)
        regSave.Close()
    End Sub

    Private Sub TerminalState()
        If (GlobalVars.stTerminal.Equals("303")) Then
            GlobalVars.Terminal = 303
            frmBlock.Show(Me)
        Else
            GlobalVars.Terminal = 301
        End If
    End Sub

    Private Sub repairBase()

        If Not File.Exists("lock") Then
            Try
                File.Create("lock")
                Execute("alter table opservice add column show_sec_num bool default false, add column sec_mask varchar(40);")
                Execute("delete from last_use_service")
                Execute("INSERT INTO last_use_service VALUES (5, 22, 'g'), (3, 20, 'g'), (4, 21, 'g'), (1, 3, 'g'), (2, 10, 'g'), (6, 14, 'g');")
            Catch ex As Exception
                msg(ex.ToString)
            End Try
        End If

        Dim dt As DataTable = GetTable("select concat('repair table ', table_name, ';') as comand from information_schema.tables where table_schema='terminal';")
        For i As Integer = 0 To dt.Rows.Count - 1
            Execute(dt.Rows(i).Item(0))
        Next
    End Sub
#Region "Threads"
    Sub Storage()
        Dim count As Int16 = 0
        Dim repl As String = "-1"
        Dim countReply As Integer = 1
        Dim countReplyBig As Integer = 0

        While (True)
            Try
                GlobalVars.lastStorageActiveTime = DateDiff("s", DateSerial(1970, 1, 1), Now())
                If Not GlobalVars.connected Then 'Нет соеденения с сервером необходима авторизация
                    msg("Соединение с сервером. Попытка №" & countReply & "...")
                    GlobalVars.connected = CL.Autorize()
                    If GlobalVars.connected Then
                        countReply = 1
                        countReplyBig = 0 'Otsledit
                        msg("Соединение с сервером установлено")
                        GlobalVars.doUnBlockLine = GlobalVars.tBlocked
                        get_status(True) 'отправляем статус терминала, если true соединение. Если false, то статус устройств
                    Else
                        countReply = countReply + 1
                        If countReply = 7 Then
                            If GlobalVars.BlockTerminal Then GlobalVars.doBlockLine = Not GlobalVars.tBlocked
                            countReplyBig = countReplyBig + 1
                            If countReplyBig = 10 Then
                                msg("Соединение с сервером НЕ УДАЛОСЬ. Количество попыток: " & countReply * countReplyBig & ". Ждем 10 минут...")
                                GlobalVars.lastStorageActiveTime = DateDiff("s", DateSerial(1970, 1, 1), DateAdd(DateInterval.Minute, 10, Now()))
                                countReplyBig = 1
                                countReply = 1
                                System.Threading.Thread.Sleep(600000) 'задержка на 10 минут
                            Else
                                msg("Соединение с сервером НЕ УДАЛОСЬ. Количество попыток: " & countReply * countReplyBig & ". Ждем 1 минуту...")
                                GlobalVars.lastStorageActiveTime = DateDiff("s", DateSerial(1970, 1, 1), DateAdd(DateInterval.Minute, 1, Now()))
                                countReply = 1
                                System.Threading.Thread.Sleep(60000) 'задержка на 1 минуту
                            End If
                        End If
                    End If
                Else
                    'соединение имеется отправляем данные
                    'синхронизация даты времени
                    If Not GlobalVars.syncDateTime Then
                        msg("Запрос времени...")
                        repl = CL.sSend("2", "")
                    End If
                    If (Not GlobalVars.getExistsServices) Then
                        msg("Формирование меню...")
                        getActiveServiceGroup()
                        fillGroups(True)
                        fillLastUsedServices(True)
                        msg("Меню сформированно")
                    End If
                    GetNewPlatej()
                    GetNewInkassation()
                    get_status(False) 'отправляем статус терминала, если true соединение. Если false, то статус устройств
                    count = count + 1
                    If count > 20 Then
                        count = 0
                        get_status(True)
                    End If
                    countReply = 1
                End If
                System.Threading.Thread.Sleep(1000) 'задержка на 1 секунд
                'msg("sleep")
            Catch ex As Exception
                msg("Error: 001  " & ex.Message & " " & ex.TargetSite.Name)
                GlobalVars.connected = False
                countReply = countReply + 1
            End Try
        End While
    End Sub

    Sub DoCommands()
        Dim act As Integer

        While (True)
            Try
                Dim rs As DataTable = GetTable("select * from commands WHERE status=0 Limit 1")
                If rs.Rows.Count > 0 Then
                    act = rs.Rows(0).Item("act")
                    GlobalVars.doCommand(0) = rs.Rows(0).Item("id_on_server")
                    GlobalVars.doCommand(1) = GlobalVars.doCommand(1) + 1
                    GlobalVars.doCommand(2) = rs.Rows(0).Item("id")
                    If GlobalVars.doCommand(1) < 10 Then
                        Select Case act
                            Case 0
                                GetNewPlatej()
                            Case 1

                            Case 2 ' Синхронизировать время 
                                CL.sSend("2", "")
                            Case 3 ' Заблокировать терминал
                                GlobalVars.doBlock = True
                            Case 4 ' Разблокировать терминал
                                GlobalVars.doUnBlock = True
                            Case 5
                                msg("Запрос групп операторов...")
                                CL.sSend("5", "")
                                GlobalVars.getExistsServices = False
                            Case 6
                                msg("Запрос списка операторов...")
                                CL.sSend("6", "")
                                GlobalVars.getExistsServices = False
                            Case 7
                                msg("Запрос тарифных планов...")
                                CL.sSend("7", "")
                            Case 8
                                GetNewInkassation()
                            Case 9
                                msg("Запрос настроек терминала...")
                                CL.sSend("9", "")
                            Case 10
                                msg("Запрос инкассаторов...")
                                CL.sSend("10", "")
                            Case 11
                                msg("Удаление всех комиссий на услуги...")
                                Dim sql As String = "DELETE FROM proc_tarifplan"
                                Execute(sql)
                                sql = "DELETE FROM proc_tarif"
                                Execute(sql)
                                sql = "DELETE FROM proc_tarifarr"
                                Execute(sql)
                                msg("Удалены все комиссии на услуги!!!!")
                            Case 12

                            Case 13
                                If GlobalVars.getCashCode Then
                                    Dim sql1 As String = "UPDATE commands SET status=1, date_execute=UNIX_TIMESTAMP() WHERE id_on_server=" & GlobalVars.doCommand(0).ToString()
                                    Execute(sql1)
                                    msg("Перезагрузка терминала...")
                                    GlobalVars.Terminal = 302
                                    Dim comm1 As New command
                                    comm1.setStatus(6)
                                    comm1.setId(GlobalVars.doCommand(0))
                                    Dim strSend As String = JsonConvert.SerializeObject(comm1)
                                    CL.sSend("12", strSend)
                                    Thread.Sleep(800)
                                    GlobalVars.doCommand(0) = 0
                                    GlobalVars.doCommand(1) = 0
                                    System.Diagnostics.Process.Start("cmd", "/c shutdown -r -f -t 0")
                                End If
                            Case 14
                                If GlobalVars.getCashCode Then
                                    Dim sql1 As String = "UPDATE commands SET status=1, date_execute=UNIX_TIMESTAMP() WHERE id_on_server=" & GlobalVars.doCommand(0).ToString()
                                    Execute(sql1)
                                    Dim comm1 As New command
                                    comm1.setStatus(6)
                                    comm1.setId(GlobalVars.doCommand(0))
                                    Dim strSend As String = JsonConvert.SerializeObject(comm1)
                                    CL.sSend("12", strSend)
                                    GlobalVars.doCommand(0) = 0
                                    GlobalVars.doCommand(1) = 0
                                    msg("Выключение терминала...")
                                    GlobalVars.Terminal = 302
                                    Thread.Sleep(800)
                                    System.Diagnostics.Process.Start("cmd", "/c shutdown -s -f -t 0")
                                End If
                            Case 15
                                'Отправка логов на сервер
                                Dim sql1 As String = "UPDATE commands SET status=2, date_execute=UNIX_TIMESTAMP() WHERE id=" & GlobalVars.doCommand(2)
                                Execute(sql1)
                                SendLog()
                            Case 16
                                'Выполнение BAT-ника и отправка результатов на сервер
                                Dim script As String = rs.Rows(0).Item("script")
                                'System.Diagnostics.Process.Start("cmd", "/c " & script & "> " & Application.StartupPath & "/Logs/" & "bat_" & Now.Year & Now.Month & Now.Day & Now.Hour & Now.Minute & Now.Second & ".txt")
                            Case 17
                                'Выполнение SQL-скрипта и отправка результатов на сервер
                                Dim script As String = rs.Rows(0).Item("script")
                            Case 18
                                'Выполнить обновление ПО. Запускается FTP клиент
                                Try
                                    msg("Запрос файла обновления...")
                                    Dim appPath As String = Application.StartupPath
                                    Dim archName As String = rs.Rows(0).Item("script")
                                    Dim slashPos As Integer = InStrRev(archName, "\")
                                    Dim ftpFolder As String = archName.Substring(0, slashPos - 1)
                                    archName = archName.Substring(slashPos)

                                    Dim upd As updateTerminal = New updateTerminal
                                    upd.setServerAddress("109.74.68.45")
                                    upd.setFtpUserName("user")
                                    upd.setFtpUserPass("123")
                                    upd.setFtpFileName(archName)
                                    upd.setDownloadTo(appPath + "\update\tmp\")

                                    If upd.DownloadArchive(ftpFolder) Then
                                        upd.setExtractTo(appPath + "\update\extract\")
                                        upd.ExtracrtArchive(upd.getdownloadTo + archName, upd.getExtractTo)
                                        upd.CreateBat(upd.getExtractTo, archName)
                                    End If
                                Catch ex As Exception
                                    msg("Ошибка при обновлени ПО терминала" & ex.ToString())
                                    Dim sql1 As String = "UPDATE commands SET status=9, date_execute=UNIX_TIMESTAMP() WHERE id_on_server=" & GlobalVars.doCommand(0).ToString()
                                    Execute(sql1)
                                    Dim comm1 As New command
                                    comm1.setStatus(9)
                                    comm1.setId(GlobalVars.doCommand(0))
                                    Dim strSend As String = JsonConvert.SerializeObject(comm1)
                                    CL.sSend("12", strSend)
                                    GlobalVars.doCommand(0) = 0
                                    GlobalVars.doCommand(1) = 0
                                End Try
                            Case 19
                                'Принудительный опрос статусов устройств терминала
                                Try
                                    _pos.Close()
                                Catch ex As Exception
                                    msg(ex.ToString())
                                End Try

                                Try
                                    _pos.Open()
                                Catch ex As Exception
                                    msg(ex.ToString())
                                End Try

                                ccode.GetState()

                                Dim sql1 As String = "UPDATE commands SET status=1, date_execute=UNIX_TIMESTAMP() WHERE id_on_server=" & GlobalVars.doCommand(0).ToString()
                                Execute(sql1)
                                Dim comm1 As New command
                                comm1.setStatus(6)
                                comm1.setId(GlobalVars.doCommand(0))
                                Dim strSend As String = JsonConvert.SerializeObject(comm1)
                                CL.sSend("12", strSend)
                                GlobalVars.doCommand(0) = 0
                                GlobalVars.doCommand(1) = 0
                        End Select
                    Else
                        msg("ERROR: Ошибка при выполнении команды полученной с сервера. Команда была выполнена " & GlobalVars.doCommand(1) & " раз. ACT=" & act & ", idCommand=" & GlobalVars.doCommand(0))
                        Dim sql1 As String = "UPDATE commands SET status=10, date_execute=UNIX_TIMESTAMP() WHERE id_on_server=" & GlobalVars.doCommand(0).ToString()
                        Execute(sql1)
                        Dim comm1 As New command
                        comm1.setStatus(10)
                        comm1.setId(GlobalVars.doCommand(0))
                        Dim strSend As String = JsonConvert.SerializeObject(comm1)
                        CL.sSend("12", strSend)
                    End If
                End If
                System.Threading.Thread.Sleep(1000) 'задержка на 1 секунд
            Catch ex As Exception
                msg("ERROR: Ошибка при выполнении комманды полученной с сервера. ACT=" & act & ", idCommand=" & GlobalVars.doCommand(0) & " " & ex.Message & " Ошибка возникла в: " & ex.TargetSite.Name)
            End Try
        End While
    End Sub
#End Region


    Private Sub SendLog()
        'Отправка логов на сервер
        Try
            Dim sFile As New SendFile

            Dim rs As DataTable = GetTable("select * from commands WHERE status=2 and  id=" & GlobalVars.doCommand(2) & " Limit 1")
            If rs.Rows(0).Item("date_from").ToString <> "" And rs.Rows(0).Item("date_to").ToString <> "" Then
                Dim date_from As Long = rs.Rows(0).Item("date_from")
                Dim date_to As Long = rs.Rows(0).Item("date_to")

                Dim copyFiles As String = ""

                For i As Long = date_from To date_to Step 86400
                    Dim d As DateTime = UnixTimestampToDateTime(i)
                    Dim LogFile As String = Application.StartupPath & "\Logs\" & d.Year & getCifr(d.Month.ToString, 2) & "\" & d.Year.ToString & getCifr(d.Month.ToString, 2) & getCifr(d.Day, 2) & ".txt"
                    If File.Exists(LogFile) Then
                        Dim tmpLogs As String = Application.StartupPath & "\tmpLogs\"
                        If Directory.Exists(tmpLogs) = False Then Directory.CreateDirectory(tmpLogs)
                        tmpLogs = tmpLogs & d.Year.ToString & getCifr(d.Month.ToString, 2) & getCifr(d.Day, 2) & ".txt"
                        copyFiles = copyFiles & vbCrLf & LogFile & " -> " & tmpLogs
                        File.Copy(LogFile, tmpLogs, True)
                    End If
                Next
                msg("Скопированы файлы для отправки на сервер: " & copyFiles)
                Dim zipFileToSend As String = Application.StartupPath & "\" & GlobalVars.doCommand(0) & ".7z"
                sFile.zip_folder(Application.StartupPath & "\tmpLogs", zipFileToSend, GlobalVars.agent_login)
                'msg("Создан архивный файл - " & zipFileToSend & ", для отправки на сервер.")
                Dim nvc As NameValueCollection = New NameValueCollection()
                nvc.Add("id", GlobalVars.doCommand(0))
                msg("Отправляем на сервер архивный файл логов - " & zipFileToSend & " размером(байт) " & FileLen(zipFileToSend))
                Dim send As String = sFile.HttpUploadFile(GlobalVars.server_file, zipFileToSend, "file", "image/jpeg", nvc, GlobalVars.agent_login, GlobalVars.agent_pass)

                If send = "OK" Then
                    'msg("На сервер отправлен архивный файл логов - " & Application.StartupPath & "\" & GlobalVars.doCommand(0) & ".7z")
                    Dim sql1 As String = "UPDATE commands SET status=1, date_execute=UNIX_TIMESTAMP() WHERE id_on_server=" & GlobalVars.doCommand(0).ToString()
                    Execute(sql1)
                    Dim comm1 As New command
                    comm1.setStatus(6)
                    comm1.setId(GlobalVars.doCommand(0))
                    Dim strSend As String = JsonConvert.SerializeObject(comm1)
                    CL.sSend("12", strSend)
                    GlobalVars.doCommand(0) = 0
                    GlobalVars.doCommand(1) = 0
                End If
                Dim a = Directory.GetFiles(Application.StartupPath & "/tmpLogs/")
                For Each f As String In a
                    File.Delete(f)
                Next
                Directory.Delete(Application.StartupPath & "/tmpLogs/")
                File.Delete(Application.StartupPath & "/" & nvc.Item("id") & ".7z")
            Else
                Dim sql1 As String = "UPDATE commands SET status=9, date_execute=UNIX_TIMESTAMP() WHERE id_on_server=" & GlobalVars.doCommand(0).ToString()
                Execute(sql1)
                Dim comm1 As New command
                comm1.setStatus(9)
                comm1.setId(GlobalVars.doCommand(0))
                Dim strSend As String = JsonConvert.SerializeObject(comm1)
                CL.sSend("12", strSend)
                GlobalVars.doCommand(0) = 0
                GlobalVars.doCommand(1) = 0
            End If
        Catch ex As Exception
            msg("Ошибка при отправке лог-файлов. " & ex.Message)
        End Try
    End Sub

    Public Sub get_status(ByVal link As Boolean)
        Dim str_send As String = ""
        Dim repl As String = "-1"
        'Gson gs= new Gson();
        If link Then
            Dim sm As stLink = New stLink()
            sm.setDate("0")
            sm.setLink("1")
            str_send = JsonConvert.SerializeObject(sm)
            If str_send <> "" Then repl = CL.sSend("1", str_send)

            If repl = -1 Then
                GlobalVars.connected = False
            End If
            msg("Связь с сервером:  " & repl)
            'If Not repl Then 'если ошибка при обработке ответа, то возвращает false
        Else
            Dim rs As DataTable = GetTable("select * from perif_status WHERE status=0 ORDER BY date_change ASC Limit 1")
            For i As Integer = 0 To rs.Rows.Count - 1
                Select Case rs.Rows(i).Item("perif")
                    Case "link"
                        Dim sm As stLink = New stLink()
                        sm.setDate("0")
                        sm.setLink("1")
                        str_send = JsonConvert.SerializeObject(sm)
                    Case "cash_code"
                        Dim sm As stCashCode = New stCashCode()
                        sm.setDate(rs.Rows(i).Item("date_change"))
                        sm.setCash_code(rs.Rows(i).Item("state"))
                        sm.setId(rs.Rows(i).Item("id"))
                        str_send = JsonConvert.SerializeObject(sm)
                    Case "printer"
                        Dim sm As stPrinter = New stPrinter()
                        sm.setDate(rs.Rows(i).Item("date_change"))
                        sm.setPrinter(rs.Rows(i).Item("state"))
                        sm.setId(rs.Rows(i).Item("id"))
                        str_send = JsonConvert.SerializeObject(sm)
                    Case "terminal"
                        Dim sm As stTerminal = New stTerminal()
                        sm.setDate(rs.Rows(i).Item("date_change"))
                        sm.setTerminal(rs.Rows(i).Item("state"))
                        sm.setId(rs.Rows(i).Item("id"))
                        str_send = JsonConvert.SerializeObject(sm)
                    Case "cash_count"
                        Dim sm As stCashCount = New stCashCount()
                        sm.setDate(rs.Rows(i).Item("date_change"))
                        sm.setCash_count(rs.Rows(i).Item("state"))
                        sm.setId(rs.Rows(i).Item("id"))
                        str_send = JsonConvert.SerializeObject(sm)
                    Case "cash_summ"
                        Dim sm As stCashSumm = New stCashSumm()
                        sm.setDate(rs.Rows(i).Item("date_change"))
                        sm.setCash_summ(rs.Rows(i).Item("state"))
                        sm.setId(rs.Rows(i).Item("id"))
                        str_send = JsonConvert.SerializeObject(sm)
                End Select

                If str_send <> "" Then
                    repl = CL.sSend("1", str_send)
                End If
            Next i
        End If

    End Sub

    Public Sub getActiveServiceGroup()
        GlobalVars.getExistsServices = True
        Dim sql As String = "select og.id,	count(os.id_operator) as col_s,	count(og1.id) as col_g from OpGroup og left outer join OpService os on (og.id=os.type) left outer join OpGroup og1 on (og.id=og1.parent) group by og.id"
        Dim rs As DataTable = GetTable(sql)

        Dim ids(,) As Integer
        ReDim ids(0 To 20, 0 To 2)
        For j As Integer = 0 To rs.Rows.Count - 1

            ids(j, 0) = rs.Rows(j).Item(0)
            ids(j, 1) = rs.Rows(j).Item(1)
            ids(j, 2) = rs.Rows(j).Item(2)
        Next j

        Dim enable As Int16 = 0
        Dim col As Integer = ids.GetUpperBound(0)
        For j As Integer = 0 To col
            enable = 0
            If (ids(j, 1) > 0) Then
                enable = 1
            ElseIf ids(j, 2) > 0 Then
                If getExistServices(ids(j, 0)) Then enable = 1
            Else
                enable = 0
            End If
            Execute("update OpGroup set enable=" & enable & " where id=" & ids(j, 0))
        Next j
    End Sub

    Public Function getExistServices(ByVal OpGroup As Integer) As Boolean
        Dim sql As String = "select count(*) as col from OpService WHERE type=" & OpGroup
        Dim rs As DataTable
        rs = GetTable(sql)
        For i As Integer = 0 To rs.Rows.Count - 1
            If rs.Rows(i).Item(0) > 0 Then Return True
        Next i

        sql = "select id from OpGroup where parent=" & OpGroup
        rs = GetTable(sql)
        For i As Integer = 0 To rs.Rows.Count - 1
            If getExistServices(rs.Rows(i).Item(0)) Then Return True
        Next i

        Return False
    End Function


    ''' <summary>
    ''' Посылает непроведенные платежи
    ''' оставшиеся в базе данных
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetNewPlatej()
        Dim rs As DataTable = GetTable("select * from plateji WHERE status=0 LIMIT 1")
        Dim pl As Platej = New Platej()
        Dim str_send As String = ""

        For i As Integer = 0 To rs.Rows.Count - 1
            pl.setVal1(rs.Rows(i).Item("val1"))
            pl.setVal3(rs.Rows(i).Item("val3"))
            pl.setVal5(rs.Rows(i).Item("val5"))
            pl.setVal10(rs.Rows(i).Item("val10"))
            pl.setVal20(rs.Rows(i).Item("val20"))
            pl.setVal50(rs.Rows(i).Item("val50"))
            pl.setVal100(rs.Rows(i).Item("val100"))
            pl.setVal200(rs.Rows(i).Item("val200"))
            pl.setVal500(rs.Rows(i).Item("val500"))
            pl.setId(CType(rs.Rows(i).Item("id").ToString(), Long))
            pl.setId_uslugi(CType(rs.Rows(i).Item("id_uslugi").ToString(), Long))
            pl.setNomer(rs.Rows(i).Item("nomer").ToString())
            pl.setSumma(CType(rs.Rows(i).Item("summa").ToString(), Double))
            pl.setSumma_zachis(CType(rs.Rows(i).Item("summa_zachis").ToString(), Double))
            pl.setStatus(CType(rs.Rows(i).Item("status").ToString(), Long))
            pl.setDate_create(rs.Rows(i).Item("date_create").ToString())
            pl.setId_inkas(rs.Rows(i).Item("id_inkass").ToString())
            pl.setHesh_id(rs.Rows(i).Item("hesh_id").ToString())
            pl.setChekn(rs.Rows(i).Item("chek").ToString())
            pl.setSumma_komissia(CType(rs.Rows(i).Item("summa_komissia").ToString(), Double))
            str_send = JsonConvert.SerializeObject(pl)
            If str_send <> "" Then CL.sSend("0", str_send) 'Отправка нового платежа на сервер
        Next i
    End Sub


    ''' <summary>
    ''' Отправляет на сервер все оставщиеся в очереди инкассаии
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub GetNewInkassation()
        Dim rs As DataTable = GetTable("select * from inkasso WHERE status=0")
        Dim inkas As MsgInkassation = New MsgInkassation()
        Dim str_send As String = ""
        For i As Integer = 0 To rs.Rows.Count - 1
            inkas.setUser(rs.Rows(i).Item("user").ToString())
            inkas.setPassw(rs.Rows(i).Item("passw").ToString())
            inkas.setDate_inkass(rs.Rows(i).Item("date_inkass").ToString())
            inkas.setSumma(CType(rs.Rows(i).Item("summa").ToString(), Double))
            inkas.setInkass_id(rs.Rows(i).Item("inkass_id").ToString())
            str_send = JsonConvert.SerializeObject(inkas)
            If str_send <> "" Then CL.sSend("8", str_send)
        Next i
    End Sub

#Region "Построение интерфейса"

    Private Sub fillGroups(Optional ByVal th As Boolean = False)
        If th Then
            tblServices.Invoke(Sub() tblServices.Controls.Clear())
        Else
            tblServices.Controls.Clear()
        End If
        Dim dt As DataTable = GetTable("SELECT * FROM OpGroup WHERE parent=0 order by enable desc, order_n")
        For i As Integer = 0 To dt.Rows.Count - 1
            Try
                Dim row As Integer = (i Mod 2) ' + 1
                Dim column As Integer = CInt(i \ 2) '+ 1
                Dim btn As New Button
                With btn
                    .AutoSize = True
                    .FlatStyle = FlatStyle.Flat
                    .FlatAppearance.BorderSize = 0
                    .FlatAppearance.MouseOverBackColor = Color.Transparent
                    .FlatAppearance.MouseDownBackColor = Color.Transparent
                    .Tag = dt.Rows(i).Item("id")
                    .BackColor = Color.Transparent
                    If dt.Rows(i).Item("enable").ToString = "1" Then
                        .Enabled = True
                        .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & .Tag & ".png")
                        AddHandler .Click, AddressOf Services_Click
                        AddHandler .MouseDown, AddressOf Services_MouseDown
                        AddHandler .MouseUp, AddressOf Services_MouseUp
                    Else
                        '.Enabled = False
                        .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & .Tag & ".png")
                    End If

                End With
                If th Then
                    tblServices.Invoke(Sub() tblServices.Controls.Add(btn, column, row))
                Else
                    tblServices.Controls.Add(btn, column, row)
                End If
            Catch ex As Exception
                msg("Ошибка добавления группы операторов (Главное меню): " & ex.Message)
            End Try
        Next
    End Sub

    Private Sub fillRightPanel()
        tblLang.Controls.Clear()
        For i As Integer = 0 To 2
            Dim btn As New Button
            With btn
                .AutoSize = True
                .FlatStyle = FlatStyle.Flat
                .FlatAppearance.BorderSize = 0
                .FlatAppearance.MouseOverBackColor = Color.Transparent
                .FlatAppearance.MouseDownBackColor = Color.Transparent
                .Tag = i
                .BackColor = Color.Transparent
                AddHandler .Click, AddressOf Lang_Click
                AddHandler .MouseDown, AddressOf Lang_MouseDown
                AddHandler .MouseUp, AddressOf Lang_MouseUp
            End With
            Try
                btn.Image = Image.FromFile(Application.StartupPath & "/skins/button_" & arrLang(i) & ".png")
            Catch ex As Exception
                msg("Ошибка добавления изображения к кнопке: " & ex.ToString & " Error occured in: " & ex.TargetSite.Name)
            End Try

            tblLang.Controls.Add(btn, 1, i + 1)
        Next
        Dim btn1 As New Button
        With btn1
            .AutoSize = True
            .FlatStyle = FlatStyle.Flat
            .FlatAppearance.BorderSize = 0
            .FlatAppearance.MouseOverBackColor = Color.Transparent
            .FlatAppearance.MouseDownBackColor = Color.Transparent
            .BackColor = Color.Transparent
            AddHandler .Click, AddressOf Help_Click
            AddHandler .MouseDown, AddressOf Help_MouseDown
            AddHandler .MouseUp, AddressOf Help_MouseUp
        End With

        Try
            btn1.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_help.png")
        Catch ex As Exception
            msg("Ошибка добавления изображения к кнопке: " & ex.ToString & " Error occured in: " & ex.TargetSite.Name)
        End Try
        tblLang.Controls.Add(btn1, 1, 4)

        Dim btn2 As New Button
        With btn2
            .AutoSize = True
            .FlatStyle = FlatStyle.Flat
            .FlatAppearance.BorderSize = 0
            .FlatAppearance.MouseOverBackColor = Color.Transparent
            .FlatAppearance.MouseDownBackColor = Color.Transparent
            .BackColor = Color.Transparent
            .Enabled = False
            AddHandler .Click, AddressOf About_Click
            AddHandler .MouseDown, AddressOf About_MouseDown
            AddHandler .MouseUp, AddressOf About_MouseUp
        End With
        Try
            btn2.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_about.png")
        Catch ex As Exception
            msg("Ошибка добавления изображения к кнопке: " & ex.ToString & " Error occured in: " & ex.TargetSite.Name)
        End Try
        tblLang.Controls.Add(btn2, 1, 5)
    End Sub

    Public Sub fillLastUsedServices(Optional ByVal th As Boolean = False)
        If th Then
            tblLastUsed.Invoke(Sub() tblLastUsed.Controls.Clear())
        Else
            tblLastUsed.Controls.Clear()
        End If
        Dim dt1 As DataTable = GetTable("select * from last_use_service order by nomer")

        For i As Integer = 0 To dt1.Rows.Count - 1
            Try
                Dim btn As New Button
                With btn
                    .AutoSize = True
                    .FlatStyle = FlatStyle.Flat
                    .FlatAppearance.BorderSize = 0
                    .FlatAppearance.MouseOverBackColor = Color.Transparent
                    .FlatAppearance.MouseDownBackColor = Color.Transparent
                    .Tag = dt1.Rows(i).Item("service").ToString() & "|" & dt1.Rows(i).Item("type").ToString()
                    If dt1.Rows(i).Item("type") = "g" Then 'Если type "g" то смотрим в group, если "s" то в opservice
                        .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & dt1.Rows(i).Item("service") & ".png")
                    Else
                        .Image = Image.FromFile(Application.StartupPath & "/skins/oper_logos/" & dt1.Rows(i).Item("service") & ".png")
                    End If

                    .BackColor = Color.Transparent
                    AddHandler .Click, AddressOf Last_Click
                    AddHandler .MouseDown, AddressOf Last_MouseDown
                    AddHandler .MouseUp, AddressOf Last_MouseUp
                End With

                If th Then
                    tblLastUsed.Invoke(Sub() tblLastUsed.Controls.Add(btn, i, 0))
                Else
                    tblLastUsed.Controls.Add(btn, i, 0)
                End If

            Catch ex As Exception
                msg("Ошибка добавления последнего использованного оператора: " & ex.Message)
            End Try
        Next
    End Sub

#End Region

    ''' <summary>
    ''' Methods to convert Unix time stamp to DateTime
    ''' </summary>
    ''' <param name="_UnixTimeStamp">Unix time stamp to convert</param>
    ''' <returns>Return DateTime</returns>
    Public Function UnixTimestampToDateTime(ByVal _UnixTimeStamp As Long) As DateTime
        Return (New DateTime(1970, 1, 1, 0, 0, 0)).AddSeconds(_UnixTimeStamp)
    End Function

    ''' <summary>
    ''' Methods to convert DateTime to Unix time stamp
    ''' </summary>
    ''' <param name="_DateTime">Unix time stamp to convert</param>
    ''' <returns>Return Unix time stamp as long type</returns>
    Public Function DateTimeToUnixTimestamp(ByVal _DateTime As DateTime) As Long
        Dim _UnixTimeSpan As TimeSpan = (_DateTime.Subtract(New DateTime(1970, 1, 1, 0, 0, 0)))
        Return CLng(Fix(_UnixTimeSpan.TotalSeconds))
    End Function
#Region "Процедуры"
    '''''''''''''''''''Last Used
    Private Sub Last_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        getTypeLastBtn(sender.Tag)

        If group_id <> 0 Then
            Dim dt As DataTable = GetTable("SELECT * FROM OpGroup WHERE id=" & group_id)
            If dt.Rows(0).Item("show_child") = "False" Then 'Если стоит галка показывать дочерние то 
                frmService.Show()
            Else
                frmGroup.Show(Me)
            End If
            group_name = dt.Rows(0).Item("name")
        Else
            'Выбран сервис
            Try
                frmService.Show(Me)
            Catch ex As Exception
                msg("Error: frmService.Show(Me)" & ex.ToString() & ". Error occured in: " & ex.TargetSite.Name)
                frmService.Visible = False
            End Try
        End If
    End Sub

    Private Sub Last_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim id As Integer = getTypeLastBtn(sender.tag)
            If sender.Tag.ToString().EndsWith("|g") Then
                sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & id & "_off.png")
            Else
                sender.Image = Image.FromFile(Application.StartupPath & "/skins/oper_logos/" & id & "_off.png")
            End If
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    Private Sub Last_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            Dim id As Integer = getTypeLastBtn(sender.tag)
            If sender.Tag.ToString().EndsWith("|g") Then
                sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & id & ".png")
            Else
                sender.Image = Image.FromFile(Application.StartupPath & "/skins/oper_logos/" & id & ".png")
            End If
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    '''''''''''''''''''Services
    Private Sub Services_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        group_id = sender.tag
        GlobalVars.getCashCode = False
        GlobalVars.Terminal = 305
        frmGroup.Show(Me)
    End Sub

    Private Sub Services_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & sender.Tag & "_off.png")
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    Private Sub Services_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & sender.Tag & ".png")
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    ''''''''''''''''''Languages
    Private Sub Lang_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        langInt = sender.tag
        fillGroups()
        fillRightPanel()
        lblStatus.Text = StatusMessage(langInt)
    End Sub

    Private Sub Lang_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/button_" & arrLang(sender.tag) & "_off.png")
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    Private Sub Lang_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/button_" & arrLang(sender.tag) & ".png")
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    ''''''''''''''''''Help
    Private Sub Help_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'MsgBox("Help")
    End Sub

    Private Sub Help_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_help_off.png")
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    Private Sub Help_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_help.png")
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    '''''''''''''''''''About
    Private Sub About_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        MsgBox("About")
    End Sub

    Private Sub About_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_about_off.png")
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    Private Sub About_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_about.png")
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

#End Region

    Private Sub Timer1_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        lblTime.Text = Date.Now
        If DateDiff("s", DateSerial(1970, 1, 1), Now()) - GlobalVars.lastStorageActiveTime > 60 Then
            Try
                GlobalVars.connected = False
                msg("Перезапуск потока по таймауту...")
                GlobalVars.lastStorageActiveTime = DateDiff("s", DateSerial(1970, 1, 1), DateAdd(DateInterval.Minute, 3, Now()))
                thStorage.Abort()
                thStorage = New Thread(AddressOf Storage)
                thStorage.Start()
                msg("Поток перезапущен по таймауту!")
            Catch ex As Exception
                'GlobalVars.connected = False
                msg("Error: 006 Не удалось перезапустить поток по таймауту!" & ex.Message & " Ошибка возникла в:" & ex.TargetSite.Name)
            End Try
        End If
        If GlobalVars.getCashCode Then
            If GlobalVars.doBlock Then
                GlobalVars.Terminal = 303
                Dim sql3 As String = "UPDATE commands SET status=1, date_execute=UNIX_TIMESTAMP() WHERE id_on_server=" & GlobalVars.doCommand(0)
                Execute(sql3)
                GlobalVars.doBlock = False
                GlobalVars.doCommand(0) = 0
                GlobalVars.doCommand(1) = 0
                frmBlock.Show(Me)
            End If

            If (GlobalVars.doUnBlock) Then
                GlobalVars.Terminal = 301
                Dim sql3 As String = "UPDATE commands SET status=1, date_execute=UNIX_TIMESTAMP() WHERE id_on_server=" & GlobalVars.doCommand(0)
                Execute(sql3)
                GlobalVars.doUnBlock = False
                GlobalVars.doCommand(0) = 0
                GlobalVars.doCommand(1) = 0
                frmBlock.Close()
            End If

            If GlobalVars.doBlockLine Then
                GlobalVars.doBlockLine = False
                GlobalVars.tBlocked = True
                msg("Терминал заблокирован из-за отсутствия связи с сервером!")
                frmBlock.Show(Me)
            End If

            If GlobalVars.doUnBlockLine And GlobalVars.Terminal <> 303 Then
                GlobalVars.doUnBlockLine = False
                GlobalVars.tBlocked = False
                msg("Терминал разблокирован!")
                frmBlock.Close()
            End If
        End If
    End Sub

    Private Sub lblStatus_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblStatus.Click
        'MsgBox(calc_tarif(21, 7))
    End Sub

    Private Function getTypeLastBtn(ByVal str As String)
        If str.EndsWith("|g") Then
            group_id = str.Substring(0, str.IndexOf("|g"))
            service_id = 0
            Return group_id
        ElseIf str.EndsWith("|s") Then
            service_id = str.Substring(0, str.IndexOf("|s"))
            group_id = 0
            Return service_id
        End If
    End Function

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Dim _dev As DeviceInfo
        posExplorer = New Microsoft.PointOfService.PosExplorer()
        Dim devices As DeviceCollection = New DeviceCollection()
        devices = posExplorer.GetDevices(DeviceCompatibilities.Opos)
        For Each di As DeviceInfo In devices
            If di.ServiceObjectName = "Custom POS Printer" Then
                _dev = di
                _pos = DirectCast(posExplorer.CreateInstance(_dev), PosCommon)
                HookUpEvents(_pos)
                Exit For
            End If
        Next

    End Sub

    Private Sub HookUpEvents(ByVal pc As PosCommon)
        If pc Is Nothing Then
            Return
        End If

        AddHandler pc.StatusUpdateEvent, AddressOf co_OnStatusUpdateEvent

        'Закрываем принтер и открываем заного
        Try
            _pos.Close()
        Catch ex As Exception
            msg(ex.ToString())
        End Try

        Try
            _pos.Open()
        Catch ex As Exception
            msg(ex.ToString())
        End Try
    End Sub

    Private Sub co_OnStatusUpdateEvent(ByVal source As Object, ByVal d As StatusUpdateEventArgs)
        Try
            If d.Status <> 12 Then GlobalVars.Printer = d.Status
        Catch ae As Exception
            msg(ae.ToString())
        End Try
    End Sub
End Class