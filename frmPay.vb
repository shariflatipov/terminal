﻿Imports Newtonsoft.Json
Imports System.Security.Cryptography
Imports System.Net
Imports System.Net.Security
Imports System.Security
Imports System.IO

Public Class frmPay
    Dim count As Integer = -1
    'Dim Massive(12) As PictureBox
    Dim cmdBack As New Button
    Dim cmdMain As New Button
    Dim cmdPay As New Button
    Dim OperMask As String
    Dim arrInfo(2) As String
    Dim arrLabel(2, 3) As String
    Public valuta As Integer() = {0, 0, 0, 0, 0, 0, 0, 0, 0}
    Dim ccStoped As Boolean = False

    Public tTimer As New System.Windows.Forms.Timer
    Public thGetInfo As New System.Threading.Thread(AddressOf getInfo)


    Private Sub getInfo()
        Try
            Dim needCheck As String = GetSingle("SELECT need_chek FROM OpService where id_operator = " + service_id.ToString())

            If Not needCheck.Equals("False") Then
                Dim info As New Info()
                info.setAct("1")
                info.setOpId(service_id)
                info.num = lblNomer.Text

                Dim [hash] As String = info.Prepare(GlobalVars.agent_login, "sy5NvO7RqR6FW3z")
                Dim md5Hash = MD5.Create()

                Dim mes As String = GetMd5Hash(md5Hash, hash)
                info.setHash(mes)

                Dim str As String = JsonConvert.SerializeObject(info)
                Dim result As String = str
                Label1.Invoke(Sub() Label1.Visible = True)

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
                ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf AcceptAllCertifications)

                Dim request As HttpWebRequest = _
                    CType(WebRequest.Create("https://" + GlobalVars.server_adr + ":7443/api/info/" + str), HttpWebRequest)

                Dim response As HttpWebResponse = _
                    CType(request.GetResponse(), HttpWebResponse)

                Dim dataStream As Stream = response.GetResponseStream()
                Dim reader As StreamReader = New StreamReader(dataStream)
                'Dim responseFromServer As String = reader.ReadToEnd()

                Dim jObject As Newtonsoft.Json.Linq.JObject = Newtonsoft.Json.Linq.JObject.Parse(reader.ReadToEnd)

                If jObject.Item("state").ToString().Equals("OK") Then
                    Label1.Invoke(Sub() Label1.Text = jObject.Item("msg").ToString())
                    'Console.WriteLine(responseFromServer)
                End If

                reader.Close()
                dataStream.Close()
                response.Close()

            End If

        Catch ex As Exception
            msg(ex.ToString())
        End Try
    End Sub

    Public Function AcceptAllCertifications(ByVal sender As Object, ByVal certification As Cryptography.X509Certificates.X509Certificate, ByVal chain As Cryptography.X509Certificates.X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) As Boolean
        Return True
    End Function


    Private Sub frmPay_Disposed(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Disposed
        If Not ccStoped Then ccode.StopPooling()
        'AxShockwaveFlash1.Stop()
        'AxShockwaveFlash1.Dispose()
    End Sub

    Private Sub frmPay_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            Label1.Visible = False
            thGetInfo.IsBackground = True
            thGetInfo.Start()

            pbLogo.BackgroundImage = Image.FromFile(Application.StartupPath & "/skins/logo.png")
            'tblTitle.BackgroundImage = Image.FromFile(Application.StartupPath & "/skins/images/caption.png")
            If group_id <> 0 Then
                pbService.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & group_id & ".png")
            Else
                pbService.Image = Image.FromFile(Application.StartupPath & "/skins/oper_logos/" & service_id & ".png")
            End If
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try

        arrInfo(0) = "Прежде чем подтвердить операцию убедитесь в правильно введенном номере и выбранном операторе"
        arrInfo(1) = "Прежде чем подтвердить операцию убедитесь в правильно введенном номере и выбранном операторе"
        arrInfo(2) = "Прежде чем подтвердить операцию убедитесь в правильно введенном номере и выбранном операторе"

        arrLabel(0, 0) = "Внесенная купюра"
        arrLabel(0, 1) = "Сумма"
        arrLabel(0, 2) = "Комиссия"
        arrLabel(0, 3) = "Сумма к зачислению"

        arrLabel(1, 0) = "Внесенная купюра"
        arrLabel(1, 1) = "Сумма"
        arrLabel(1, 2) = "Комиссия"
        arrLabel(1, 3) = "Сумма к зачислению"

        arrLabel(2, 0) = "Внесенная купюра"
        arrLabel(2, 1) = "Сумма"
        arrLabel(2, 2) = "Комиссия"
        arrLabel(2, 3) = "Сумма к зачислению"

        lblComission.Text = tarif_text(langInt, arrLang(langInt), service_id) & vbCrLf & vbCrLf & arrInfo(langInt)

        lbl0.Text = arrLabel(langInt, 0)
        lbl1.Text = arrLabel(langInt, 1)
        lbl2.Text = arrLabel(langInt, 2)
        lbl3.Text = arrLabel(langInt, 3)

        tblGo.Controls.Clear()

        With cmdBack
            .FlatAppearance.MouseOverBackColor = Color.Transparent
            .FlatAppearance.MouseDownBackColor = Color.Transparent
            .AutoSize = True
            .FlatStyle = FlatStyle.Flat
            .FlatAppearance.BorderSize = 0
            .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back.png")
            .Dock = DockStyle.Right
            .BackColor = Color.Transparent
            AddHandler .Click, AddressOf Back_Click
            AddHandler .MouseDown, AddressOf Back_MouseDown
            AddHandler .MouseUp, AddressOf Back_MouseUp
            AddHandler .MouseLeave, AddressOf Back_MouseLeave
        End With
        tblGo.Controls.Add(cmdBack, 0, 0)
        With cmdMain
            .FlatAppearance.MouseOverBackColor = Color.Transparent
            .FlatAppearance.MouseDownBackColor = Color.Transparent
            .AutoSize = True
            .FlatStyle = FlatStyle.Flat
            .FlatAppearance.BorderSize = 0
            .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
            .Dock = DockStyle.None
            .BackColor = Color.Transparent
            AddHandler .Click, AddressOf Main_Click
            AddHandler .MouseDown, AddressOf Main_MouseDown
            AddHandler .MouseUp, AddressOf Main_MouseUp
            AddHandler .MouseLeave, AddressOf Main_MouseLeave
        End With
        tblGo.Controls.Add(cmdMain, 1, 0)
        With cmdPay
            .FlatAppearance.MouseOverBackColor = Color.Transparent
            .FlatAppearance.MouseDownBackColor = Color.Transparent
            .AutoSize = True
            .FlatStyle = FlatStyle.Flat
            .FlatAppearance.BorderSize = 0
            .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_pay_dis.png")
            .Dock = DockStyle.Left
            .BackColor = Color.Transparent
            .Name = "cmdNext"
            .Enabled = False
            '.Enabled = True
            AddHandler .Click, AddressOf Pay_Click
            AddHandler .MouseDown, AddressOf Pay_MouseDown
            AddHandler .MouseUp, AddressOf Pay_MouseUp
            AddHandler .MouseLeave, AddressOf Pay_MouseLeave
        End With
        tblGo.Controls.Add(cmdPay, 2, 0)
        lblStatus.Text = StatusMessage(langInt)
        With ccode
            .service = service_id
            .lblInset = lblInsert
            .lblSum = lblSumma
            .lblComission = lblComissiya
            .lblZachislenie = lblZachislenie
            .Buttons(cmdBack, cmdMain, cmdPay)
            .nomer = lblNomer.Text
            .Pooling()
        End With
        With tTimer
            .Interval = 60000
            AddHandler .Tick, AddressOf tTimer_timer
            .Enabled = True
        End With
        'Dim pla As New ShockwaveFlash
        'pla.Movie = Application.StartupPath & "/skins/video_moneyin.swf"
        'pla.LoadMovie(0, Application.StartupPath & "/skins/video_moneyin.swf")
        'grbVideo.Controls.Add(pla)
        'pla.Play()
        AxShockwaveFlash1.Width = 230
        AxShockwaveFlash1.Stop()
        'Application.StartupPath & 
        AxShockwaveFlash1.Movie = Application.StartupPath & "\skins\video_moneyin.swf"
        AxShockwaveFlash1.Play()
    End Sub

    Private Sub tTimer_timer()
        'tTimer.
        If _isDetected Then
            _isDetected = False
            Exit Sub
        End If
        Dim s1 As Double = IIf(lblSumma.Text = "", 0, lblSumma.Text)
        If s1 > 0 Then
            pay()
        Else
            goMain()
        End If
    End Sub


#Region "Процедуры"


    Private Sub pay()
        ccode.StopPooling()
        Dim s1 As Double = lblSumma.Text
        Dim s2 As Double = lblComissiya.Text
        frmChek.s1 = s1
        frmChek.s2 = s2
        frmChek.PlatejID = ccode.PlatejID
        frmChek.chekN = ccode.chek
        frmChek.nomer = lblNomer.Text
        frmChek.ShowDialog(Me)
        tTimer.Stop()
        Try
            _pos.Close()
        Catch ex As Exception
            msg(ex.ToString())
        End Try

        Try
            _pos.Open()
        Catch ex As Exception
            msg(ex.ToString())
        End Try
    End Sub

    Private Sub goMain()
        ccode.StopPooling()
        ccStoped = True
        If frmGroup.Visible Then frmGroup.Close()
        frmService.Close()
        group_id = 0
        last_group_id = 0
        service_id = 0
        GlobalVars.getCashCode = True
        tTimer.Stop()
        GlobalVars.Terminal = 301
        Me.Close()
    End Sub

    '''''''''''''''''Back
    Private Sub Back_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ccode.StopPooling()
        ccStoped = True
        GlobalVars.Terminal = 306
        tTimer.Stop()
        Me.Close()
    End Sub

    Private Sub Back_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back_off.png")
    End Sub

    Private Sub Back_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back.png")
    End Sub

    Private Sub Back_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back.png")
    End Sub

    '''''''''''''''''''Main
    Private Sub Main_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        goMain()
    End Sub

    Private Sub Main_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main_off.png")
    End Sub

    Private Sub Main_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
    End Sub

    Private Sub Main_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
    End Sub

    ''''''''''''Pay
    Private Sub Pay_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        pay()
    End Sub

    Private Sub Pay_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_pay_off.png")
    End Sub

    Private Sub Pay_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_pay.png")
    End Sub

    Private Sub Pay_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_pay.png")
    End Sub

    Private Sub NumPad_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/images/pad_" & sender.Tag & "_off.png")
    End Sub

    Private Sub NumPad_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/images/pad_" & sender.Tag & ".png")
    End Sub

    Private Sub NumPad_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/images/pad_" & sender.Tag & ".png")
    End Sub

#End Region

End Class