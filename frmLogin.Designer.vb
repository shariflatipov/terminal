﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmLogin
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.grbKeyBoard = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtPass = New System.Windows.Forms.TextBox()
        Me.txtLogin = New System.Windows.Forms.TextBox()
        Me.btngz = New System.Windows.Forms.Button()
        Me.btnzu = New System.Windows.Forms.Button()
        Me.btn_lng = New System.Windows.Forms.Button()
        Me.btno = New System.Windows.Forms.Button()
        Me.btni = New System.Windows.Forms.Button()
        Me.btnu = New System.Windows.Forms.Button()
        Me.btny = New System.Windows.Forms.Button()
        Me.btnt = New System.Windows.Forms.Button()
        Me.btnr = New System.Windows.Forms.Button()
        Me.btne = New System.Windows.Forms.Button()
        Me.btnw = New System.Windows.Forms.Button()
        Me.btnspace = New System.Windows.Forms.Button()
        Me.btnnokta = New System.Windows.Forms.Button()
        Me.btn_c = New System.Windows.Forms.Button()
        Me.btnvirgul = New System.Windows.Forms.Button()
        Me.btnm = New System.Windows.Forms.Button()
        Me.btnn = New System.Windows.Forms.Button()
        Me.btnb = New System.Windows.Forms.Button()
        Me.btnv = New System.Windows.Forms.Button()
        Me.btnc = New System.Windows.Forms.Button()
        Me.btnx = New System.Windows.Forms.Button()
        Me.btnz = New System.Windows.Forms.Button()
        Me.btnsolshift = New System.Windows.Forms.Button()
        Me.btn_i = New System.Windows.Forms.Button()
        Me.btn_s = New System.Windows.Forms.Button()
        Me.btnl = New System.Windows.Forms.Button()
        Me.btnk = New System.Windows.Forms.Button()
        Me.btnj = New System.Windows.Forms.Button()
        Me.btnh = New System.Windows.Forms.Button()
        Me.btng = New System.Windows.Forms.Button()
        Me.btnf = New System.Windows.Forms.Button()
        Me.btnd = New System.Windows.Forms.Button()
        Me.btns = New System.Windows.Forms.Button()
        Me.btna = New System.Windows.Forms.Button()
        Me.btn_u = New System.Windows.Forms.Button()
        Me.btn_kh = New System.Windows.Forms.Button()
        Me.btnp = New System.Windows.Forms.Button()
        Me.btnq = New System.Windows.Forms.Button()
        Me.btnbackspace = New System.Windows.Forms.Button()
        Me.btn0 = New System.Windows.Forms.Button()
        Me.btn9 = New System.Windows.Forms.Button()
        Me.btn8 = New System.Windows.Forms.Button()
        Me.btn7 = New System.Windows.Forms.Button()
        Me.btn6 = New System.Windows.Forms.Button()
        Me.btn5 = New System.Windows.Forms.Button()
        Me.btn4 = New System.Windows.Forms.Button()
        Me.btn3 = New System.Windows.Forms.Button()
        Me.btn2 = New System.Windows.Forms.Button()
        Me.btn1 = New System.Windows.Forms.Button()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.LineShape1 = New Microsoft.VisualBasic.PowerPacks.LineShape()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.pbLogo = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.tblGo = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.grbKeyBoard.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 2000
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 3
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.99999!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 699.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.00001!))
        Me.TableLayoutPanel6.Controls.Add(Me.grbKeyBoard, 1, 0)
        Me.TableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(3, 53)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 3
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 400.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(1182, 388)
        Me.TableLayoutPanel6.TabIndex = 3
        '
        'grbKeyBoard
        '
        Me.grbKeyBoard.Controls.Add(Me.Label2)
        Me.grbKeyBoard.Controls.Add(Me.Label1)
        Me.grbKeyBoard.Controls.Add(Me.txtPass)
        Me.grbKeyBoard.Controls.Add(Me.txtLogin)
        Me.grbKeyBoard.Controls.Add(Me.btngz)
        Me.grbKeyBoard.Controls.Add(Me.btnzu)
        Me.grbKeyBoard.Controls.Add(Me.btn_lng)
        Me.grbKeyBoard.Controls.Add(Me.btno)
        Me.grbKeyBoard.Controls.Add(Me.btni)
        Me.grbKeyBoard.Controls.Add(Me.btnu)
        Me.grbKeyBoard.Controls.Add(Me.btny)
        Me.grbKeyBoard.Controls.Add(Me.btnt)
        Me.grbKeyBoard.Controls.Add(Me.btnr)
        Me.grbKeyBoard.Controls.Add(Me.btne)
        Me.grbKeyBoard.Controls.Add(Me.btnw)
        Me.grbKeyBoard.Controls.Add(Me.btnspace)
        Me.grbKeyBoard.Controls.Add(Me.btnnokta)
        Me.grbKeyBoard.Controls.Add(Me.btn_c)
        Me.grbKeyBoard.Controls.Add(Me.btnvirgul)
        Me.grbKeyBoard.Controls.Add(Me.btnm)
        Me.grbKeyBoard.Controls.Add(Me.btnn)
        Me.grbKeyBoard.Controls.Add(Me.btnb)
        Me.grbKeyBoard.Controls.Add(Me.btnv)
        Me.grbKeyBoard.Controls.Add(Me.btnc)
        Me.grbKeyBoard.Controls.Add(Me.btnx)
        Me.grbKeyBoard.Controls.Add(Me.btnz)
        Me.grbKeyBoard.Controls.Add(Me.btnsolshift)
        Me.grbKeyBoard.Controls.Add(Me.btn_i)
        Me.grbKeyBoard.Controls.Add(Me.btn_s)
        Me.grbKeyBoard.Controls.Add(Me.btnl)
        Me.grbKeyBoard.Controls.Add(Me.btnk)
        Me.grbKeyBoard.Controls.Add(Me.btnj)
        Me.grbKeyBoard.Controls.Add(Me.btnh)
        Me.grbKeyBoard.Controls.Add(Me.btng)
        Me.grbKeyBoard.Controls.Add(Me.btnf)
        Me.grbKeyBoard.Controls.Add(Me.btnd)
        Me.grbKeyBoard.Controls.Add(Me.btns)
        Me.grbKeyBoard.Controls.Add(Me.btna)
        Me.grbKeyBoard.Controls.Add(Me.btn_u)
        Me.grbKeyBoard.Controls.Add(Me.btn_kh)
        Me.grbKeyBoard.Controls.Add(Me.btnp)
        Me.grbKeyBoard.Controls.Add(Me.btnq)
        Me.grbKeyBoard.Controls.Add(Me.btnbackspace)
        Me.grbKeyBoard.Controls.Add(Me.btn0)
        Me.grbKeyBoard.Controls.Add(Me.btn9)
        Me.grbKeyBoard.Controls.Add(Me.btn8)
        Me.grbKeyBoard.Controls.Add(Me.btn7)
        Me.grbKeyBoard.Controls.Add(Me.btn6)
        Me.grbKeyBoard.Controls.Add(Me.btn5)
        Me.grbKeyBoard.Controls.Add(Me.btn4)
        Me.grbKeyBoard.Controls.Add(Me.btn3)
        Me.grbKeyBoard.Controls.Add(Me.btn2)
        Me.grbKeyBoard.Controls.Add(Me.btn1)
        Me.grbKeyBoard.Controls.Add(Me.ShapeContainer1)
        Me.grbKeyBoard.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.grbKeyBoard.ForeColor = System.Drawing.Color.Navy
        Me.grbKeyBoard.Location = New System.Drawing.Point(244, 3)
        Me.grbKeyBoard.Name = "grbKeyBoard"
        Me.TableLayoutPanel6.SetRowSpan(Me.grbKeyBoard, 2)
        Me.grbKeyBoard.Size = New System.Drawing.Size(693, 449)
        Me.grbKeyBoard.TabIndex = 49
        Me.grbKeyBoard.TabStop = False
        Me.grbKeyBoard.Text = "Введите Логин и пароль:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Palatino Linotype", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(354, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(89, 29)
        Me.Label2.TabIndex = 145
        Me.Label2.Tag = ""
        Me.Label2.Text = "Пароль"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Palatino Linotype", 16.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(7, 55)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(76, 29)
        Me.Label1.TabIndex = 144
        Me.Label1.Tag = ""
        Me.Label1.Text = "Логин"
        '
        'txtPass
        '
        Me.txtPass.BackColor = System.Drawing.Color.White
        Me.txtPass.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtPass.Location = New System.Drawing.Point(443, 51)
        Me.txtPass.Name = "txtPass"
        Me.txtPass.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtPass.ReadOnly = True
        Me.txtPass.Size = New System.Drawing.Size(218, 36)
        Me.txtPass.TabIndex = 143
        '
        'txtLogin
        '
        Me.txtLogin.BackColor = System.Drawing.Color.White
        Me.txtLogin.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtLogin.Location = New System.Drawing.Point(84, 51)
        Me.txtLogin.Name = "txtLogin"
        Me.txtLogin.ReadOnly = True
        Me.txtLogin.Size = New System.Drawing.Size(218, 36)
        Me.txtLogin.TabIndex = 142
        '
        'btngz
        '
        Me.btngz.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btngz.Enabled = False
        Me.btngz.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btngz.Location = New System.Drawing.Point(490, 113)
        Me.btngz.Name = "btngz"
        Me.btngz.Size = New System.Drawing.Size(42, 42)
        Me.btngz.TabIndex = 141
        Me.btngz.UseVisualStyleBackColor = False
        '
        'btnzu
        '
        Me.btnzu.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnzu.Enabled = False
        Me.btnzu.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnzu.Location = New System.Drawing.Point(538, 113)
        Me.btnzu.Name = "btnzu"
        Me.btnzu.Size = New System.Drawing.Size(42, 42)
        Me.btnzu.TabIndex = 140
        Me.btnzu.UseVisualStyleBackColor = False
        '
        'btn_lng
        '
        Me.btn_lng.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn_lng.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn_lng.Location = New System.Drawing.Point(12, 264)
        Me.btn_lng.Name = "btn_lng"
        Me.btn_lng.Size = New System.Drawing.Size(72, 42)
        Me.btn_lng.TabIndex = 139
        Me.btn_lng.Text = "RU"
        Me.btn_lng.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btn_lng.UseVisualStyleBackColor = False
        Me.btn_lng.Visible = False
        '
        'btno
        '
        Me.btno.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btno.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btno.Location = New System.Drawing.Point(424, 163)
        Me.btno.Name = "btno"
        Me.btno.Size = New System.Drawing.Size(42, 42)
        Me.btno.TabIndex = 138
        Me.btno.Text = "O"
        Me.btno.UseVisualStyleBackColor = False
        '
        'btni
        '
        Me.btni.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btni.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btni.Location = New System.Drawing.Point(376, 163)
        Me.btni.Name = "btni"
        Me.btni.Size = New System.Drawing.Size(42, 42)
        Me.btni.TabIndex = 137
        Me.btni.Text = "I"
        Me.btni.UseVisualStyleBackColor = False
        '
        'btnu
        '
        Me.btnu.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnu.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnu.Location = New System.Drawing.Point(328, 163)
        Me.btnu.Name = "btnu"
        Me.btnu.Size = New System.Drawing.Size(42, 42)
        Me.btnu.TabIndex = 136
        Me.btnu.Text = "U"
        Me.btnu.UseVisualStyleBackColor = False
        '
        'btny
        '
        Me.btny.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btny.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btny.Location = New System.Drawing.Point(280, 163)
        Me.btny.Name = "btny"
        Me.btny.Size = New System.Drawing.Size(42, 42)
        Me.btny.TabIndex = 135
        Me.btny.Text = "Y"
        Me.btny.UseVisualStyleBackColor = False
        '
        'btnt
        '
        Me.btnt.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnt.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnt.Location = New System.Drawing.Point(232, 163)
        Me.btnt.Name = "btnt"
        Me.btnt.Size = New System.Drawing.Size(42, 42)
        Me.btnt.TabIndex = 134
        Me.btnt.Text = "T"
        Me.btnt.UseVisualStyleBackColor = False
        '
        'btnr
        '
        Me.btnr.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnr.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnr.Location = New System.Drawing.Point(184, 163)
        Me.btnr.Name = "btnr"
        Me.btnr.Size = New System.Drawing.Size(42, 42)
        Me.btnr.TabIndex = 133
        Me.btnr.Text = "R"
        Me.btnr.UseVisualStyleBackColor = False
        '
        'btne
        '
        Me.btne.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btne.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btne.Location = New System.Drawing.Point(136, 163)
        Me.btne.Name = "btne"
        Me.btne.Size = New System.Drawing.Size(42, 42)
        Me.btne.TabIndex = 132
        Me.btne.Text = "E"
        Me.btne.UseVisualStyleBackColor = False
        '
        'btnw
        '
        Me.btnw.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnw.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnw.Location = New System.Drawing.Point(88, 163)
        Me.btnw.Name = "btnw"
        Me.btnw.Size = New System.Drawing.Size(42, 42)
        Me.btnw.TabIndex = 131
        Me.btnw.TabStop = False
        Me.btnw.Text = "W"
        Me.btnw.UseVisualStyleBackColor = False
        '
        'btnspace
        '
        Me.btnspace.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnspace.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnspace.Location = New System.Drawing.Point(88, 312)
        Me.btnspace.Name = "btnspace"
        Me.btnspace.Size = New System.Drawing.Size(477, 42)
        Me.btnspace.TabIndex = 127
        Me.btnspace.Text = "Space"
        Me.btnspace.UseVisualStyleBackColor = False
        '
        'btnnokta
        '
        Me.btnnokta.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnnokta.Enabled = False
        Me.btnnokta.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnnokta.Location = New System.Drawing.Point(472, 264)
        Me.btnnokta.Name = "btnnokta"
        Me.btnnokta.Size = New System.Drawing.Size(44, 42)
        Me.btnnokta.TabIndex = 124
        Me.btnnokta.Text = "."
        Me.btnnokta.UseVisualStyleBackColor = False
        '
        'btn_c
        '
        Me.btn_c.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn_c.Enabled = False
        Me.btn_c.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn_c.Location = New System.Drawing.Point(521, 264)
        Me.btn_c.Name = "btn_c"
        Me.btn_c.Size = New System.Drawing.Size(44, 42)
        Me.btn_c.TabIndex = 123
        Me.btn_c.UseVisualStyleBackColor = False
        '
        'btnvirgul
        '
        Me.btnvirgul.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnvirgul.Enabled = False
        Me.btnvirgul.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnvirgul.Location = New System.Drawing.Point(423, 264)
        Me.btnvirgul.Name = "btnvirgul"
        Me.btnvirgul.Size = New System.Drawing.Size(44, 42)
        Me.btnvirgul.TabIndex = 122
        Me.btnvirgul.Text = ","
        Me.btnvirgul.UseVisualStyleBackColor = False
        '
        'btnm
        '
        Me.btnm.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnm.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnm.Location = New System.Drawing.Point(375, 264)
        Me.btnm.Name = "btnm"
        Me.btnm.Size = New System.Drawing.Size(44, 42)
        Me.btnm.TabIndex = 121
        Me.btnm.Text = "M"
        Me.btnm.UseVisualStyleBackColor = False
        '
        'btnn
        '
        Me.btnn.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnn.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnn.Location = New System.Drawing.Point(328, 264)
        Me.btnn.Name = "btnn"
        Me.btnn.Size = New System.Drawing.Size(44, 42)
        Me.btnn.TabIndex = 120
        Me.btnn.Text = "N"
        Me.btnn.UseVisualStyleBackColor = False
        '
        'btnb
        '
        Me.btnb.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnb.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnb.Location = New System.Drawing.Point(280, 264)
        Me.btnb.Name = "btnb"
        Me.btnb.Size = New System.Drawing.Size(44, 42)
        Me.btnb.TabIndex = 119
        Me.btnb.Text = "B"
        Me.btnb.UseVisualStyleBackColor = False
        '
        'btnv
        '
        Me.btnv.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnv.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnv.Location = New System.Drawing.Point(232, 264)
        Me.btnv.Name = "btnv"
        Me.btnv.Size = New System.Drawing.Size(44, 42)
        Me.btnv.TabIndex = 118
        Me.btnv.Text = "V"
        Me.btnv.UseVisualStyleBackColor = False
        '
        'btnc
        '
        Me.btnc.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnc.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnc.Location = New System.Drawing.Point(184, 264)
        Me.btnc.Name = "btnc"
        Me.btnc.Size = New System.Drawing.Size(44, 42)
        Me.btnc.TabIndex = 117
        Me.btnc.Text = "C"
        Me.btnc.UseVisualStyleBackColor = False
        '
        'btnx
        '
        Me.btnx.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnx.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnx.Location = New System.Drawing.Point(135, 264)
        Me.btnx.Name = "btnx"
        Me.btnx.Size = New System.Drawing.Size(44, 42)
        Me.btnx.TabIndex = 116
        Me.btnx.Text = "X"
        Me.btnx.UseVisualStyleBackColor = False
        '
        'btnz
        '
        Me.btnz.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnz.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnz.Location = New System.Drawing.Point(88, 264)
        Me.btnz.Name = "btnz"
        Me.btnz.Size = New System.Drawing.Size(44, 42)
        Me.btnz.TabIndex = 115
        Me.btnz.Text = "Z"
        Me.btnz.UseVisualStyleBackColor = False
        '
        'btnsolshift
        '
        Me.btnsolshift.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnsolshift.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnsolshift.Location = New System.Drawing.Point(570, 264)
        Me.btnsolshift.Name = "btnsolshift"
        Me.btnsolshift.Size = New System.Drawing.Size(92, 42)
        Me.btnsolshift.TabIndex = 114
        Me.btnsolshift.Text = "↑Shift"
        Me.btnsolshift.UseVisualStyleBackColor = False
        '
        'btn_i
        '
        Me.btn_i.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn_i.Enabled = False
        Me.btn_i.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn_i.Location = New System.Drawing.Point(538, 214)
        Me.btn_i.Name = "btn_i"
        Me.btn_i.Size = New System.Drawing.Size(42, 42)
        Me.btn_i.TabIndex = 112
        Me.btn_i.UseVisualStyleBackColor = False
        '
        'btn_s
        '
        Me.btn_s.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn_s.Enabled = False
        Me.btn_s.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn_s.Location = New System.Drawing.Point(490, 214)
        Me.btn_s.Name = "btn_s"
        Me.btn_s.Size = New System.Drawing.Size(42, 42)
        Me.btn_s.TabIndex = 111
        Me.btn_s.UseVisualStyleBackColor = False
        '
        'btnl
        '
        Me.btnl.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnl.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnl.Location = New System.Drawing.Point(442, 214)
        Me.btnl.Name = "btnl"
        Me.btnl.Size = New System.Drawing.Size(42, 42)
        Me.btnl.TabIndex = 110
        Me.btnl.Text = "L"
        Me.btnl.UseVisualStyleBackColor = False
        '
        'btnk
        '
        Me.btnk.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnk.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnk.Location = New System.Drawing.Point(394, 214)
        Me.btnk.Name = "btnk"
        Me.btnk.Size = New System.Drawing.Size(42, 42)
        Me.btnk.TabIndex = 109
        Me.btnk.Text = "K"
        Me.btnk.UseVisualStyleBackColor = False
        '
        'btnj
        '
        Me.btnj.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnj.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnj.Location = New System.Drawing.Point(346, 214)
        Me.btnj.Name = "btnj"
        Me.btnj.Size = New System.Drawing.Size(42, 42)
        Me.btnj.TabIndex = 108
        Me.btnj.Text = "J"
        Me.btnj.UseVisualStyleBackColor = False
        '
        'btnh
        '
        Me.btnh.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnh.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnh.Location = New System.Drawing.Point(298, 214)
        Me.btnh.Name = "btnh"
        Me.btnh.Size = New System.Drawing.Size(42, 42)
        Me.btnh.TabIndex = 107
        Me.btnh.Text = "H"
        Me.btnh.UseVisualStyleBackColor = False
        '
        'btng
        '
        Me.btng.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btng.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btng.Location = New System.Drawing.Point(250, 214)
        Me.btng.Name = "btng"
        Me.btng.Size = New System.Drawing.Size(42, 42)
        Me.btng.TabIndex = 106
        Me.btng.Text = "G"
        Me.btng.UseVisualStyleBackColor = False
        '
        'btnf
        '
        Me.btnf.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnf.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnf.Location = New System.Drawing.Point(202, 214)
        Me.btnf.Name = "btnf"
        Me.btnf.Size = New System.Drawing.Size(42, 42)
        Me.btnf.TabIndex = 105
        Me.btnf.Text = "F"
        Me.btnf.UseVisualStyleBackColor = False
        '
        'btnd
        '
        Me.btnd.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnd.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnd.Location = New System.Drawing.Point(154, 214)
        Me.btnd.Name = "btnd"
        Me.btnd.Size = New System.Drawing.Size(42, 42)
        Me.btnd.TabIndex = 104
        Me.btnd.Text = "D"
        Me.btnd.UseVisualStyleBackColor = False
        '
        'btns
        '
        Me.btns.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btns.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btns.Location = New System.Drawing.Point(106, 214)
        Me.btns.Name = "btns"
        Me.btns.Size = New System.Drawing.Size(42, 42)
        Me.btns.TabIndex = 103
        Me.btns.Text = "S"
        Me.btns.UseVisualStyleBackColor = False
        '
        'btna
        '
        Me.btna.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btna.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btna.Location = New System.Drawing.Point(58, 214)
        Me.btna.Name = "btna"
        Me.btna.Size = New System.Drawing.Size(42, 42)
        Me.btna.TabIndex = 102
        Me.btna.Text = "A"
        Me.btna.UseVisualStyleBackColor = False
        '
        'btn_u
        '
        Me.btn_u.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn_u.Enabled = False
        Me.btn_u.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn_u.Location = New System.Drawing.Point(568, 163)
        Me.btn_u.Name = "btn_u"
        Me.btn_u.Size = New System.Drawing.Size(42, 42)
        Me.btn_u.TabIndex = 101
        Me.btn_u.UseVisualStyleBackColor = False
        '
        'btn_kh
        '
        Me.btn_kh.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn_kh.Enabled = False
        Me.btn_kh.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn_kh.Location = New System.Drawing.Point(520, 163)
        Me.btn_kh.Name = "btn_kh"
        Me.btn_kh.Size = New System.Drawing.Size(42, 42)
        Me.btn_kh.TabIndex = 100
        Me.btn_kh.UseVisualStyleBackColor = False
        '
        'btnp
        '
        Me.btnp.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnp.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnp.Location = New System.Drawing.Point(472, 163)
        Me.btnp.Name = "btnp"
        Me.btnp.Size = New System.Drawing.Size(42, 42)
        Me.btnp.TabIndex = 99
        Me.btnp.Text = "P"
        Me.btnp.UseVisualStyleBackColor = False
        '
        'btnq
        '
        Me.btnq.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnq.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnq.Location = New System.Drawing.Point(40, 163)
        Me.btnq.Name = "btnq"
        Me.btnq.Size = New System.Drawing.Size(42, 42)
        Me.btnq.TabIndex = 98
        Me.btnq.TabStop = False
        Me.btnq.Text = "Q"
        Me.btnq.UseVisualStyleBackColor = False
        '
        'btnbackspace
        '
        Me.btnbackspace.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnbackspace.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btnbackspace.Location = New System.Drawing.Point(586, 113)
        Me.btnbackspace.Name = "btnbackspace"
        Me.btnbackspace.Size = New System.Drawing.Size(100, 42)
        Me.btnbackspace.TabIndex = 97
        Me.btnbackspace.Text = "← BS"
        Me.btnbackspace.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnbackspace.UseVisualStyleBackColor = False
        '
        'btn0
        '
        Me.btn0.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn0.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn0.Location = New System.Drawing.Point(442, 113)
        Me.btn0.Name = "btn0"
        Me.btn0.Size = New System.Drawing.Size(42, 42)
        Me.btn0.TabIndex = 96
        Me.btn0.Text = "0"
        Me.btn0.UseVisualStyleBackColor = False
        '
        'btn9
        '
        Me.btn9.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn9.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn9.Location = New System.Drawing.Point(394, 113)
        Me.btn9.Name = "btn9"
        Me.btn9.Size = New System.Drawing.Size(42, 42)
        Me.btn9.TabIndex = 95
        Me.btn9.Text = "9"
        Me.btn9.UseVisualStyleBackColor = False
        '
        'btn8
        '
        Me.btn8.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn8.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn8.Location = New System.Drawing.Point(346, 113)
        Me.btn8.Name = "btn8"
        Me.btn8.Size = New System.Drawing.Size(42, 42)
        Me.btn8.TabIndex = 94
        Me.btn8.Text = "8"
        Me.btn8.UseVisualStyleBackColor = False
        '
        'btn7
        '
        Me.btn7.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn7.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn7.Location = New System.Drawing.Point(298, 113)
        Me.btn7.Name = "btn7"
        Me.btn7.Size = New System.Drawing.Size(42, 42)
        Me.btn7.TabIndex = 93
        Me.btn7.Text = "7"
        Me.btn7.UseVisualStyleBackColor = False
        '
        'btn6
        '
        Me.btn6.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn6.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn6.Location = New System.Drawing.Point(250, 113)
        Me.btn6.Name = "btn6"
        Me.btn6.Size = New System.Drawing.Size(42, 42)
        Me.btn6.TabIndex = 92
        Me.btn6.Text = "6"
        Me.btn6.UseVisualStyleBackColor = False
        '
        'btn5
        '
        Me.btn5.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn5.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn5.Location = New System.Drawing.Point(202, 113)
        Me.btn5.Name = "btn5"
        Me.btn5.Size = New System.Drawing.Size(42, 42)
        Me.btn5.TabIndex = 91
        Me.btn5.Text = "5"
        Me.btn5.UseVisualStyleBackColor = False
        '
        'btn4
        '
        Me.btn4.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn4.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn4.Location = New System.Drawing.Point(154, 113)
        Me.btn4.Name = "btn4"
        Me.btn4.Size = New System.Drawing.Size(42, 42)
        Me.btn4.TabIndex = 90
        Me.btn4.Text = "4"
        Me.btn4.UseVisualStyleBackColor = False
        '
        'btn3
        '
        Me.btn3.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn3.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn3.Location = New System.Drawing.Point(106, 113)
        Me.btn3.Name = "btn3"
        Me.btn3.Size = New System.Drawing.Size(42, 42)
        Me.btn3.TabIndex = 89
        Me.btn3.Text = "3"
        Me.btn3.UseVisualStyleBackColor = False
        '
        'btn2
        '
        Me.btn2.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn2.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn2.Location = New System.Drawing.Point(58, 113)
        Me.btn2.Name = "btn2"
        Me.btn2.Size = New System.Drawing.Size(42, 42)
        Me.btn2.TabIndex = 88
        Me.btn2.Text = "2"
        Me.btn2.UseVisualStyleBackColor = False
        '
        'btn1
        '
        Me.btn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btn1.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.btn1.Location = New System.Drawing.Point(10, 113)
        Me.btn1.Name = "btn1"
        Me.btn1.Size = New System.Drawing.Size(42, 42)
        Me.btn1.TabIndex = 87
        Me.btn1.Text = "1"
        Me.btn1.UseVisualStyleBackColor = False
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(3, 32)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.LineShape1})
        Me.ShapeContainer1.Size = New System.Drawing.Size(687, 414)
        Me.ShapeContainer1.TabIndex = 146
        Me.ShapeContainer1.TabStop = False
        '
        'LineShape1
        '
        Me.LineShape1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.LineShape1.Name = "LineShape1"
        Me.LineShape1.X1 = -2
        Me.LineShape1.X2 = 685
        Me.LineShape1.Y1 = 62
        Me.LineShape1.Y2 = 62
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblStatus.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Indigo
        Me.lblStatus.Location = New System.Drawing.Point(3, 0)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(1182, 50)
        Me.lblStatus.TabIndex = 1
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.pbLogo, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1194, 780)
        Me.TableLayoutPanel2.TabIndex = 9
        '
        'pbLogo
        '
        Me.pbLogo.BackColor = System.Drawing.Color.Transparent
        Me.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TableLayoutPanel2.SetColumnSpan(Me.pbLogo, 2)
        Me.pbLogo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbLogo.Location = New System.Drawing.Point(3, 3)
        Me.pbLogo.Name = "pbLogo"
        Me.pbLogo.Size = New System.Drawing.Size(1188, 214)
        Me.pbLogo.TabIndex = 11
        Me.pbLogo.TabStop = False
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.lblStatus, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel6, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.tblGo, 0, 2)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 223)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 3
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1188, 554)
        Me.TableLayoutPanel3.TabIndex = 8
        '
        'tblGo
        '
        Me.tblGo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tblGo.ColumnCount = 2
        Me.tblGo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblGo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblGo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblGo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblGo.Location = New System.Drawing.Point(3, 447)
        Me.tblGo.Name = "tblGo"
        Me.tblGo.RowCount = 1
        Me.tblGo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblGo.Size = New System.Drawing.Size(1182, 104)
        Me.tblGo.TabIndex = 4
        '
        'frmLogin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1194, 780)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmLogin"
        Me.Text = "frmLogin"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.grbKeyBoard.ResumeLayout(False)
        Me.grbKeyBoard.PerformLayout()
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents tblGo As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pbLogo As System.Windows.Forms.PictureBox
    Friend WithEvents grbKeyBoard As System.Windows.Forms.GroupBox
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtPass As System.Windows.Forms.TextBox
    Friend WithEvents txtLogin As System.Windows.Forms.TextBox
    Friend WithEvents btngz As System.Windows.Forms.Button
    Friend WithEvents btnzu As System.Windows.Forms.Button
    Friend WithEvents btn_lng As System.Windows.Forms.Button
    Friend WithEvents btno As System.Windows.Forms.Button
    Friend WithEvents btni As System.Windows.Forms.Button
    Friend WithEvents btnu As System.Windows.Forms.Button
    Friend WithEvents btny As System.Windows.Forms.Button
    Friend WithEvents btnt As System.Windows.Forms.Button
    Friend WithEvents btnr As System.Windows.Forms.Button
    Friend WithEvents btne As System.Windows.Forms.Button
    Friend WithEvents btnw As System.Windows.Forms.Button
    Friend WithEvents btnspace As System.Windows.Forms.Button
    Friend WithEvents btnnokta As System.Windows.Forms.Button
    Friend WithEvents btn_c As System.Windows.Forms.Button
    Friend WithEvents btnvirgul As System.Windows.Forms.Button
    Friend WithEvents btnm As System.Windows.Forms.Button
    Friend WithEvents btnn As System.Windows.Forms.Button
    Friend WithEvents btnb As System.Windows.Forms.Button
    Friend WithEvents btnv As System.Windows.Forms.Button
    Friend WithEvents btnc As System.Windows.Forms.Button
    Friend WithEvents btnx As System.Windows.Forms.Button
    Friend WithEvents btnz As System.Windows.Forms.Button
    Friend WithEvents btnsolshift As System.Windows.Forms.Button
    Friend WithEvents btn_i As System.Windows.Forms.Button
    Friend WithEvents btn_s As System.Windows.Forms.Button
    Friend WithEvents btnl As System.Windows.Forms.Button
    Friend WithEvents btnk As System.Windows.Forms.Button
    Friend WithEvents btnj As System.Windows.Forms.Button
    Friend WithEvents btnh As System.Windows.Forms.Button
    Friend WithEvents btng As System.Windows.Forms.Button
    Friend WithEvents btnf As System.Windows.Forms.Button
    Friend WithEvents btnd As System.Windows.Forms.Button
    Friend WithEvents btns As System.Windows.Forms.Button
    Friend WithEvents btna As System.Windows.Forms.Button
    Friend WithEvents btn_u As System.Windows.Forms.Button
    Friend WithEvents btn_kh As System.Windows.Forms.Button
    Friend WithEvents btnp As System.Windows.Forms.Button
    Friend WithEvents btnq As System.Windows.Forms.Button
    Friend WithEvents btnbackspace As System.Windows.Forms.Button
    Friend WithEvents btn0 As System.Windows.Forms.Button
    Friend WithEvents btn9 As System.Windows.Forms.Button
    Friend WithEvents btn8 As System.Windows.Forms.Button
    Friend WithEvents btn7 As System.Windows.Forms.Button
    Friend WithEvents btn6 As System.Windows.Forms.Button
    Friend WithEvents btn5 As System.Windows.Forms.Button
    Friend WithEvents btn4 As System.Windows.Forms.Button
    Friend WithEvents btn3 As System.Windows.Forms.Button
    Friend WithEvents btn2 As System.Windows.Forms.Button
    Friend WithEvents btn1 As System.Windows.Forms.Button
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents LineShape1 As Microsoft.VisualBasic.PowerPacks.LineShape
End Class
