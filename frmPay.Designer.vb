﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPay
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPay))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.AxShockwaveFlash1 = New AxShockwaveFlashObjects.AxShockwaveFlash()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel8 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl3 = New System.Windows.Forms.Label()
        Me.lblZachislenie = New System.Windows.Forms.Label()
        Me.TableLayoutPanel7 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl2 = New System.Windows.Forms.Label()
        Me.lblComissiya = New System.Windows.Forms.Label()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl1 = New System.Windows.Forms.Label()
        Me.lblSumma = New System.Windows.Forms.Label()
        Me.TableLayoutPanel4 = New System.Windows.Forms.TableLayoutPanel()
        Me.lbl0 = New System.Windows.Forms.Label()
        Me.lblInsert = New System.Windows.Forms.Label()
        Me.groupCommision = New System.Windows.Forms.GroupBox()
        Me.lblComission = New System.Windows.Forms.Label()
        Me.TableLayoutPanel9 = New System.Windows.Forms.TableLayoutPanel()
        Me.pbService = New System.Windows.Forms.PictureBox()
        Me.tblTitle = New System.Windows.Forms.TableLayoutPanel()
        Me.lblNomer = New System.Windows.Forms.Label()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.pbLogo = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.tblGo = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TableLayoutPanel6.SuspendLayout()
        CType(Me.AxShockwaveFlash1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.TableLayoutPanel8.SuspendLayout()
        Me.TableLayoutPanel7.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.TableLayoutPanel4.SuspendLayout()
        Me.groupCommision.SuspendLayout()
        Me.TableLayoutPanel9.SuspendLayout()
        CType(Me.pbService, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.tblTitle.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 90000
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 3
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 677.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.AxShockwaveFlash1, 2, 1)
        Me.TableLayoutPanel6.Controls.Add(Me.TableLayoutPanel1, 1, 1)
        Me.TableLayoutPanel6.Controls.Add(Me.groupCommision, 0, 1)
        Me.TableLayoutPanel6.Controls.Add(Me.TableLayoutPanel9, 0, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.Label1, 1, 2)
        Me.TableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(3, 53)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 3
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 353.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(1268, 632)
        Me.TableLayoutPanel6.TabIndex = 3
        '
        'AxShockwaveFlash1
        '
        Me.AxShockwaveFlash1.Enabled = True
        Me.AxShockwaveFlash1.Location = New System.Drawing.Point(975, 133)
        Me.AxShockwaveFlash1.Name = "AxShockwaveFlash1"
        Me.AxShockwaveFlash1.OcxState = CType(resources.GetObject("AxShockwaveFlash1.OcxState"), System.Windows.Forms.AxHost.State)
        Me.AxShockwaveFlash1.Size = New System.Drawing.Size(194, 340)
        Me.AxShockwaveFlash1.TabIndex = 4
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel8, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel7, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel5, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel4, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(298, 133)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 70.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(671, 347)
        Me.TableLayoutPanel1.TabIndex = 2
        '
        'TableLayoutPanel8
        '
        Me.TableLayoutPanel8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TableLayoutPanel8.ColumnCount = 2
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 348.0!))
        Me.TableLayoutPanel8.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel8.Controls.Add(Me.lbl3, 0, 0)
        Me.TableLayoutPanel8.Controls.Add(Me.lblZachislenie, 1, 0)
        Me.TableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel8.Location = New System.Drawing.Point(3, 213)
        Me.TableLayoutPanel8.Name = "TableLayoutPanel8"
        Me.TableLayoutPanel8.RowCount = 1
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel8.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 174.0!))
        Me.TableLayoutPanel8.Size = New System.Drawing.Size(665, 131)
        Me.TableLayoutPanel8.TabIndex = 5
        '
        'lbl3
        '
        Me.lbl3.BackColor = System.Drawing.Color.Transparent
        Me.lbl3.Dock = System.Windows.Forms.DockStyle.Top
        Me.lbl3.Font = New System.Drawing.Font("Microsoft Sans Serif", 21.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lbl3.ForeColor = System.Drawing.Color.Indigo
        Me.lbl3.Location = New System.Drawing.Point(3, 0)
        Me.lbl3.Name = "lbl3"
        Me.lbl3.Size = New System.Drawing.Size(342, 64)
        Me.lbl3.TabIndex = 2
        Me.lbl3.Text = "Сумма к зачислению"
        Me.lbl3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblZachislenie
        '
        Me.lblZachislenie.BackColor = System.Drawing.Color.Transparent
        Me.lblZachislenie.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblZachislenie.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblZachislenie.ForeColor = System.Drawing.Color.Indigo
        Me.lblZachislenie.Image = Global.Terminal.My.Resources.Resources.summ
        Me.lblZachislenie.Location = New System.Drawing.Point(351, 0)
        Me.lblZachislenie.Name = "lblZachislenie"
        Me.lblZachislenie.Size = New System.Drawing.Size(311, 64)
        Me.lblZachislenie.TabIndex = 1
        Me.lblZachislenie.Text = "0"
        Me.lblZachislenie.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel7
        '
        Me.TableLayoutPanel7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TableLayoutPanel7.ColumnCount = 2
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 348.0!))
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel7.Controls.Add(Me.lbl2, 0, 0)
        Me.TableLayoutPanel7.Controls.Add(Me.lblComissiya, 1, 0)
        Me.TableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel7.Location = New System.Drawing.Point(3, 143)
        Me.TableLayoutPanel7.Name = "TableLayoutPanel7"
        Me.TableLayoutPanel7.RowCount = 1
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64.0!))
        Me.TableLayoutPanel7.Size = New System.Drawing.Size(665, 64)
        Me.TableLayoutPanel7.TabIndex = 4
        '
        'lbl2
        '
        Me.lbl2.BackColor = System.Drawing.Color.Transparent
        Me.lbl2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl2.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lbl2.ForeColor = System.Drawing.Color.Red
        Me.lbl2.Location = New System.Drawing.Point(3, 0)
        Me.lbl2.Name = "lbl2"
        Me.lbl2.Size = New System.Drawing.Size(342, 64)
        Me.lbl2.TabIndex = 2
        Me.lbl2.Text = "Комиссия"
        Me.lbl2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblComissiya
        '
        Me.lblComissiya.BackColor = System.Drawing.Color.Transparent
        Me.lblComissiya.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblComissiya.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblComissiya.ForeColor = System.Drawing.Color.Red
        Me.lblComissiya.Image = Global.Terminal.My.Resources.Resources.summ
        Me.lblComissiya.Location = New System.Drawing.Point(351, 0)
        Me.lblComissiya.Name = "lblComissiya"
        Me.lblComissiya.Size = New System.Drawing.Size(311, 64)
        Me.lblComissiya.TabIndex = 1
        Me.lblComissiya.Text = "0"
        Me.lblComissiya.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TableLayoutPanel5.ColumnCount = 2
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 348.0!))
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.lbl1, 0, 0)
        Me.TableLayoutPanel5.Controls.Add(Me.lblSumma, 1, 0)
        Me.TableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(3, 73)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(665, 64)
        Me.TableLayoutPanel5.TabIndex = 3
        '
        'lbl1
        '
        Me.lbl1.BackColor = System.Drawing.Color.Transparent
        Me.lbl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl1.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lbl1.ForeColor = System.Drawing.Color.Indigo
        Me.lbl1.Location = New System.Drawing.Point(3, 0)
        Me.lbl1.Name = "lbl1"
        Me.lbl1.Size = New System.Drawing.Size(342, 64)
        Me.lbl1.TabIndex = 2
        Me.lbl1.Text = "Сумма"
        Me.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'lblSumma
        '
        Me.lblSumma.BackColor = System.Drawing.Color.Transparent
        Me.lblSumma.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblSumma.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblSumma.ForeColor = System.Drawing.Color.Indigo
        Me.lblSumma.Image = Global.Terminal.My.Resources.Resources.summ
        Me.lblSumma.Location = New System.Drawing.Point(351, 0)
        Me.lblSumma.Name = "lblSumma"
        Me.lblSumma.Size = New System.Drawing.Size(311, 64)
        Me.lblSumma.TabIndex = 1
        Me.lblSumma.Text = "0"
        Me.lblSumma.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'TableLayoutPanel4
        '
        Me.TableLayoutPanel4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TableLayoutPanel4.ColumnCount = 2
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 347.0!))
        Me.TableLayoutPanel4.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.Controls.Add(Me.lbl0, 0, 0)
        Me.TableLayoutPanel4.Controls.Add(Me.lblInsert, 1, 0)
        Me.TableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel4.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel4.Name = "TableLayoutPanel4"
        Me.TableLayoutPanel4.RowCount = 1
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel4.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64.0!))
        Me.TableLayoutPanel4.Size = New System.Drawing.Size(665, 64)
        Me.TableLayoutPanel4.TabIndex = 2
        '
        'lbl0
        '
        Me.lbl0.BackColor = System.Drawing.Color.Transparent
        Me.lbl0.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lbl0.Font = New System.Drawing.Font("Microsoft Sans Serif", 26.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lbl0.ForeColor = System.Drawing.Color.Indigo
        Me.lbl0.Location = New System.Drawing.Point(3, 0)
        Me.lbl0.Name = "lbl0"
        Me.lbl0.Size = New System.Drawing.Size(341, 64)
        Me.lbl0.TabIndex = 2
        Me.lbl0.Text = "Внесенная купюра"
        Me.lbl0.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.lbl0.Visible = False
        '
        'lblInsert
        '
        Me.lblInsert.BackColor = System.Drawing.Color.Transparent
        Me.lblInsert.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblInsert.Font = New System.Drawing.Font("Microsoft Sans Serif", 36.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblInsert.ForeColor = System.Drawing.Color.Indigo
        Me.lblInsert.Image = Global.Terminal.My.Resources.Resources.summ
        Me.lblInsert.Location = New System.Drawing.Point(350, 0)
        Me.lblInsert.Name = "lblInsert"
        Me.lblInsert.Size = New System.Drawing.Size(312, 64)
        Me.lblInsert.TabIndex = 1
        Me.lblInsert.Text = "0"
        Me.lblInsert.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.lblInsert.Visible = False
        '
        'groupCommision
        '
        Me.groupCommision.BackColor = System.Drawing.Color.Transparent
        Me.groupCommision.BackgroundImage = Global.Terminal.My.Resources.Resources.panel
        Me.groupCommision.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.groupCommision.Controls.Add(Me.lblComission)
        Me.groupCommision.Dock = System.Windows.Forms.DockStyle.Fill
        Me.groupCommision.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.groupCommision.Location = New System.Drawing.Point(3, 133)
        Me.groupCommision.Name = "groupCommision"
        Me.groupCommision.Size = New System.Drawing.Size(289, 347)
        Me.groupCommision.TabIndex = 3
        Me.groupCommision.TabStop = False
        '
        'lblComission
        '
        Me.lblComission.AutoEllipsis = True
        Me.lblComission.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblComission.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblComission.ForeColor = System.Drawing.Color.Indigo
        Me.lblComission.Location = New System.Drawing.Point(3, 16)
        Me.lblComission.Name = "lblComission"
        Me.lblComission.Size = New System.Drawing.Size(283, 328)
        Me.lblComission.TabIndex = 0
        '
        'TableLayoutPanel9
        '
        Me.TableLayoutPanel9.ColumnCount = 2
        Me.TableLayoutPanel6.SetColumnSpan(Me.TableLayoutPanel9, 3)
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel9.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel9.Controls.Add(Me.pbService, 0, 0)
        Me.TableLayoutPanel9.Controls.Add(Me.tblTitle, 1, 0)
        Me.TableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel9.Location = New System.Drawing.Point(3, 3)
        Me.TableLayoutPanel9.Name = "TableLayoutPanel9"
        Me.TableLayoutPanel9.RowCount = 1
        Me.TableLayoutPanel9.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel9.Size = New System.Drawing.Size(1262, 124)
        Me.TableLayoutPanel9.TabIndex = 5
        '
        'pbService
        '
        Me.pbService.BackColor = System.Drawing.Color.Transparent
        Me.pbService.Dock = System.Windows.Forms.DockStyle.Right
        Me.pbService.Location = New System.Drawing.Point(10, 10)
        Me.pbService.Margin = New System.Windows.Forms.Padding(10)
        Me.pbService.Name = "pbService"
        Me.pbService.Size = New System.Drawing.Size(255, 104)
        Me.pbService.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbService.TabIndex = 2
        Me.pbService.TabStop = False
        '
        'tblTitle
        '
        Me.tblTitle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tblTitle.ColumnCount = 1
        Me.tblTitle.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblTitle.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblTitle.Controls.Add(Me.lblNomer, 0, 0)
        Me.tblTitle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblTitle.Location = New System.Drawing.Point(278, 3)
        Me.tblTitle.Name = "tblTitle"
        Me.tblTitle.RowCount = 1
        Me.tblTitle.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblTitle.Size = New System.Drawing.Size(981, 118)
        Me.tblTitle.TabIndex = 1
        '
        'lblNomer
        '
        Me.lblNomer.BackColor = System.Drawing.Color.Transparent
        Me.lblNomer.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblNomer.Font = New System.Drawing.Font("Microsoft Sans Serif", 62.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblNomer.ForeColor = System.Drawing.Color.Indigo
        Me.lblNomer.Image = Global.Terminal.My.Resources.Resources.number_selected1
        Me.lblNomer.Location = New System.Drawing.Point(0, 0)
        Me.lblNomer.Margin = New System.Windows.Forms.Padding(0, 0, 3, 0)
        Me.lblNomer.Name = "lblNomer"
        Me.lblNomer.Size = New System.Drawing.Size(978, 118)
        Me.lblNomer.TabIndex = 1
        Me.lblNomer.Text = "(92) 774-77-36"
        Me.lblNomer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblStatus.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Indigo
        Me.lblStatus.Location = New System.Drawing.Point(3, 0)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(1268, 50)
        Me.lblStatus.TabIndex = 1
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.pbLogo, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1280, 1024)
        Me.TableLayoutPanel2.TabIndex = 9
        '
        'pbLogo
        '
        Me.pbLogo.BackColor = System.Drawing.Color.Transparent
        Me.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TableLayoutPanel2.SetColumnSpan(Me.pbLogo, 2)
        Me.pbLogo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbLogo.Location = New System.Drawing.Point(3, 3)
        Me.pbLogo.Name = "pbLogo"
        Me.pbLogo.Size = New System.Drawing.Size(1274, 214)
        Me.pbLogo.TabIndex = 11
        Me.pbLogo.TabStop = False
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.lblStatus, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel6, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.tblGo, 0, 2)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 223)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 3
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1274, 798)
        Me.TableLayoutPanel3.TabIndex = 8
        '
        'tblGo
        '
        Me.tblGo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tblGo.ColumnCount = 3
        Me.tblGo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblGo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblGo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblGo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblGo.Location = New System.Drawing.Point(3, 691)
        Me.tblGo.Name = "tblGo"
        Me.tblGo.RowCount = 1
        Me.tblGo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblGo.Size = New System.Drawing.Size(1268, 104)
        Me.tblGo.TabIndex = 4
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Blue
        Me.Label1.Location = New System.Drawing.Point(298, 483)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(671, 149)
        Me.Label1.TabIndex = 6
        '
        'frmPay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1280, 1024)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmPay"
        Me.Text = "frmPay"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TableLayoutPanel6.ResumeLayout(False)
        CType(Me.AxShockwaveFlash1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel8.ResumeLayout(False)
        Me.TableLayoutPanel7.ResumeLayout(False)
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel4.ResumeLayout(False)
        Me.groupCommision.ResumeLayout(False)
        Me.TableLayoutPanel9.ResumeLayout(False)
        CType(Me.pbService, System.ComponentModel.ISupportInitialize).EndInit()
        Me.tblTitle.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents tblGo As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pbLogo As System.Windows.Forms.PictureBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel7 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblComissiya As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblSumma As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel4 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblInsert As System.Windows.Forms.Label
    Friend WithEvents lbl2 As System.Windows.Forms.Label
    Friend WithEvents lbl1 As System.Windows.Forms.Label
    Friend WithEvents lbl0 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel8 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lbl3 As System.Windows.Forms.Label
    Friend WithEvents lblZachislenie As System.Windows.Forms.Label
    Friend WithEvents AxShockwaveFlash1 As AxShockwaveFlashObjects.AxShockwaveFlash
    Friend WithEvents TableLayoutPanel9 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pbService As System.Windows.Forms.PictureBox
    Friend WithEvents tblTitle As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblNomer As System.Windows.Forms.Label
    Friend WithEvents groupCommision As System.Windows.Forms.GroupBox
    Friend WithEvents lblComission As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
End Class
