﻿Public Class GlobalVar
    Public value As Integer = 10
    Public rst As Boolean = False

    Public terminal_id As String
    Public server_adr As String
    Public server_port As Integer
    Public server_file As String
    Public ClientCertFileName As String
    Public KS_PATH As String
    Public KS_PASS As String
    Public TS_PATH As String
    Public TS_PASS As String

    Public inkass_id As String
    Public inkass_n As String
    Public inkass_code As String
    Public com_n As Integer

    Public agent_login As String
    Public agent_pass As String

    Public getCashCode As Boolean = True
    Public syncDateTime As Boolean = False

    Public getServises As Boolean = False
    Public getGroupServises As Boolean = False
    Public getTarifServises As Boolean = False
    Public getExistsServices As Boolean = True

    Public getSettings As Boolean = False
    Public getInkassUsers As Boolean = False
    Public getS As Boolean = False
    Public CashCodeSerial As String = ""
    Public stBillCount As Integer = 0
    Public stBillSumm As Integer = 0
    Public showInterfase As Boolean = False

    Public connected As Boolean = False

    Public doBlockLine As Boolean = False
    Public doUnBlockLine As Boolean = False
    Public tBlocked As Boolean = False
    Public BlockTerminal As Boolean = False  ' Блокировать ли терминал если пропала связь

    Public doBlock As Boolean = False
    Public doUnBlock As Boolean = False
    Public doCommand(2) As Integer
    'Public doExecuteCount As Integer = 0

    Public TerminalAddress(2) As String

    Public callcenter As String = ""
    Public Organisation As String = ""

    Public stCashcode As String = ""
    Public stPrinter As Integer = 202 'По умолчанию статус принтера не работает. FIXME. dll Шарифако возвращает 201 даже если принтер не подключен.
    Public lastStPrinter As Integer
    Public stTerminal As Integer = 0
    Public lastInsertPlatej As Integer = 0
    Public KillExplorer As Boolean = True
    Public LogLevel As Integer = 1

    Public LangId As Integer
    Public GroupId As Integer
    Public OperId As Integer
    Public OperMask As String = "(XX) XXX-XX-XX"
    Public NomerAb As String = ""
    Public TypeDB As String = "MySQL"

    Public LastMSG As String = ""
    Public lastStorageActiveTime As Integer = 0
    'public Connection DataBase
    'public Statement st

    Public StatusMessage As String() = {"rus", "eng", "taj"}

    ''' <summary>
    ''' Статус терминала (Визуальное состояние)
    ''' </summary>
    ''' <value></value>
    ''' <returns>Целое число</returns>
    ''' <remarks></remarks>
    Public Property Terminal As Integer
        Get
            Return stTerminal
        End Get
        Set(ByVal Value As Integer)
            'If getLastState("terminal", Value) Then
            Execute("INSERT INTO perif_status (perif,state,date_change) values('terminal','" & Value & "',UNIX_TIMESTAMP())")
            Execute("UPDATE settings set value='" & Value & "' WHERE variable='terminal_state'")
            Dim state As String = ""
            Select Case Value
                Case 301
                    state = "Главное меню"
                Case 302
                    state = "Выключен"
                Case 303
                    state = "Блокирован"
                Case 304
                    state = "Инкассация"
                Case 305
                    state = "Выбор оператора"
                Case 306
                    state = "Ввод номера абонента"
                Case 307
                    state = "Приём денег"
                Case 308
                    state = "Вход в инкассацию"
            End Select
            msg("Статус терминала: " & state)
            'End If
            stTerminal = Value
        End Set
    End Property

    Public Property BillCount As Integer
        Get
            Return stBillCount
        End Get
        Set(ByVal Value As Integer)
            'If getLastState("terminal", Value) Then
            Execute("INSERT INTO perif_status (perif,state,date_change) values('cash_count','" & Value & "',UNIX_TIMESTAMP())")
            Execute("UPDATE settings set value='" & Value & "' WHERE variable='cash_count'")
            'End If
            stBillCount = Value
        End Set
    End Property

    Public Property BillSumm As Integer
        Get
            Return stBillSumm
        End Get
        Set(ByVal Value As Integer)
            'If getLastState("terminal", Value) Then
            Execute("INSERT INTO perif_status (perif,state,date_change) values('cash_summ','" & Value & "',UNIX_TIMESTAMP())")
            Execute("UPDATE settings set value='" & Value & "' WHERE variable='cash_summ'")
            'End If
            stBillSumm = Value
        End Set
    End Property

    Public Property Printer As Integer
        Get
            Return stPrinter
        End Get
        Set(ByVal value As Integer)
            If stPrinter <> value Then
                stPrinter = value
                lastStPrinter = value
            End If
            Execute("INSERT INTO perif_status (perif,state,date_change) values('printer','" & value & "',UNIX_TIMESTAMP())")
        End Set
    End Property


    Public Function GetPrinterState(ByVal stPrint As Integer) As Integer
        If stPrint = 201 Then
            'Если принтер работает, то пишем event
            Return stPrinter
        End If
        Return stPrint
    End Function
End Class