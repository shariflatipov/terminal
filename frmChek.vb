﻿Imports PrinterStatus

Public Class frmChek
    Dim cmdNext As New Button
    Dim OperMask As String
    Dim arrInfo(2) As String
    Dim arrLabel(2, 3) As String
    Public s1 As Double = 0
    Public s2 As Double = 0
    Public valuta As Integer()
    Public nomer As String
    Public PlatejID As Integer = 0

    Public chekN As String = ""

    Private Sub frmChek_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        Try
            pbLogo.BackgroundImage = Image.FromFile(Application.StartupPath & "/skins/logo.png")
        Catch ex As Exception
            msg("ERROR frmChek: " & ex.Message & " Error occured in: " & ex.TargetSite.ToString())
        End Try

        Dim sql As String = "UPDATE plateji SET status=0 WHERE id=" & PlatejID
        Execute(sql)

        Dim dt As DataTable
        Dim ser As Integer = 0
        Dim type As String = ""
        If group_id <> 0 Then
            ser = group_id
            type = "g"
        Else
            ser = service_id
            type = "s"
        End If

        ' ----------- Было закоментирована по причине необходимости создания
        ' ----------- Статического главного меню


        'dt = GetTable("select * from last_use_service where service = " & ser & " and type='" & type & "'")
        'If (dt.Rows.Count = 0) Then
        'Execute("update last_use_service set nomer=nomer+1")
        'Execute("delete from last_use_service where nomer > 6")
        'Execute("insert into last_use_service (nomer, service,type) values (1," & ser & ",'" & type & "')")
        'Else
        'Dim nomer2 As Integer = dt.Rows(0).Item("nomer")
        'Execute("update last_use_service set nomer=0 where nomer=" & nomer2 & "")
        'Execute("update last_use_service set nomer=nomer+1 where nomer < " & nomer2 & "")
        'End If
        ' ----------------------------------------------------------------------------------

        Execute("UPDATE settings SET value='" & GlobalVars.BillCount & "' WHERE variable='bill_count'")
        Execute("UPDATE settings SET value='" & GlobalVars.BillSumm & "' WHERE variable='bill_summ'")
        Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change) values('cash_count','" & GlobalVars.BillCount & "',UNIX_TIMESTAMP())"
        Dim sql2 As String = "INSERT INTO perif_status (perif,state,date_change) values('cash_summ','" & GlobalVars.BillSumm & "',UNIX_TIMESTAMP())"
        Execute(sql1)
        Execute(sql2)
        frmMain.fillLastUsedServices()

        Dim vDate As Date = New Date()
        Dim date_time As String = Date.Now
        Dim mes As String = "Ваш платеж принят на обработку!" & vbCrLf & vbCrLf & _
                "Дата платежа: " & date_time & vbCrLf & _
                "Чек №: " & chekN & vbCrLf & _
                "Сервис: " & service_name & vbCrLf & _
                "Номер счета: " & nomer & vbCrLf & _
                "Принято: " & s1 & vbCrLf & _
                "К зачислению: " & (s1 - s2) & vbCrLf & _
                "Комиссия: " & s2 & _
                vbCrLf & vbCrLf & vbCrLf & "Не забудьте забрать чек." & vbCrLf & _
                "Cохраняйте чек до момента поступления денег на счет"

        lblCheck.Text = mes
        msg("Был произведен платеж с чеком: " & vbCrLf &
            "Чек №: " & chekN & vbCrLf &
            "Сервис: " & service_id & " " & service_name & vbCrLf &
            "Номер: " & nomer & vbCrLf &
            "Принято: " & s1 & vbCrLf &
            "К зачислению: " & (s1 - s2) & vbCrLf &
            "Комиссия: " & s2)
        tblGo.Controls.Clear()
        'Dim btn2 As New Button
        'With btn2
        '    .FlatAppearance.MouseOverBackColor = Color.Transparent
        '    .FlatAppearance.MouseDownBackColor = Color.Transparent
        '    .AutoSize = True
        '    .FlatStyle = FlatStyle.Flat
        '    .FlatAppearance.BorderSize = 0
        '    .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
        '    .Dock = DockStyle.Right
        '    .BackColor = Color.Transparent
        '    AddHandler .Click, AddressOf Main_Click
        '    AddHandler .MouseDown, AddressOf Main_MouseDown
        '    AddHandler .MouseUp, AddressOf Main_MouseUp
        '    AddHandler .MouseLeave, AddressOf Main_MouseLeave
        'End With
        'tblGo.Controls.Add(btn2, 0, 0)
        With cmdNext
            .FlatAppearance.MouseOverBackColor = Color.Transparent
            .FlatAppearance.MouseDownBackColor = Color.Transparent
            .AutoSize = True
            .FlatStyle = FlatStyle.Flat
            .FlatAppearance.BorderSize = 0
            .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_check.png")
            .Dock = DockStyle.None
            .BackColor = Color.Transparent
            .Name = "cmdNext"
            '.Enabled = False
            AddHandler .Click, AddressOf Check_Click
            AddHandler .MouseDown, AddressOf Check_MouseDown
            AddHandler .MouseUp, AddressOf Check_MouseUp
            AddHandler .MouseLeave, AddressOf Check_MouseLeave
        End With
        tblGo.Controls.Add(cmdNext, 1, 0)
        lblStatus.Text = StatusMessage(langInt)
    End Sub

    Private Sub getChek()
        Dim date_time As String = Date.Now
        
        Dim printer As CustomPrint
        printer = New CustomPrint

        Dim a As Integer
        Dim arr = New String(15) {}

        arr(0) = GlobalVars.Organisation
        arr(1) = set_50("-", "-", "-")
        arr(2) = set_50("ИНН", "510004229", " ")
        arr(3) = set_50("Терминал №", GlobalVars.terminal_id, " ")
        arr(4) = set_50("Адрес терминала: ", "", " ")
        arr(5) = set_50("", GlobalVars.TerminalAddress(0), " ")
        arr(6) = set_50("Дата платежа", date_time, " ")
        arr(7) = set_50("Чек №", chekN, " ")
        arr(8) = set_50("Сервис", service_name, " ")
        arr(9) = set_50("Счет №", nomer, " ")
        arr(10) = set_50("Принято", s1 & " Сомони", " ")
        arr(11) = set_50("К зачислению", (s1 - s2).ToString & " Сомони", " ")
        arr(12) = set_50("Коммисия", s2.ToString + " Сомони", " ")
        arr(13) = set_50("Call-Center", GlobalVars.callcenter, " ")
        arr(14) = set_50(" ", " ", " ")
        arr(15) = set_50("Спасибо за платеж!", "", " ")

        Try
            printer.setPrintFont(New Font("Courier", 8, FontStyle.Bold))
            a = printer.Print(arr)  ' If there is no installed printers returns 210
            '                       ' If OK printers returns 201
            a = GlobalVars.GetPrinterState(a)
        Catch ex As Exception
            msg("ERROR frmChek: " & ex.Message & " Error occured in: " & ex.TargetSite.ToString())
        End Try
        
        If (GlobalVars.lastStPrinter <> a) Then
            Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change,status) values('printer','" & a & "',UNIX_TIMESTAMP(),0)"
            Execute(sql1)
            GlobalVars.lastStPrinter = a
        End If
    End Sub

    Public Function set_50(ByVal str1 As String, ByVal str2 As String, ByVal str3 As String) As String
        Dim str As String
        Dim len2 As Integer = str2.Length
        For i As Integer = str1.Length To 31 - len2
            str1 = str1 & str3
        Next
        str = str1 & str2
        Return str
    End Function

#Region "Процедуры"

    '''''''''''''''''''Main
    Private Sub Main_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            frmMain.Activate()
            If frmGroup.Visible Then frmGroup.Close()
            If frmPay.Visible Then frmPay.Close()
            frmService.Close()
            group_id = 0
            last_group_id = 0
            service_id = 0
            GlobalVars.Terminal = 301
            Me.Close()
        Catch ex As Exception
            msg("ERROR frmChek: " & ex.Message)
        End Try
    End Sub

    Private Sub Main_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main_off.png")
        Catch ex As Exception
            msg("ERROR frmChek: " & ex.Message & " Error occured in: " & ex.TargetSite.ToString())
        End Try
    End Sub

    Private Sub Main_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
        Catch ex As Exception
            msg("ERROR frmChek: " & ex.Message & " Error occured in: " & ex.TargetSite.ToString())
        End Try
    End Sub

    Private Sub Main_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
        Catch ex As Exception
            msg("ERROR frmChek: " & ex.Message & " Error occured in: " & ex.TargetSite.ToString())
        End Try
    End Sub

    ''''''''''''Check
    Private Sub Check_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        sender.enabled = False
        getChek()
        Main_Click(sender, e)
    End Sub

    Private Sub Check_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_check_off.png")
        Catch ex As Exception
            msg("ERROR frmChek: " & ex.Message & " Error occured in: " & ex.TargetSite.ToString())
        End Try
    End Sub

    Private Sub Check_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_check.png")
        Catch ex As Exception
            msg("ERROR frmChek: " & ex.Message & " Error occured in: " & ex.TargetSite.ToString())
        End Try
    End Sub

    Private Sub Check_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_check.png")
        Catch ex As Exception
            msg("ERROR frmChek: " & ex.Message & " Error occured in: " & ex.TargetSite.ToString())
        End Try
    End Sub

#End Region

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Main_Click(sender, e)
    End Sub
End Class