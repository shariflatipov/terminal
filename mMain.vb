﻿Imports System.Security.Cryptography
Imports System.Text
Imports MySql.Data.MySqlClient
Imports System.IO
Imports MySql
Imports System.Data
Imports System.Threading
Imports Microsoft.PointOfService

Module mMain
    Public dataAdapter As New MySql.Data.MySqlClient.MySqlDataAdapter
    Public ds As DataSet

    Public GlobalVars As New GlobalVar
    Public ccode As New CashCode()

    Public arrLang() As String = {"rus", "eng", "taj"}
    Public lang As String = "rus"
    Public langInt As Integer = 0

    Public last_group_id As Integer
    Public group_id As Integer
    Public group_name As String
    Public service_id As Integer
    Public service_name As String

    Public web_server As String
    Public connecting As Boolean = False
    Public strConnection As String = ""
    Public meClosing As Boolean = False
    Public nomConnecting As Integer = 0

    Public StatusMessage(2) As String
    Public SelectCodeMessage(2) As String

    Public _isDetected As Boolean = False

    Public public_key As String = "asdfghjk"
    Public CL As New Client()

    Public _printer As PosPrinter
    Public _pos As PosCommon
    Public posExplorer As PosExplorer

    Public Sub msg(ByVal Mesage As String, Optional ByVal LogLevel As Integer = 1)
        If LogLevel <= GlobalVars.LogLevel Then
            Try
                If GlobalVars.LastMSG <> Mesage Then
                    Dim dirName As String = Application.StartupPath & "/Logs/" & Now.Year & getCifr(Now.Month.ToString, 2)
                    If Directory.Exists(dirName) = False Then Directory.CreateDirectory(dirName)
                    Dim fFile As Short
                    fFile = FreeFile()
                    Dim logfile As String = dirName & "/" & Now.Year.ToString & getCifr(Now.Month.ToString, 2) & getCifr(Now.Day, 2) & ".txt"
                    FileOpen(fFile, logfile, OpenMode.Append)
                    PrintLine(fFile, strTime(2) & " " & Mesage)

                    FileClose(fFile)
                    GlobalVars.LastMSG = Mesage
                End If
            Catch ex As Exception
                msg(ex.Message)
                msg(Mesage)
            End Try
        End If
    End Sub

    Private Function strTime(Optional ByVal f As Integer = 0) As String
        Select Case f
            Case 0
                Return Now
            Case 1
                Return Now.Day & "." & Now.Month & " " & Now.Hour & ":" & Now.Minute & ":" & Now.Second & "." & Now.Millisecond
            Case 2
                Return getCifr(Now.Day.ToString, 2) & "." & getCifr(Now.Month.ToString, 2) & " " & getCifr(Now.Hour, 2) & ":" & getCifr(Now.Minute, 2) & ":" & getCifr(Now.Second, 2) & "." & getCifr(Now.Millisecond.ToString, 3)
        End Select
        Return ""
    End Function

    Public Sub msg2(ByVal Mesage As String, Optional ByVal LogLevel As Integer = 1)
        If LogLevel <= GlobalVars.LogLevel Then
            Try
                If GlobalVars.LastMSG <> Mesage Then
                    Dim dirName As String = Application.StartupPath & "/Logs/" & Now.Year & getCifr(Now.Month.ToString, 2)
                    If Directory.Exists(dirName) = False Then Directory.CreateDirectory(dirName)
                    Dim fFile As Short
                    fFile = FreeFile()
                    FileOpen(fFile, dirName & "/" & Now.Year.ToString & getCifr(Now.Month.ToString, 2) & getCifr(Now.Day, 2) & "_BV.txt", OpenMode.Append)
                    PrintLine(fFile, Now & " " & Mesage)
                    FileClose(fFile)
                    GlobalVars.LastMSG = Mesage
                End If
            Catch ex As Exception
                msg(ex.Message)
                msg(Mesage)
            End Try
        End If
    End Sub

    Public Function getCifr(ByVal str As String, ByVal col As Integer) As String
        For i As Integer = str.Length To col - 1
            str = "0" & str
        Next
        Return str
    End Function

    Public Sub main(ByVal args As Object)
        'Application.ThreadException += New ThreadExceptionEventHandler(Application_ThreadException)
        'Application.SetUnhandledExceptionMode(UnhandledExceptionMode.ThrowException)

        Application.SetUnhandledExceptionMode(UnhandledExceptionMode.CatchException)
        AddHandler Application.ThreadException, AddressOf OnThreadException

        Application.Run(New frmMain)
    End Sub

    Private Sub OnThreadException(ByVal sender As Object, ByVal e As ThreadExceptionEventArgs)
        msg(e.Exception.Message)
    End Sub

    Public connString As String = "server=localhost" & _
                                  ";user id=root" & _
                                  ";Password=123" & _
                                  ";port=3306" & _
                                  ";database=terminal" & _
                                  ";Pooling=True" & _
                                  ";Max Pool Size=200" & _
                                  ";Min Pool Size=0" & _
                                  ";Connection Lifetime=0" & _
                                  ";treat blobs as utf8=True"

    Public Function Connect() As MySql.Data.MySqlClient.MySqlConnection
        Dim conn As MySql.Data.MySqlClient.MySqlConnection = New MySql.Data.MySqlClient.MySqlConnection(connString)
        Try
            Return conn
        Catch ex As Exception
            msg(ex.Message)
            Return New MySql.Data.MySqlClient.MySqlConnection
        End Try
    End Function

    Public Function GetTable(ByVal sql As String) As DataTable
        Dim dt As New DataTable
        If sql = "" Then Return dt
        Dim mys As MySql.Data.MySqlClient.MySqlConnection = Connect()

        Try
            Dim myDataAdapter As New MySql.Data.MySqlClient.MySqlDataAdapter
            If mys.State <> ConnectionState.Closed Then mys.Close()
            mys.Open()
            myDataAdapter.SelectCommand = New MySql.Data.MySqlClient.MySqlCommand(sql, mys)
            myDataAdapter.Fill(dt)
        Catch ex As Exception
            msg(ex.Message & " " & sql & " Error ocured in: " & ex.TargetSite.ToString)
        Finally
            If mys.State <> ConnectionState.Closed Then
                mys.Close()
            End If
        End Try
        Return dt
    End Function

    Public Function GetSingle(ByVal sql As String)
        If sql = "" Then Return ""
        Dim result As String
        Try
            Dim mys As MySqlConnection = Connect()
            If mys.State <> ConnectionState.Closed Then mys.Close()
            mys.Open()
            Dim mysqlCommand As New MySqlCommand(sql, mys)
            result = mysqlCommand.ExecuteScalar().ToString()
            mys.Close()
            Return result
        Catch ex As Exception
            msg("Exception: " & ex.ToString & "; Exception message:" & ex.Message & "; Executed sql: " & sql & "; Error ocured in: " & ex.TargetSite.ToString)
        End Try
        Return 0
    End Function

    Public Function Execute(ByVal sql As String, Optional ByVal ret As Boolean = False) As Integer
        If sql = "" Then Return 0
        Try
            Dim mys As MySql.Data.MySqlClient.MySqlConnection = Connect()
            If mys.State <> ConnectionState.Closed Then mys.Close()
            mys.Open()
            Dim mysqlCommand As New MySql.Data.MySqlClient.MySqlCommand(sql, mys)
            mysqlCommand.ExecuteNonQuery()
            Dim InsID As Integer = mysqlCommand.LastInsertedId
            mys.Close()
            Return InsID
        Catch ex As Exception
            msg("Exception: " & ex.ToString & "; Exception message:" & ex.Message & "; Executed sql: " & sql & "; Error ocured in: " & ex.TargetSite.ToString)
        End Try
        Return 0
    End Function

    Public Function tarif_text(ByVal LangID As Integer, ByVal Lang As String, ByVal operId As Integer) As String
        Dim arrInfo As String() = {
                                 "Комиссия за платеж: 0%",
                                 "Comission for payment: 0%",
                                 "Ҳаққи хизмат: 0%"}
        Dim s As String = arrInfo(LangID)
        Dim tPlanId As Integer
        Dim sql As String =
        "SELECT " +
        " 	* " +
        " FROM " +
        " 	proc_tarifplan " & _
        " WHERE " +
        " 	date_begin<=UNIX_TIMESTAMP() AND  " & _
        "   IF(date_end='null',UNIX_TIMESTAMP(),IF(date_end='',UNIX_TIMESTAMP(),date_end))>=UNIX_TIMESTAMP() " & _
        " LIMIT 1"
        Dim dt As DataTable = GetTable(sql)

        If (dt.Rows.Count > 0) Then
            'System.out.println("Нашли тарифный план");
            tPlanId = dt.Rows(0).Item("id")
            sql = " SELECT " & _
              Lang & _
              " FROM " & _
              " 	proc_tarif " & _
              " WHERE " & _
              " 	op_service_id = " & operId & " AND  " & _
              "   tarif_plan_id = " & tPlanId & _
              " LIMIT 1"
            Dim dt2 As DataTable = GetTable(sql)
            'ResultSet rs1 = GlobalVars.st.executeQuery(sql);
            If (dt2.Rows.Count > 0) Then
                If (dt2.Rows(0).Item(Lang) <> "") Then s = dt2.Rows(0).Item(Lang)
            End If
        End If
        Return s
    End Function

    Public Function calc_tarif(ByVal summa As Integer, ByVal operId As Integer) As Double
        Dim s As Double = 0
        Dim tPlanId, tarifId As Integer
        Dim sql As String =
        "SELECT " & _
        " 	* " & _
        " FROM " & _
        " 	proc_tarifplan " & _
        " WHERE " & _
        " 	date_begin<=UNIX_TIMESTAMP() AND  " & _
        "   IF(date_end='null',UNIX_TIMESTAMP(),IF(date_end='',UNIX_TIMESTAMP(),date_end))>=UNIX_TIMESTAMP() " & _
        " LIMIT 1"
        Dim dt As DataTable = GetTable(sql)
        If (dt.Rows.Count > 0) Then
            'System.out.println("Нашли тарифный план");
            tPlanId = dt.Rows(0).Item("id")
            sql = " SELECT " & _
              " 	* " & _
              " FROM " & _
              " 	proc_tarif " & _
              " WHERE " & _
              " 	op_service_id = " & operId & " AND  " & _
              "   tarif_plan_id = " & tPlanId & _
              " LIMIT 1"
            Dim dt2 As DataTable = GetTable(sql)
            If (dt2.Rows.Count > 0) Then
                tarifId = dt2.Rows(0).Item("id")
                If (dt2.Rows(0).Item("prc").Equals("True")) Then ' Расчет по процентам
                    s = summa * dt2.Rows(0).Item("summa") / 100
                    ' Если сумма переодпределена, то следует прибавить и добавочную комиссию
                    If (dt2.Rows(0).Item("summa_own") > 0) Then s = s + (summa * dt2.Rows(0).Item("summa_own") / 100)
                    If (s < dt2.Rows(0).Item("min") And dt2.Rows(0).Item("min") <> 0) Then s = dt2.Rows(0).Item("min")
                    If (s > dt2.Rows(0).Item("max") And dt2.Rows(0).Item("max") <> 0) Then s = dt2.Rows(0).Item("max")
                Else
                    If (dt2.Rows(0).Item("summa") > 0) Then
                        s = dt2.Rows(0).Item("summa") 'Расчет по )сумме
                        'System.out.println("Расчет по сумме");
                        ' Если сумма передопределена, то следует прибавить и добавочную комиссию                
                        If (dt2.Rows(0).Item("summa_own") > 0) Then s = s + dt2.Rows(0).Item("summa_own")
                    Else  ' Если сумма 0 тариф вычисляется по массиву сумм тарифа
                        ' 1.Сначала рассчитаем комисию по базовому (унаследованному) массиву                
                        Try
                            Dim dt3 As DataTable = GetTable(
                                                      " SELECT " & _
                                                      " 	prc,summa " & _
                                                      " FROM " & _
                                                      " 	proc_tarifarr " & _
                                                      " WHERE " & _
                                                      " 	min <= " & summa & " AND  " & _
                                                      "   max >= " & summa & " AND " & _
                                                      "   tarif_id = " & tarifId & " AND " & _
                                                      "	parent='True'" & _
                                                      " LIMIT 1"
                                                     )
                            If (dt3.Rows.Count > 0) Then
                                'System.out.println("1.Сначала рассчитаем комисию по базовому (унаследованному) массиву");
                                If (dt3.Rows(0).Item("prc").Equals("True")) Then
                                    'System.out.println("Расчет по процентам");
                                    s = summa * dt3.Rows(0).Item("summa") / 100
                                Else
                                    'System.out.println("Расчет по сумме");
                                    s = dt3.Rows(0).Item("summa")
                                End If
                            End If
                        Catch ex As Exception

                        End Try
                        ' 2.Теперь прибавим добавочную комисию (praent = False)                
                        Try
                            Dim dt3 As DataTable = GetTable(
                                                   " SELECT " & _
                                                   " 	prc,summa " & _
                                                   " FROM " & _
                                                   " 	proc_tarifarr " & _
                                                   " WHERE " & _
                                                   " 	min <= " & summa & " AND  " & _
                                                   "   max >= " & summa & " AND " & _
                                                   "   tarif_id = " & tarifId & " AND " & _
                                                   "	parent='False' " & _
                                                   " LIMIT 1"
                                                  )
                            If (dt3.Rows.Count > 0) Then
                                If (dt3.Rows(0).Item("prc").Equals("True")) Then
                                    'System.out.println("Расчет по процентам")
                                    s = summa * dt3.Rows(0).Item("summa") / 100
                                Else
                                    'System.out.println("Расчет по сумме")
                                    s = dt3.Rows(0).Item("summa")
                                End If
                            End If
                        Catch ex As Exception

                        End Try
                    End If
                End If
            End If
        End If
        Return s
    End Function

    Public Function GetMd5Hash(ByVal md5Hash As MD5, ByVal input As String) As String

        ' Convert the input string to a byte array and compute the hash.
        Dim data As Byte() = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input))

        ' Create a new Stringbuilder to collect the bytes
        ' and create a string.
        Dim sBuilder As New StringBuilder()

        ' Loop through each byte of the hashed data 
        ' and format each one as a hexadecimal string.
        Dim i As Integer
        For i = 0 To data.Length - 1
            sBuilder.Append(data(i).ToString("x2"))
        Next i

        ' Return the hexadecimal string.
        Return sBuilder.ToString()

    End Function 'GetMd5Hash


    ' Verify a hash against a string.
    Public Function VerifyMd5Hash(ByVal md5Hash As MD5, ByVal input As String, ByVal hash As String) As Boolean
        ' Hash the input.
        Dim hashOfInput As String = GetMd5Hash(md5Hash, input)

        ' Create a StringComparer an compare the hashes.
        Dim comparer As StringComparer = StringComparer.OrdinalIgnoreCase

        If 0 = comparer.Compare(hashOfInput, hash) Then
            Return True
        Else
            Return False
        End If

    End Function

    Public Sub show_browser(ByVal address As String, ByVal caption As String)
        address = web_server & address
        If String.IsNullOrEmpty(address) Then Return
        If address.Equals("about:blank") Then Return
        If Not address.StartsWith("http://") And _
            Not address.StartsWith("https://") Then
            address = "http://" & address
        End If

        Try

        Catch ex As System.UriFormatException
            Return
        End Try
    End Sub

    Private key() As Byte = {}
    Private IV() As Byte = {&H12, &H34, &H56, &H78, &H90, &HAB, &HCD, &HEF}
    'Private Const EncryptionKey As String = ""

    Public Function Decrypt(ByVal stringToDecrypt As String, ByVal EncryptionKey As String) As String
        'msg(stringToDecrypt)
        Try
            Dim inputByteArray(stringToDecrypt.Length) As Byte
            key = System.Text.Encoding.UTF8.GetBytes(Microsoft.VisualBasic.Left(EncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider
            inputByteArray = Convert.FromBase64String(Trim(stringToDecrypt))
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateDecryptor(key, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Dim encoding As System.Text.Encoding = System.Text.Encoding.UTF8
            Return encoding.GetString(ms.ToArray())
        Catch ex As Exception
            msg("ERROR: 005 " & ex.Message)
            Return "0"
        End Try
    End Function

    Public Function Encrypt(ByVal stringToEncrypt As String, ByVal EncryptionKey As String) As String
        Try
            key = System.Text.Encoding.UTF8.GetBytes(Microsoft.VisualBasic.Left(EncryptionKey, 8))
            Dim des As New DESCryptoServiceProvider
            Dim inputByteArray() As Byte = Encoding.UTF8.GetBytes(stringToEncrypt)
            Dim ms As New MemoryStream
            Dim cs As New CryptoStream(ms, des.CreateEncryptor(key, IV), CryptoStreamMode.Write)
            cs.Write(inputByteArray, 0, inputByteArray.Length)
            cs.FlushFinalBlock()
            Return Convert.ToBase64String(ms.ToArray())
        Catch ex As Exception
            'oops - add your exception logic
            Return "0"
        End Try
    End Function

    Public Sub GetTerminalSettings()
        Dim rs As DataTable
        Try
            rs = GetTable("select * from settings;")
            If rs.Rows.Count = 0 Then
                Execute("repair table settings;")
            Else
                For i As Integer = 0 To rs.Rows.Count - 1
                    Select Case rs.Rows(i).Item("variable")
                        Case "terminal_id"
                            GlobalVars.terminal_id = rs.Rows(i).Item("value")
                        Case "server_file"
                            GlobalVars.server_file = rs.Rows(i).Item("value")
                        Case "server"
                            GlobalVars.server_adr = rs.Rows(i).Item("value")
                        Case "server_port"
                            GlobalVars.server_port = rs.Rows(i).Item("value")
                        Case "inkass_id"
                            GlobalVars.inkass_id = rs.Rows(i).Item("value")
                        Case "inkass_n"
                            GlobalVars.inkass_n = rs.Rows(i).Item("value")
                        Case "com_n"
                            GlobalVars.com_n = rs.Rows(i).Item("value")
                        Case "inkass_code"
                            GlobalVars.inkass_code = rs.Rows(i).Item("value")
                        Case "adress_rus"
                            GlobalVars.TerminalAddress(0) = rs.Rows(i).Item("value")
                        Case "adress_eng"
                            GlobalVars.TerminalAddress(1) = rs.Rows(i).Item("value")
                        Case "adress_taj"
                            GlobalVars.TerminalAddress(2) = rs.Rows(i).Item("value")
                        Case "organisation"
                            GlobalVars.Organisation = rs.Rows(i).Item("value")
                        Case "agent_login"
                            GlobalVars.agent_login = rs.Rows(i).Item("value")
                        Case "agent_pass"
                            GlobalVars.agent_pass = rs.Rows(i).Item("value")
                        Case "cash_count"
                            GlobalVars.BillCount = rs.Rows(i).Item("value")
                        Case "cash_summ"
                            GlobalVars.BillSumm = rs.Rows(i).Item("value")
                        Case "terminal_state"
                            GlobalVars.stTerminal = rs.Rows(i).Item("value")
                        Case "call_center"
                            GlobalVars.callcenter = rs.Rows(i).Item("value")
                        Case "KillExplorer"
                            GlobalVars.KillExplorer = rs.Rows(i).Item("value") <> "0"
                        Case "BlockTerminal"
                            GlobalVars.BlockTerminal = rs.Rows(i).Item("value") <> "0"
                    End Select
                Next
            End If
        Catch ex As Exception
            Execute("repair table settings;")
        End Try
    End Sub
End Module
