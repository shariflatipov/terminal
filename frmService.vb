﻿Public Class frmService
    Public OperMask As String
    Public selectedMask As String
    Dim OperMaskGroup As String 'групповая маска

    Dim count As Integer = -1
    'Dim Massive(12) As PictureBox
    Dim Massive(12) As Button
    Dim cmdNext As New Button
    Dim isNomClicked As Boolean = False

    'Dim maskDict As New Dictionary(Of String, Integer)
    Dim maskDict As New Dictionary(Of String, StructOpservice)
    Public msisdn As String   'вводимый номер

    Private Sub frmService_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        'Костыль Костыль Костыль Костыль Костыль

        If service_id = 50 Then
            msisdn = 201
            txtNumber.Text = 201
        Else
            msisdn = ""
        End If

        'Костыль Костыль Костыль Костыль Костыль

        Try
            pbLogo.BackgroundImage = Image.FromFile(Application.StartupPath & "/skins/logo.png")
        Catch ex As Exception
            msg("Error: " & ex.ToString & " Error occured in: " & ex.TargetSite.Name)
        End Try

        'tblTitle.BackgroundImage = Image.FromFile(Application.StartupPath & "/skins/images/caption.png")
        Dim sql_opser = "Select id_operator, mask, name, mask_length from OpService where state=1"
        Dim struct As StructOpservice
        Dim tMask As String
        'выбрана групповая услуга считываем дочерние
        If group_id <> 0 Then
            Try
                pbService.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & group_id & "_m.png")
                pbWarning.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & group_id & "_m.png")
            Catch ex As Exception
                msg("Error: " & ex.ToString & " Error occured in: " & ex.TargetSite.Name)
            End Try
            Dim dt As DataTable = GetTable(sql_opser & " and type=" & group_id)

            'Прочитаем все маски в двумерный Dictionary где первый маска второй id услуги
            Dim opserCount = dt.Rows.Count 'Количсетво дочерних услуг

            For i As Integer = 0 To opserCount - 1
                tMask = dt.Rows(i).Item("mask")

                struct.name = dt.Rows(i).Item("name")
                struct.maskLength = dt.Rows(i).Item("mask_length")
                struct.id = dt.Rows(i).Item("id_operator")
                While tMask.IndexOf(";") <> -1
                    Dim b = tMask.IndexOf(";")
                    Dim s As String = tMask.Substring(0, b)

                    'maskDict.Add(s, idOpser)
                    maskDict.Add(s, struct)
                    tMask = tMask.Substring(tMask.IndexOf(";") + 1)
                End While
                If maskDict.ContainsKey(tMask) = False Then
                    maskDict.Add(tMask, struct)
                End If
            Next i

            service_name = group_name
        Else
            Try
                pbService.Image = Image.FromFile(Application.StartupPath & "/skins/oper_logos/" & service_id & "_m.png")
                pbWarning.Image = Image.FromFile(Application.StartupPath & "/skins/oper_logos/" & service_id & "_m.png")
            Catch ex As Exception
                msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
            End Try
            Dim dt As DataTable = GetTable("Select id_operator, mask, name, mask_length from OpService where id_operator=" & service_id)
            If dt.Rows.Count > 0 Then
                struct.name = dt.Rows(0).Item("name")
                struct.maskLength = dt.Rows(0).Item("mask_length")
                struct.id = dt.Rows(0).Item("id_operator")
                tMask = dt.Rows(0).Item("mask")
                Do
                    Dim b = tMask.IndexOf(";")
                    Dim s As String
                    If b <> -1 Then
                        s = tMask.Substring(0, b)
                    Else
                        Exit Do
                    End If

                    maskDict.Add(s, struct)
                    tMask = tMask.Substring(tMask.IndexOf(";") + 1)
                Loop While tMask.IndexOf(";") <> -1
                maskDict.Add(tMask, struct)

                service_name = struct.name
            End If
        End If

        If (maskDict.Count > 0) Then
            tMask = maskDict.First.Key
            selectedMask = tMask
            Dim nMask As String = getMaskFromString(tMask)
            txtNumber.Mask = nMask
            OperMaskGroup = nMask
            OperMask = nMask
            'lblNomer.Text = nMask

            fillNumPad()

            'tblGo.Controls.Clear()
        End If
        Try
            Dim btn1 As New Button
            With btn1
                .FlatAppearance.MouseOverBackColor = Color.Transparent
                .FlatAppearance.MouseDownBackColor = Color.Transparent
                .AutoSize = True
                .FlatStyle = FlatStyle.Flat
                .FlatAppearance.BorderSize = 0
                .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back.png")
                .Dock = DockStyle.Right
                .BackColor = Color.Transparent
                AddHandler .Click, AddressOf Back_Click
                AddHandler .MouseDown, AddressOf Back_MouseDown
                AddHandler .MouseUp, AddressOf Back_MouseUp
            End With
            'tblGo.Controls.Add(btn1, 0, 0)
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
        'Try
        '    Dim btn2 As New Button
        '    With btn2
        '        .FlatAppearance.MouseOverBackColor = Color.Transparent
        '        .FlatAppearance.MouseDownBackColor = Color.Transparent
        '        .AutoSize = True
        '        .FlatStyle = FlatStyle.Flat
        '        .FlatAppearance.BorderSize = 0
        '        .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
        '        .Dock = DockStyle.None
        '        .BackColor = Color.Transparent
        '        AddHandler .Click, AddressOf Main_Click
        '        AddHandler .MouseDown, AddressOf Main_MouseDown
        '        AddHandler .MouseUp, AddressOf Main_MouseUp
        '    End With
        '    tblGo.Controls.Add(btn2, 1, 0)
        'Catch ex As Exception
        '    msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        'End Try
        Try
            With cmdNext
                .FlatAppearance.MouseOverBackColor = Color.Transparent
                .FlatAppearance.MouseDownBackColor = Color.Transparent
                .AutoSize = True
                .FlatStyle = FlatStyle.Flat
                .FlatAppearance.BorderSize = 0
                .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_next_dis.png")
                .Dock = DockStyle.Left
                .BackColor = Color.Transparent
                .Name = "cmdNext"
                .Enabled = False
                AddHandler .Click, AddressOf Next_Click
                AddHandler .MouseDown, AddressOf Next_MouseDown
                AddHandler .MouseUp, AddressOf Next_MouseUp
            End With
            'tblGo.Controls.Add(cmdNext, 2, 0)
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
        lblStatus.Text = StatusMessage(langInt)
        'Massive(1).Focus()

    End Sub

#Region "Построение интерфейса"

    Private Sub fillNumPad()
        'Заполняет num pad

        For i As Integer = 1 To 9
            Dim column As Integer = (i Mod 3) - 1
            Dim row As Integer = CInt(i \ 3) '+ 1
            'MsgBox("i=" & i & "; row=" & row & "; column=" & column)
            'Massive(i) = New PictureBox
            Massive(i) = New Button
            With Massive(i)
                .FlatAppearance.MouseOverBackColor = Color.Transparent
                .FlatAppearance.MouseDownBackColor = Color.Transparent
                .Size = New Size(200, 110)
                .FlatStyle = FlatStyle.Flat
                .FlatAppearance.BorderSize = 0
                .Tag = i
                .Image = Image.FromFile(Application.StartupPath & "/skins/images/pad_" & .Tag & ".png")
                .BackColor = Color.Transparent
                AddHandler .Click, AddressOf NumPad_Click
                AddHandler .MouseDown, AddressOf NumPad_MouseDown
                AddHandler .MouseUp, AddressOf NumPad_MouseUp
                'AddHandler .MouseLeave, AddressOf NumPad_MouseLeave
            End With
            tblNumPad.Controls.Add(Massive(i), column, row)
        Next


        ' Button 0
        Massive(0) = New Button
        With Massive(0)
            .FlatAppearance.MouseOverBackColor = Color.Transparent
            .FlatAppearance.MouseDownBackColor = Color.Transparent
            .Size = New Size(200, 110)
            .FlatStyle = FlatStyle.Flat
            .FlatAppearance.BorderSize = 0
            .Tag = 0
            .Image = Image.FromFile(Application.StartupPath & "/skins/images/pad_0.png")
            .BackColor = Color.Transparent
            AddHandler .Click, AddressOf NumPad_Click
            AddHandler .MouseDown, AddressOf NumPad_MouseDown
            AddHandler .MouseUp, AddressOf NumPad_MouseUp
            'AddHandler .MouseLeave, AddressOf NumPad_MouseLeave
        End With
        tblNumPad.Controls.Add(Massive(0), 1, 3)

        ' Button backspace
        Massive(10) = New Button()
        With Massive(10)
            .FlatAppearance.MouseOverBackColor = Color.Transparent
            .FlatAppearance.MouseDownBackColor = Color.Transparent
            .Size = New Size(110, 90)
            .FlatStyle = FlatStyle.Flat
            .FlatAppearance.BorderSize = 0
            .Tag = 10
            .Margin = New Padding(0, 10, 0, 0)
            .Image = Image.FromFile(Application.StartupPath & "/skins/images/pad_10.png")
            .BackColor = Color.Transparent
            AddHandler .Click, AddressOf NumPad_Click
            AddHandler .MouseDown, AddressOf NumPad_MouseDown
            AddHandler .MouseUp, AddressOf NumPad_MouseUp
            'AddHandler .MouseLeave, AddressOf NumPad_MouseLeave
        End With
        tblTitle.Controls.Add(Massive(10), 2, 0)

        ' Button clear
        Massive(11) = New Button
        With Massive(11)
            .FlatAppearance.MouseOverBackColor = Color.Transparent
            .FlatAppearance.MouseDownBackColor = Color.Transparent
            .Size = New Size(110, 90)
            .FlatStyle = FlatStyle.Flat
            .FlatAppearance.BorderSize = 0
            .Tag = 11
            .Margin = New Padding(0, 10, 10, 0)
            .Image = Image.FromFile(Application.StartupPath & "/skins/images/pad_11.png")
            .BackColor = Color.Transparent
            AddHandler .Click, AddressOf NumPad_Click
            AddHandler .MouseDown, AddressOf NumPad_MouseDown
            AddHandler .MouseUp, AddressOf NumPad_MouseUp
            'AddHandler .MouseLeave, AddressOf NumPad_MouseLeave
        End With
        tblTitle.Controls.Add(Massive(11), 3, 0)
    End Sub

#End Region

#Region "Процедуры"

    Private Sub Back_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        service_id = 0
        last_group_id = 0
        If group_id = 0 Then
            GlobalVars.getCashCode = True
            GlobalVars.Terminal = 301
        Else
            frmGroup.Timer1.Start()
            GlobalVars.Terminal = 305
        End If
        Me.Close()
    End Sub

    Private Sub Back_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back_off.png")
    End Sub

    Private Sub Back_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back.png")
    End Sub

    '''''''''''''''''''Main
    Private Sub Main_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If frmGroup.Visible Then frmGroup.Close()
        group_id = 0
        last_group_id = 0
        service_id = 0
        GlobalVars.Terminal = 301
        GlobalVars.getCashCode = True
        Me.Close()
    End Sub

    Private Sub Main_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main_off.png")
    End Sub

    Private Sub Main_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
    End Sub

    ''''''''''''Next
    Private Sub Next_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Timer1.Stop()
        If (txtNumber.Text.Equals(GlobalVars.inkass_code)) Then
            frmLogin.Show(Me)
            GlobalVars.Terminal = 308
        Else
            GlobalVars.Terminal = 307
            Dim a As String
            If txtNumber.Text.Contains(" ") Then
                a = txtNumber.Text.Substring(0, (txtNumber.Text.IndexOf(" ") - 1))
            Else
                a = txtNumber.Text
            End If

            Dim num As String = a
            Dim idxStar As Integer = num.IndexOf("*")
            If idxStar <> -1 Then
                'удаляем все звездочки
                num = num.Substring(0, idxStar)
                If num.EndsWith("-") Then num = num.Substring(0, idxStar - 1)
            End If
            frmPay.lblNomer.Text = num
            frmPay.Show(Me)
        End If
    End Sub

    Private Sub Next_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_next_off.png")
    End Sub

    Private Sub Next_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_next.png")
    End Sub

    ''''''''''''NumPad
    Private Sub NumPad_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

        Select Case sender.tag
            Case 0 To 9
                msisdn = msisdn & sender.tag.ToString()
                Dim chkNum As Boolean = chkEnterNum(msisdn)
                If chkNum Then
                    txtNumber.Text = msisdn
                Else
                    'Выводим форму выбора кодов
                    frmCheckCode.maskDict = maskDict
                    frmCheckCode.ShowDialog()
                End If

            Case 10
                If (msisdn.Length > 0) Then
                    If txtNumber.MaskFull Then
                        enableNumPadState(True)
                    End If
                    msisdn = msisdn.Substring(0, msisdn.Length - 1)
                    txtNumber.Text = msisdn
                End If
            Case 11
                enableNumPadState(True)
                msisdn = ""
                txtNumber.Text = msisdn
        End Select
        If txtNumber.MaskFull Then
            enableNumPadState(False)
        End If
        If maskDict.Item(selectedMask).maskLength <= msisdn.Length Then
            enableBtnNext(True)
        Else
            enableBtnNext(False)
        End If
        isNomClicked = True
    End Sub

    Private Sub NumPad_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/images/pad_" & sender.Tag & "_off.png")
    End Sub

    Private Sub NumPad_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/images/pad_" & sender.Tag & ".png")
    End Sub

    Private Sub NumPad_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/images/pad_" & sender.Tag & ".png")
    End Sub

    Private Sub NumPad_MouseLeave2(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/images/pad_" & sender.Tag & "_dis.png")
    End Sub
#End Region

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If isNomClicked Then
            isNomClicked = False
        Else
            Main_Click(sender, e)
        End If
    End Sub
     'Проверяет нажатую клавишу на соответствие в масках
    Private Function chkEnterNum(ByVal strNum As String)
        Dim otv As Boolean = True
        'Dim lNum As Integer = strNum.Length
        Dim tmpMask As String ', tmpNum As String
        'Dim startPos As Integer = strNum.IndexOf("*")

        'If startPos = -1 Then startPos = lNum
        'tmpNum = strNum.Substring(0, startPos).Replace("-", "")
        For Each mask In maskDict
            tmpMask = mask.Key.Replace("-", "")
            tmpMask = tmpMask.Substring(0, tmpMask.IndexOf("*"))

            If strNum.Length >= tmpMask.Length Then
                If strNum.Substring(0, tmpMask.Length) = tmpMask Then
                    otv = True
                    txtNumber.Text = mask.Key
                    OperMask = getMaskFromString(mask.Key)
                    selectedMask = mask.Key
                    Exit For
                End If
            ElseIf strNum = tmpMask.Substring(0, strNum.Length) Then
                otv = True
                txtNumber.Text = mask.Key
                OperMask = getMaskFromString(mask.Key)
                selectedMask = mask.Key
                Exit For
            End If
            otv = False

        Next
        service_id = maskDict.Item(selectedMask).id
        'Меняем маску lblNumber
        'If otv = True Then
        '    lblNomer.Text = getEnteredMask(lblNomer.Text, strNum)
        'End If
        Return otv
    End Function

    'Возвращает общую маску из строки 
    Public Function getMaskFromString(ByVal tMask As String)
        Dim nMask As String = ""
        Dim lenMask As Integer = tMask.Length
        For i As Integer = 0 To lenMask - 1
            Dim l As String = tMask.Substring(i, 1)
            'If l <> "*" And l <> "-" And l <> " " Then
            If l <> "-" And l <> " " Then
                l = "a"
            End If
            nMask = nMask & l
        Next i
        Return nMask
    End Function

    'Строит квадратики для ввода номера по маске
    Private Sub GenNumField(ByVal strMask As String)
        'фиксированные переменные
        Dim lblW As Integer = 50, lblH As Integer = 75
        Dim lblX As Integer = 230, lblY As Integer = 0

        Dim lMask As Integer = strMask.Length
        Dim arrLbl(lMask) As Label
        For i As Integer = 0 To lMask - 1
            arrLbl(i) = New Label
            With arrLbl(i)
                .Size = New Size(lblW, lblH)
                .Location = New System.Drawing.Point(lblX, lblY)
                .BackColor = Color.White
                .ForeColor = Color.Navy
            End With
            lblX = lblX + 60
            tblTitle.Controls.Add(arrLbl(i))
            tblTitle.Refresh()
        Next
    End Sub

    Public Structure StructOpservice
        Public name As String
        Public maskLength As Integer
        Public id As Integer
        'Public mask As Dictionary(Of String, Integer)
    End Structure

    Function LetterCount(ByVal word As String, ByVal letter As Char) As Integer
        Dim counter As Integer = 0
        For i As Integer = 0 To word.Length - 1
            If word.ToUpper.Chars(i) = letter Then
                counter += 1
            End If
        Next
        Return counter
    End Function

    Private Sub enableNumPadState(ByVal isFull As Boolean)
        For j As Integer = 0 To 9
            Massive(j).Enabled = isFull
        Next
    End Sub

    Private Sub enableBtnNext(ByVal flag As Boolean)
        Dim btnType As String = ""
        If flag.Equals(False) Then
            btnType = "_dis"
        Else
            btnType = ""
        End If
        cmdNext.Enabled = flag
        cmdNext.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_next" & btnType & ".png")
    End Sub

    Private Sub txtNumber_MouseClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtNumber.MouseClick
        Massive(1).Focus()
    End Sub
End Class