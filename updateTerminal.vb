﻿Imports EnterpriseDT.Net.Ftp
Imports System.IO
Imports Newtonsoft.Json
Imports System.Threading

Public Class updateTerminal

    Dim serverAddress As String
    Dim ftpUserName As String
    Dim ftpUserPass As String
    Dim ftpFileName As String
    Dim downloadTo As String
    Dim extractTo As String

#Region "Get; Set пременных"

    Public Sub setServerAddress(ByVal _serverAddress As String)
        serverAddress = _serverAddress
    End Sub

    Public Function getServerAddress() As String
        Return serverAddress
    End Function

    Public Sub setFtpUserName(ByVal _ftpUserName As String)
        ftpUserName = _ftpUserName
    End Sub
    Public Function getFtpUserName() As String
        Return ftpUserName
    End Function

    Public Sub setFtpUserPass(ByVal _ftpUserPass As String)
        ftpUserPass = _ftpUserPass
    End Sub
    Public Function getFtpUserPass() As String
        Return ftpUserPass
    End Function

    Public Sub setFtpFileName(ByVal _ftpFileName As String)
        ftpFileName = _ftpFileName
    End Sub
    Public Function getFtpFileName() As String
        Return ftpFileName
    End Function

    Public Sub setDownloadTo(ByVal _downloadTo As String)
        If Directory.Exists(_downloadTo) = False Then
            Directory.CreateDirectory(_downloadTo)
        Else
            Dim a = Directory.GetFiles(_downloadTo)
            For Each f As String In a
                File.Delete(f)
            Next
        End If
        downloadTo = _downloadTo
    End Sub
    Public Function getdownloadTo() As String
        Return downloadTo
    End Function

    Public Sub setExtractTo(ByVal _extractTo As String)
        If Directory.Exists(_extractTo) = False Then
            Directory.CreateDirectory(_extractTo)
        Else
            Dim a = Directory.GetFiles(_extractTo)
            For Each f As String In a
                File.Delete(f)
            Next
        End If
        extractTo = _extractTo
    End Sub
    Public Function getExtractTo() As String
        Return extractTo
    End Function
#End Region

    Public Function DownloadArchive(ByVal pathFolder As String) As Boolean
        'Скачивает с ftp сервера архив
        msg("Закачка архива с ftp")
        Try
            Dim ftp As New FTPConnection
            ftp.ServerAddress = serverAddress
            ftp.UserName = ftpUserName
            ftp.Password = ftpUserPass
            ftp.Connect()

            ftp.ChangeWorkingDirectory(pathFolder)
            ftp.TransferType = FTPTransferType.BINARY

            'download a file
            ftp.DownloadFile(downloadTo, ftpFileName)

            'close the connection
            ftp.Close()
            msg("Закачка архива с ftp прошла успешно")
            Return True
        Catch ex As Exception
            msg("Ошибка при закачке архива с ftp: " + ex.Message.ToString())
            Return False
        End Try
    End Function

    Public Sub ExtracrtArchive(ByVal archivePath As String, ByVal extractPath As String)
        Dim proc As System.Diagnostics.Process = New System.Diagnostics.Process()
        proc.StartInfo.FileName = Application.StartupPath & "\7z.exe"
        Dim arg As String = "x -y " & Chr(34) & archivePath & Chr(34) & " -o" & Chr(34) & extractPath & Chr(34)
        proc.StartInfo.Arguments = arg
        proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
        proc.Start()
        proc.WaitForExit()
        msg("Обновление разархивировано")
    End Sub

    Public Sub CreateBat(ByVal extPath As String, ByVal archName As String)
        'Создает bat для 
        'extPath = extPath + "/" + archName + "/"
        archName = archName.Replace(".7z", "")
        Dim fileName As String = extPath + archName + ".txt"
        Dim batName As String = extPath + archName + ".bat"

        Dim s As String
        Dim o As New System.IO.StreamReader(fileName)
        s = o.ReadToEnd()
        o.Close()
        o = Nothing

        Dim strBat As String = "ping ya.ru -n 10" & vbCrLf
        Dim updArr As List(Of UpdateJson) = New List(Of UpdateJson)
        updArr = JsonConvert.DeserializeObject(Of List(Of UpdateJson))(s)
        Dim cnt_json As Integer = updArr.Count
        If cnt_json > 0 Then
            For i As Integer = 0 To cnt_json - 1
                If (Directory.Exists(extPath + updArr(i).getSrc())) Then
                    strBat = strBat & "xcopy /E /I /Y " & Chr(34) & extPath + updArr(i).getSrc() & Chr(34) & " " & Chr(34) & updArr(i).getDest() & Chr(34) & vbCrLf
                Else
                    strBat = strBat & "copy /Y  " & Chr(34) & extPath + updArr(i).getSrc() & Chr(34) & " " & Chr(34) & updArr(i).getDest() & Chr(34) & vbCrLf
                End If
            Next

            strBat = strBat & "ping ya.ru -n 5" & vbCrLf
            strBat = strBat & "shutdown -r -f -t 0" & vbCrLf
            Dim fFile As Short
            fFile = FreeFile()
            FileOpen(fFile, batName, OpenMode.Output)
            PrintLine(fFile, strBat)
            FileClose(fFile)

            Dim sql1 As String = "UPDATE commands SET status=1, date_execute=UNIX_TIMESTAMP() WHERE id_on_server=" & GlobalVars.doCommand(0).ToString()
            Execute(sql1)
            Dim comm1 As New command
            comm1.setStatus(6)
            comm1.setId(GlobalVars.doCommand(0))
            Dim strSend As String = JsonConvert.SerializeObject(comm1)
            CL.sSend("12", strSend)
            GlobalVars.doCommand(0) = 0
            GlobalVars.doCommand(1) = 0

            Dim currentForm As Form
            Dim k As Integer = 0
            Dim flag As Boolean = True

            While (flag)
                Try
                    currentForm = Form.ActiveForm
                    If currentForm Is Nothing Or currentForm.Name <> "frmPay" Then
                        Dim proc As System.Diagnostics.Process = New System.Diagnostics.Process()
                        proc.StartInfo.FileName = batName

                        proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal
                        proc.Start()
                        Application.Exit()
                    Else
                        Thread.Sleep(6000)
                    End If
                Catch ex As Exception
                    msg("Ошибка обновления: " & ex.ToString())
                End Try
            End While
        End If
    End Sub
End Class

Public Class UpdateJson
    Public act As String
    Public src As String
    Public dest As String

    Public Function getAct() As String
        Return act
    End Function

    Public Sub setAct(ByVal n_act As String)
        act = n_act
    End Sub
    Public Function getSrc() As String
        Return src
    End Function
    Public Sub setSrc(ByVal n_src As String)
        src = n_src
    End Sub
    Public Function getDest() As String
        Return dest
    End Function
    Public Sub setDest(ByVal n_dest As String)
        dest = n_dest
    End Sub
End Class
