﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmService
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmService))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.tblServices = New System.Windows.Forms.TableLayoutPanel()
        Me.tblTitle = New System.Windows.Forms.TableLayoutPanel()
        Me.txtNumber = New System.Windows.Forms.MaskedTextBox()
        Me.tblNumPad = New System.Windows.Forms.TableLayoutPanel()
        Me.pbWarning = New System.Windows.Forms.PictureBox()
        Me.pbService = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.pbLogo = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.tblServices.SuspendLayout()
        Me.tblTitle.SuspendLayout()
        CType(Me.pbWarning, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.pbService, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 60000
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 1
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.tblServices, 0, 0)
        Me.TableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(3, 33)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 1
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 400.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(1268, 754)
        Me.TableLayoutPanel6.TabIndex = 3
        '
        'tblServices
        '
        Me.tblServices.AutoSize = True
        Me.tblServices.BackColor = System.Drawing.Color.Transparent
        Me.tblServices.ColumnCount = 3
        Me.tblServices.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tblServices.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblServices.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tblServices.Controls.Add(Me.tblTitle, 0, 0)
        Me.tblServices.Controls.Add(Me.tblNumPad, 1, 1)
        Me.tblServices.Controls.Add(Me.pbWarning, 0, 1)
        Me.tblServices.Controls.Add(Me.pbService, 2, 1)
        Me.tblServices.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblServices.Location = New System.Drawing.Point(3, 3)
        Me.tblServices.Name = "tblServices"
        Me.tblServices.RowCount = 2
        Me.tblServices.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120.0!))
        Me.tblServices.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 500.0!))
        Me.tblServices.Size = New System.Drawing.Size(1262, 748)
        Me.tblServices.TabIndex = 0
        '
        'tblTitle
        '
        Me.tblTitle.BackColor = System.Drawing.Color.White
        Me.tblTitle.BackgroundImage = CType(resources.GetObject("tblTitle.BackgroundImage"), System.Drawing.Image)
        Me.tblTitle.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.tblTitle.ColumnCount = 5
        Me.tblServices.SetColumnSpan(Me.tblTitle, 3)
        Me.tblTitle.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.tblTitle.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.77778!))
        Me.tblTitle.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.tblTitle.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.tblTitle.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 45.0!))
        Me.tblTitle.Controls.Add(Me.txtNumber, 1, 0)
        Me.tblTitle.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblTitle.Location = New System.Drawing.Point(3, 3)
        Me.tblTitle.Name = "tblTitle"
        Me.tblTitle.RowCount = 1
        Me.tblTitle.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblTitle.Size = New System.Drawing.Size(1256, 114)
        Me.tblTitle.TabIndex = 1
        '
        'txtNumber
        '
        Me.txtNumber.BackColor = System.Drawing.Color.White
        Me.txtNumber.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.txtNumber.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.txtNumber.Dock = System.Windows.Forms.DockStyle.Right
        Me.txtNumber.Font = New System.Drawing.Font("Microsoft Sans Serif", 48.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.txtNumber.ForeColor = System.Drawing.Color.Indigo
        Me.txtNumber.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.txtNumber.Location = New System.Drawing.Point(190, 20)
        Me.txtNumber.Margin = New System.Windows.Forms.Padding(3, 20, 20, 3)
        Me.txtNumber.Name = "txtNumber"
        Me.txtNumber.PromptChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.txtNumber.Size = New System.Drawing.Size(740, 73)
        Me.txtNumber.TabIndex = 2
        Me.txtNumber.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'tblNumPad
        '
        Me.tblNumPad.ColumnCount = 3
        Me.tblNumPad.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblNumPad.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblNumPad.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.tblNumPad.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblNumPad.Location = New System.Drawing.Point(318, 123)
        Me.tblNumPad.Name = "tblNumPad"
        Me.tblNumPad.RowCount = 4
        Me.tblNumPad.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblNumPad.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblNumPad.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblNumPad.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.tblNumPad.Size = New System.Drawing.Size(625, 622)
        Me.tblNumPad.TabIndex = 0
        '
        'pbWarning
        '
        Me.pbWarning.BackColor = System.Drawing.Color.Transparent
        Me.pbWarning.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbWarning.Location = New System.Drawing.Point(10, 130)
        Me.pbWarning.Margin = New System.Windows.Forms.Padding(10)
        Me.pbWarning.Name = "pbWarning"
        Me.pbWarning.Size = New System.Drawing.Size(295, 608)
        Me.pbWarning.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pbWarning.TabIndex = 2
        Me.pbWarning.TabStop = False
        '
        'pbService
        '
        Me.pbService.BackColor = System.Drawing.Color.Transparent
        Me.pbService.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbService.Location = New System.Drawing.Point(956, 130)
        Me.pbService.Margin = New System.Windows.Forms.Padding(10)
        Me.pbService.Name = "pbService"
        Me.pbService.Size = New System.Drawing.Size(296, 608)
        Me.pbService.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.pbService.TabIndex = 0
        Me.pbService.TabStop = False
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.pbLogo, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1280, 1024)
        Me.TableLayoutPanel2.TabIndex = 9
        '
        'pbLogo
        '
        Me.pbLogo.BackColor = System.Drawing.Color.Transparent
        Me.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TableLayoutPanel2.SetColumnSpan(Me.pbLogo, 2)
        Me.pbLogo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbLogo.Location = New System.Drawing.Point(3, 3)
        Me.pbLogo.Name = "pbLogo"
        Me.pbLogo.Size = New System.Drawing.Size(1274, 214)
        Me.pbLogo.TabIndex = 11
        Me.pbLogo.TabStop = False
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.lblStatus, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel6, 0, 1)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 223)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 3
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1274, 798)
        Me.TableLayoutPanel3.TabIndex = 8
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblStatus.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Indigo
        Me.lblStatus.Location = New System.Drawing.Point(3, 0)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(1268, 30)
        Me.lblStatus.TabIndex = 1
        '
        'frmService
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.White
        Me.ClientSize = New System.Drawing.Size(1280, 1024)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmService"
        Me.Text = "frmService"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.TableLayoutPanel6.PerformLayout()
        Me.tblServices.ResumeLayout(False)
        Me.tblTitle.ResumeLayout(False)
        Me.tblTitle.PerformLayout()
        CType(Me.pbWarning, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.pbService, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pbLogo As System.Windows.Forms.PictureBox
    Friend WithEvents tblServices As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tblTitle As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents tblNumPad As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pbWarning As System.Windows.Forms.PictureBox
    Friend WithEvents pbService As System.Windows.Forms.PictureBox
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents txtNumber As System.Windows.Forms.MaskedTextBox
End Class
