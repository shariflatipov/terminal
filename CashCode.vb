﻿Imports System
Imports System.IO.Ports
Imports System.Runtime.InteropServices
Imports System.Windows.Forms

Imports System.Threading

Public Class CashCode

#Region " Параметры "
    ''' <summary>
    ''' Номер COM порта
    ''' </summary>
    ''' <remarks></remarks>
    Private _port As Integer
    Private _lblInsert As Label
    Private _lblSum As Label
    Private _lblComission As Label
    Private _lblZachislenie As Label
    Private _service_id As Integer = 0
    Private _arrButtons(2) As Button
    Private valuta As Integer()
    Private _nomer As String = ""
    Private _chek As String = ""
    Public _lastInsertPlatej As Integer = 0
    Private _summ As Double = 0
    Public _komission As Double = 0
    Public isDetected As Boolean = False

    Dim CurentPortName As String
    Dim CurentPort As New SerialPort()
    Dim Data As New Data
    'Dim GlobalVars As New GlobalVars
    Dim quit As Boolean = False
    Dim bufferIn As Byte() = {0, 0, 0, 0, 0, 0}

    Dim Thread1 As Thread
    'Dim GlobalVars As New GlobalVar

#End Region

#Region " Свойства класса "
    ''' <summary>
    ''' Номер COM порта
    ''' </summary>
    ''' <value></value>
    ''' <returns>Целое число</returns>
    ''' <remarks></remarks>
    ''' 
    Public Property service As Integer
        Get
            Return _service_id
        End Get
        Set(ByVal Value As Integer)
            _service_id = Value
        End Set
    End Property

    Public Property port() As Integer
        Get
            Return _port
        End Get
        Set(ByVal Value As Integer)
            _port = Value
            CurentPortName = "COM" & _port
        End Set
    End Property

    Public ReadOnly Property Valutas() As Integer()
        Get
            Return valuta
        End Get
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property lblInset() As Label
        Get
            Return _lblInsert
        End Get
        Set(ByVal Value As Label)
            _lblInsert = Value
        End Set
    End Property

    Public Property lblSum() As Label
        Get
            Return _lblSum
        End Get
        Set(ByVal Value As Label)
            _lblSum = Value
        End Set
    End Property

    Public Property lblComission() As Label
        Get
            Return _lblComission
        End Get
        Set(ByVal Value As Label)
            _lblComission = Value
        End Set
    End Property

    Public Property lblZachislenie() As Label
        Get
            Return _lblZachislenie
        End Get
        Set(ByVal Value As Label)
            _lblZachislenie = Value
        End Set
    End Property

    Public Property nomer() As String
        Get
            Return _nomer
        End Get
        Set(ByVal Value As String)
            _nomer = Value
        End Set
    End Property

    Public ReadOnly Property chek() As String
        Get
            Return _chek
        End Get
    End Property

    Public ReadOnly Property PlatejID() As Integer
        Get
            Return _lastInsertPlatej
        End Get
    End Property

    Public ReadOnly Property summ() As Double
        Get
            Return _summ
        End Get
    End Property

    Public ReadOnly Property comiss() As Double
        Get
            Return _komission
        End Get
    End Property

#End Region

#Region " Class Constructors "
    ''' <summary>
    ''' Empty constructor
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        'Set the file stream
        MyBase.New()
        'Instantiate out Text property to an empty string
        _port = 1
        CurentPortName = "COM" & _port
        CurentPort.BaudRate = 9600
        CurentPort.ReadTimeout = 2000
        CurentPort.WriteTimeout = 1500
    End Sub

    ''' <summary>
    ''' Создается экземпляр класса с указанным номером порта
    ''' </summary>
    ''' <param name="nom">Номер COM порта</param>
    ''' <remarks></remarks>
    Public Sub New(ByVal nom As Integer)
        'Set the file stream
        MyBase.New()
        'Set our Text property value
        _port = nom
        CurentPortName = "COM" & _port
        CurentPort.BaudRate = 9600
        CurentPort.ReadTimeout = 2000
        CurentPort.WriteTimeout = 1500
    End Sub
#End Region

#Region " Процедуры "
    ''' <summary>
    ''' Перезагрузить CashCode
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub Reset()
        Try
            If CurentPort.IsOpen = False Then
                CurentPort.PortName = CurentPortName
                CurentPort.Open()
            End If
            'CurentPort.WriteTimeout = 10
            CurentPort.Write(Data.ConReset, 0, 6)
            msg("-> " & split_print("", Data.ConReset))
            Dim a = CurentPort.BytesToRead
            Dim buff(a - 1) As Byte
            CurentPort.Read(buff, 0, a)
            Thread.Sleep(100)
            msg("<- " & split_print("", buff))

            CurentPort.Write(Data.ConStPoll, 0, 6)
            msg("-> " & split_print("", Data.ConStPoll))
            'Thread.Sleep(tOut)

            a = CurentPort.BytesToRead
            'Dim buff(a - 1) As Byte
            ReDim buff(a - 1)
            CurentPort.Read(buff, 0, a)

            Thread.Sleep(50)
            Dim buff_temp() As Byte
            If (buff.Count > 3) Then
                If buff.Count > buff(2) Then
                    ReDim buff_temp(buff.Count - buff(2) - 1)
                    Array.Copy(buff, buff.Count - buff(2), buff_temp, 0, buff.Count - buff(2))
                    ReDim Preserve buff(buff(2) - 1)
                End If
            End If
            msg("<- " & split_print("", buff))

            CurentPort.Write(Data.ConReACK, 0, 6)
            msg("-> " & split_print("", Data.ConReACK))

            _lastInsertPlatej = 0
        Catch ex As Exception
            msg("ERROR Cashecode: " & ex.Message)
        End Try
    End Sub

    Public Sub Buttons(ByVal Value0 As Button, ByVal Value1 As Button, ByVal Value2 As Button)
        _arrButtons(0) = Value0
        _arrButtons(1) = Value1
        _arrButtons(2) = Value2
    End Sub

    ''' <summary>
    ''' Приём денег
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Sub Pooling()
        quit = False
        Thread1 = New Thread(AddressOf StartPoll)
        Thread1.Start()
    End Sub

    Private Sub StartPoll()
        Dim arrBillTypes() As Integer = {1, 3, 5, 10, 20, 50, 100, 200, 500}
        valuta = {0, 0, 0, 0, 0, 0, 0, 0, 0}
        _lastInsertPlatej = 0
        If CurentPort.IsOpen = False Then
            CurentPort.PortName = CurentPortName
            CurentPort.Open()
        End If
        msg("=================Начинаем прием денег абоненту: " & _nomer & "=================")
        Dim tOut As Integer = 50
        While (Not quit)
            Try
                'Dim buff(128) As Byte '() = {0, 0, 0, 0, 0, 0}
                Dim buff() As Byte
                Dim buff3(128) As Byte '() = {0, 0, 0, 0, 0, 0}
                'CurentPort.Write(Data.ConIdent, 0, 6)

                'Thread.Sleep(100)
                'CurentPort.Read(buff3, 0, 6)

                CurentPort.Write(Data.ConStPoll, 0, 6)
                msg("-> " & split_print("", Data.ConStPoll))
                'Thread.Sleep(tOut)

                Dim a = CurentPort.BytesToRead
                'Dim buff(a - 1) As Byte
                ReDim buff(a - 1)
                CurentPort.Read(buff, 0, a)

                Thread.Sleep(50)
                Dim buff_temp() As Byte
                If (buff.Count > 3) Then
                    If buff.Count > buff(2) Then
                        ReDim buff_temp(buff.Count - buff(2) - 1)
                        Array.Copy(buff, buff.Count - buff(2), buff_temp, 0, buff.Count - buff(2))
                        ReDim Preserve buff(buff(2) - 1)
                    End If
                End If
                msg("<- " & split_print("", buff))

                CurentPort.Write(Data.ConReACK, 0, 6)
                msg("-> " & split_print("", Data.ConReACK))

reReadAsk:
                If (buff.Count > 3) Then
                    'If buff.Count > buff(2) Then
                    '    ReDim buff_temp(buff.Count - buff(2) - 1)
                    '    Array.Copy(buff, buff.Count - buff(2), buff_temp, 0, buff.Count - buff(2))
                    'End If
                    If (buff(3) = 19) Then
                        'msg("*Initialization*")
                        If (GlobalVars.stCashcode <> "101") Then
                            Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change,status) values('cash_code','101',UNIX_TIMESTAMP(),0)"
                            tOut = 25
                            Try
                                'Надо доделать
                                Execute(sql1)
                            Catch ex As Exception

                            End Try
                            GlobalVars.stCashcode = "101"
                        End If
                    End If

                    If (buff(3) = 21) Then
                        'msg("**Accepting**")
                        _arrButtons(0).Invoke(Sub() _arrButtons(0).Enabled = False)
                        _arrButtons(0).Invoke(Sub() _arrButtons(0).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back_dis.png"))
                        _arrButtons(1).Invoke(Sub() _arrButtons(1).Enabled = False)
                        _arrButtons(1).Invoke(Sub() _arrButtons(1).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main_dis.png"))
                        _arrButtons(2).Invoke(Sub() _arrButtons(2).Enabled = False)
                        _arrButtons(2).Invoke(Sub() _arrButtons(2).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_pay_dis.png"))
                    End If
                    If (buff(3) = 23) Then
                        'msg("**Stack**")
                        _arrButtons(0).Invoke(Sub() _arrButtons(0).Enabled = False)
                        _arrButtons(0).Invoke(Sub() _arrButtons(0).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back_dis.png"))
                        _arrButtons(1).Invoke(Sub() _arrButtons(1).Enabled = False)
                        _arrButtons(1).Invoke(Sub() _arrButtons(1).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main_dis.png"))
                        _arrButtons(2).Invoke(Sub() _arrButtons(2).Enabled = False)
                        _arrButtons(2).Invoke(Sub() _arrButtons(2).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_pay_dis.png"))
                    End If
                    If (buff(3) = 24) Then
                        'msg("**Returning**")
                        EnableDisableButtons()
                        isDetected = True
                        _isDetected = True
                    End If
                    If (buff(3) = 28) Then
                        'msg("**Generic rejecting code. Always followed by rejection reason byte (see below).**" & vbTab & split_print("EJECT", buff))
                        EnableDisableButtons()
                        isDetected = True
                        _isDetected = True
                    End If
                    If (buff(3) = 65) Then
                        If (Not GlobalVars.stCashcode.Equals("104")) Then
                            'msg("**41H		Drop Cassette Full	|  Drop Cassette full condition**")
                            Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change,status) values('cash_code','104',UNIX_TIMESTAMP(),0)"
                            Execute(sql1)
                            GlobalVars.stCashcode = "104"
                            EnableDisableButtons()
                        End If
                    End If
                    If (buff(3) = 66) Then
                        If (Not GlobalVars.stCashcode.Equals("105")) Then
                            'msg("**42H		Drop Cassette out of position	|  The Bill Validator has detected the drop cassette to be open or removed.**")
                            Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change,status) values('cash_code','105',UNIX_TIMESTAMP(),0)"
                            Execute(sql1)
                            GlobalVars.stCashcode = "105"
                            EnableDisableButtons()
                        End If
                    End If
                    If (buff(3) = 67) Then
                        If (Not GlobalVars.stCashcode.Equals("103")) Then
                            'msg("**43H		Validator Jammed	A bill(s) has jammed in the acceptance path.**")
                            Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change,status) values('cash_code','103',UNIX_TIMESTAMP(),0)"
                            Execute(sql1)
                            GlobalVars.stCashcode = "103"
                            EnableDisableButtons()
                        End If
                    End If
                    If (buff(3) = 68) Then
                        If (Not GlobalVars.stCashcode.Equals("106")) Then
                            'msg("**44H		Drop Cassette Jammed	A bill has jammed in drop cassette.**")
                            Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change,status) values('cash_code','106',UNIX_TIMESTAMP(),0)"
                            Execute(sql1)
                            GlobalVars.stCashcode = "106"
                            EnableDisableButtons()
                        End If
                    End If
                    If (buff(3) = 25) Then
                        Dim buff2 As Byte() = {0, 0, 0, 0, 0, 0}
                        CurentPort.Write(Data.ConEnBT, 0, 12)
                        Thread.Sleep(50)
                        msg("-> " & split_print("", Data.ConEnBT))
                        CurentPort.Read(buff2, 0, 6)
                        Thread.Sleep(50)
                        msg("<- " & split_print("", buff2))
                    End If
                    If buff(3) = 128 Then
                        If buff(4) < 9 Then
                            'isDetected = True
                        End If
                    End If
                    If (buff(2) = 7) And buff(3) = 129 Then
                        If buff(4) < 9 Then
                            msg("Внесена купюра: " & arrBillTypes(buff(4)) & "; Абоненту: " & _nomer & ";")
                            getSum(arrBillTypes(buff(4)))
                            'Thread.Sleep(10)
                            'CurentPort.Write(Data.ConReACK, 0, 6)
                            isDetected = True
                            _isDetected = True
                        End If
                    End If
                    If (isDetected) Then

                    End If
                End If
                Try
                    If buff_temp IsNot Nothing Then
                        If buff_temp.Count > 5 Then
                            ReDim buff(buff_temp.Count - 1)
                            buff = buff_temp
                            ReDim buff_temp(0)
                            GoTo reReadAsk
                        End If
                    End If
                Catch ex As Exception
                    msg("ERROR CashCode (Redim): " & ex.Message)
                End Try
                Thread.Sleep(70)
            Catch ex As Exception
                msg("ERROR CashCode: " & ex.Message)
            End Try
        End While
    End Sub

    ''' <summary>
    ''' Остановить приём денег
    ''' </summary>
    ''' <remarks></remarks>
    ''' 
    Public Sub StopPooling()
        msg("Останавливаем прием денег абоненту: " & _nomer)
        If CurentPort.IsOpen Then
            quit = True
            Thread.Sleep(500)
            CurentPort.Write(Data.ConDiBT, 0, 12)
            Thread.Sleep(50)
            'msg("-> " & split_print("", Data.ConDiBT))
            Dim a = CurentPort.BytesToRead
            Dim buff(a - 1) As Byte
            CurentPort.Read(buff, 0, a)
            Thread.Sleep(50)

            'msg("<- " & split_print("", buff))

            CurentPort.Write(Data.ConReACK, 0, 6)
            Thread.Sleep(50)
            'msg("-> " & split_print("", Data.ConReACK))

            CurentPort.Write(Data.ConStPoll, 0, 6)
            Thread.Sleep(50)
            'msg("-> " & split_print("", Data.ConStPoll))
            a = CurentPort.BytesToRead
            ReDim buff(a - 1)
            CurentPort.Read(buff, 0, a)
            Thread.Sleep(50)

            'msg("<- " & split_print("", buff))

            CurentPort.Write(Data.ConReACK, 0, 6)
            Thread.Sleep(50)
            'msg("-> " & split_print("", Data.ConReACK))

            CurentPort.Write(Data.ConDiBT, 0, 12)
            If Thread1.IsAlive Then Thread1.Abort()
        End If
        msg("=================Прекращен прием денег абоненту: " & _nomer & "=================")
    End Sub

    Public Sub GetState()
        Thread1 = New Thread(AddressOf State)
        Thread1.Start()
    End Sub

    Private Sub State()
        If CurentPort.IsOpen = False Then
            CurentPort.PortName = CurentPortName
            CurentPort.Open()
        End If
        msg("=================Начинаем опрос купюроприемника=================")
        Dim countt As Integer = 0
        While (countt < 5)
            countt += 1
            Try
                Dim buff() As Byte
                Dim buff3(128) As Byte

                CurentPort.Write(Data.ConStPoll, 0, 6)
                msg("-> " & split_print("", Data.ConStPoll))
                'Thread.Sleep(tOut)

                Dim a = CurentPort.BytesToRead
                'Dim buff(a - 1) As Byte
                ReDim buff(a - 1)
                CurentPort.Read(buff, 0, a)

                Thread.Sleep(50)
                Dim buff_temp() As Byte
                If (buff.Count > 3) Then
                    If buff.Count > buff(2) Then
                        ReDim buff_temp(buff.Count - buff(2) - 1)
                        Array.Copy(buff, buff.Count - buff(2), buff_temp, 0, buff.Count - buff(2))
                        ReDim Preserve buff(buff(2) - 1)
                    End If
                End If
                msg("<- " & split_print("", buff))

                CurentPort.Write(Data.ConReACK, 0, 6)
                msg("-> " & split_print("", Data.ConReACK))

reReadAsk:
                If (buff.Count > 3) Then
                    If (buff(3) = 19) Then
                        'msg("*Inicialization*")
                        If (GlobalVars.stCashcode <> "101") Then
                            Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change,status) values('cash_code','101',UNIX_TIMESTAMP(),0)"
                            Execute(sql1)
                            GlobalVars.stCashcode = "101"
                            GlobalVars.doUnBlock = True
                        End If
                    End If

                    If (buff(3) = 16) Then
                        msg("*CashCode PowerUp*")
                        Reset()
                    End If

                    If (buff(3) = 65) Then
                        If (Not GlobalVars.stCashcode.Equals("104")) Then
                            'msg("**41H		Drop Cassette Full	|  Drop Cassette full condition**")
                            Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change,status) values('cash_code','104',UNIX_TIMESTAMP(),0)"
                            Execute(sql1)
                            GlobalVars.stCashcode = "104"
                            GlobalVars.doBlock = True
                        End If
                    End If
                    If (buff(3) = 66) Then
                        If (Not GlobalVars.stCashcode.Equals("105")) Then
                            'msg("**42H		Drop Cassette out of position	|  The Bill Validator has detected the drop cassette to be open or removed.**")
                            Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change,status) values('cash_code','105',UNIX_TIMESTAMP(),0)"
                            Execute(sql1)
                            GlobalVars.stCashcode = "105"
                            GlobalVars.doBlock = True
                        End If
                    End If
                    If (buff(3) = 67) Then
                        If (Not GlobalVars.stCashcode.Equals("103")) Then
                            'msg("**43H		Validator Jammed	A bill(s) has jammed in the acceptance path.**")
                            Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change,status) values('cash_code','103',UNIX_TIMESTAMP(),0)"
                            Execute(sql1)
                            GlobalVars.stCashcode = "103"
                            GlobalVars.doBlock = True
                        End If
                    End If
                    If (buff(3) = 68) Then
                        If (Not GlobalVars.stCashcode.Equals("106")) Then
                            'msg("**44H		Drop Cassette Jammed	A bill has jammed in drop cassette.**")
                            Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change,status) values('cash_code','106',UNIX_TIMESTAMP(),0)"
                            Execute(sql1)
                            GlobalVars.stCashcode = "106"
                            GlobalVars.doBlock = True
                        End If
                    End If
                End If
                Try
                    If buff_temp IsNot Nothing Then
                        If buff_temp.Count > 5 Then
                            ReDim buff(buff_temp.Count - 1)
                            buff = buff_temp
                            ReDim buff_temp(0)
                            GoTo reReadAsk
                        End If
                    End If
                Catch ex As Exception
                    msg("ERROR CashCode (Redim): " & ex.Message)
                End Try
                Thread.Sleep(70)
            Catch ex As Exception
                msg("ERROR CashCode: " & ex.Message)
            End Try
        End While
        msg("=================Закончили опрос купюроприемника=================")
    End Sub
#End Region

    Protected Overrides Sub Finalize()
        If Thread1 IsNot Nothing Then Thread1.Abort()
        MyBase.Finalize()
    End Sub

    Public Sub getSum(ByVal ins As Integer)
        Try
            Select Case ins
                Case 1
                    valuta(0) = valuta(0) + 1
                Case 3
                    valuta(1) = valuta(1) + 1
                Case 5
                    valuta(2) = valuta(2) + 1
                Case 10
                    valuta(3) = valuta(3) + 1
                Case 20
                    valuta(4) = valuta(4) + 1
                Case 50
                    valuta(5) = valuta(5) + 1
                Case 100
                    valuta(6) = valuta(6) + 1
                Case 200
                    valuta(7) = valuta(7) + 1
                Case 500
                    valuta(8) = valuta(8) + 1
            End Select
            _lblInsert.Invoke(Sub() _lblInsert.Text = ins)
            _lblSum.Invoke(Sub() _lblSum.Text = CInt(_lblSum.Text) + ins)
            _lblComission.Invoke(Sub() _lblComission.Text = calc_tarif(_lblSum.Text, _service_id))
            _lblZachislenie.Invoke(Sub() _lblZachislenie.Text = _lblSum.Text - _lblComission.Text)

            _lblSum.Invoke(Sub() _summ = _lblSum.Text)
            _lblComission.Invoke(Sub() _komission = _lblComission.Text)

            _arrButtons(0).Invoke(Sub() _arrButtons(0).Enabled = False)
            _arrButtons(0).Invoke(Sub() _arrButtons(0).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back_dis.png"))
            _arrButtons(1).Invoke(Sub() _arrButtons(1).Enabled = False)
            _arrButtons(1).Invoke(Sub() _arrButtons(1).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main_dis.png"))
            _arrButtons(2).Invoke(Sub() _arrButtons(2).Enabled = True)
            _arrButtons(2).Invoke(Sub() _arrButtons(2).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_pay.png"))

            GlobalVars.BillCount = GlobalVars.BillCount + 1
            GlobalVars.BillSumm = GlobalVars.BillSumm + ins

            msg("Абонент: " & _nomer & " внес сумму: " & _summ & " Комиссия: " & _komission)

            If _lastInsertPlatej > 0 Then
                Dim sql As String = "UPDATE plateji SET " & _
                "summa = " & _summ.ToString.Replace(",", ".") & ", " & _
                "summa_zachis = " & (_summ - _komission).ToString.Replace(",", ".") & ", " & _
                "val1 = " & valuta(0) & ", " & _
                "val3 = " & valuta(1) & ", " & _
                "val5 = " & valuta(2) & ", " & _
                "val10 = " & valuta(3) & ", " & _
                "val20 = " & valuta(4) & ", " & _
                "val50 = " & valuta(5) & ", " & _
                "val100 = " & valuta(6) & ", " & _
                "val200 = " & valuta(7) & ", " & _
                "val500 = " & valuta(8) & ", " & _
                "summa_komissia = " & _komission.ToString.Replace(",", ".") & _
                " WHERE id = " & _lastInsertPlatej
                Execute(sql)
            Else
                Dim randomUUIDString As String = System.Guid.NewGuid.ToString()
                Dim timestamp As String = DateDiff("s", DateSerial(1970, 1, 1), Now())
                _chek = GlobalVars.terminal_id & timestamp

                Dim sql As String = "insert into plateji (" & _
                "id_uslugi, nomer, summa, summa_zachis, status, date_create, id_inkass, hesh_id, val1,val3,val5,val10,val20,val50,val100,val200,val500,chek,summa_komissia" & _
                ") values (" & _
                "" & service_id & ", '" & _
                _nomer & "'," & _
                _summ.ToString.Replace(",", ".") & "," & _
                (_summ - _komission).ToString.Replace(",", ".") & ",-1,'" & _
                timestamp & "','" & _
                GlobalVars.inkass_n & "' , '" & _
                randomUUIDString & "'," & _
                valuta(0) & "," & _
                valuta(1) & "," & _
                valuta(2) & "," & _
                valuta(3) & "," & _
                valuta(4) & "," & _
                valuta(5) & "," & _
                valuta(6) & "," & _
                valuta(7) & "," & _
                valuta(8) & ", '" & _
                _chek & "'," & _
                _komission.ToString.Replace(",", ".") & ");"
                'GlobalVars.lastInsertPlatej = Execute(sql, True)
                _lastInsertPlatej = Execute(sql, True) 'GlobalVars.lastInsertPlatej
            End If
        Catch ex As Exception
            msg("ERROR: Определение купюры... " & ex.Message)
        End Try
    End Sub

    Public Function split_print(ByVal name As String, ByVal buff As Byte()) As String
        If (buff.Count > 0) Then
            Dim str As String
            If name.Equals("ConGetBT") Then
                str = ""
                For i As Integer = 0 To buff.Length - 1
                    If (buff(i) = 74 Or buff(i) = 84 Or buff(i) = 75) Then
                        str = str & Chr(buff(i))
                    Else
                        str = str & buff(i)
                    End If
                Next
            ElseIf (name.Equals("ConIdent")) Then
                str = ""
                Dim name1 As String = ""
                Dim number As String = ""
                Dim else1 As String = ""
                For i As Integer = 0 To buff.Length - 1
                    'If (i > 17 And i < 30) Then 
                    Dim itog As Integer
                    If i >= 3 And i < 12 Then
                        If (buff(i) < 0) Then
                            itog = buff(i) + 255
                        Else
                            itog = buff(i)
                        End If
                        name1 = name1 & Chr(itog)
                    ElseIf i >= 18 And i < 30 Then
                        If (buff(i) < 0) Then
                            itog = buff(i) + 255
                        Else
                            itog = buff(i)
                        End If
                        number = number & Chr(itog)
                    Else
                        If (buff(i) < 0) Then
                            itog = buff(i) + 255
                        Else
                            itog = buff(i)
                        End If
                        else1 = else1 & Chr(itog)
                    End If
                    'End If
                Next
                str = "CashCode  -   Название: " & name1 & "; Серийный номер: " & number '& "; else: " & else1
            Else
                str = ""
                For i As Integer = 0 To buff.Length - 1
                    'System.out.print(Integer.reverseBytes(buff[i]))
                    If (buff(i) < 0) Then
                        Dim itog As Integer = buff(i) + 255
                        str = str & itog
                    Else
                        str = str & Hex$(buff(i))
                    End If
                    If (i < buff.Length - 1) Then str = str & " - "
                Next
                Select Case Hex$(buff(3))
                    Case "13"
                        str = str & vbTab & "Инициализация купюроприёмника"
                    Case "14"
                        str = str & vbTab & "Ждем ввода купюр"
                    Case "15"
                        str = str & vbTab & "Распознавание купюры"
                    Case "17"
                        str = str & vbTab & "Укладка купюры в кассету"
                    Case "18"
                        str = str & vbTab & "Возврат купюры клиенту"
                    Case "19"
                        str = str & vbTab & "Unit Disabled	Bill Validator has been disabled by the Controller or just came out of initialization"
                    Case "19"
                        str = str & vbTab & ""
                    Case "1C"
                        Select Case Hex$(buff(4))
                            Case "60"
                                str = str & vbTab & "Rejecting due to Insertion. Insertion error"
                            Case "61"
                                str = str & vbTab & "Rejecting due to Magnetic. Dielectric error"
                            Case "62"
                                str = str & vbTab & "Rejecting due to Remained bill in head. Previously inserted bill remains in head"
                            Case "63"
                                str = str & vbTab & "Rejecting due to Multiplying. Compensation error/multiplying factor error"
                            Case "64"
                                str = str & vbTab & "Rejecting due to Conveying. Bill transport error"
                            Case "65"
                                str = str & vbTab & "Rejecting due to Identification1. Identification error"
                            Case "66"
                                str = str & vbTab & "Ошибка распознавания купюры. Verification error"
                            Case "67"
                                str = str & vbTab & "Rejecting due to Optic. Optic Sensor errorr"
                            Case "68"
                                str = str & vbTab & "Rejecting due to Inhibit. Return by <<inhibit denomination>> error"
                            Case "69"
                                str = str & vbTab & "Rejecting due to Capacity. Capacitance error"
                            Case "6A"
                                str = str & vbTab & "Rejecting due to Operation. Operation error"
                            Case "6C"
                                str = str & vbTab & "Rejecting due to Length. Length error "
                        End Select
                    Case "41"
                        str = str & vbTab & "Кассета заполнена"
                    Case "42"
                        str = str & vbTab & "Нет кассеты"
                    Case "43"
                        str = str & vbTab & "Замятие купюры в тракте"
                    Case "44"
                        str = str & vbTab & "Замятие купюры в кассете"
                    Case "45"
                        str = str & vbTab & "Bill Validator sends this event if the intentions of the user to deceive the Bill Validator are detected."
                    Case "46"
                        str = str & vbTab & "Пауза для укладки купюры в кассету"
                End Select
            End If
            'name
            Return str
        Else
            Return "CashCode return null"
        End If
    End Function

    Public Sub EnableDisableButtons()
        If (_lblSum.Text > 0) Then
            _arrButtons(0).Invoke(Sub() _arrButtons(0).Enabled = False)
            _arrButtons(0).Invoke(Sub() _arrButtons(0).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back_dis.png"))
            _arrButtons(1).Invoke(Sub() _arrButtons(1).Enabled = False)
            _arrButtons(1).Invoke(Sub() _arrButtons(1).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main_dis.png"))
            _arrButtons(2).Invoke(Sub() _arrButtons(2).Enabled = True)
            _arrButtons(2).Invoke(Sub() _arrButtons(2).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_pay.png"))
        Else
            _arrButtons(0).Invoke(Sub() _arrButtons(0).Enabled = True)
            _arrButtons(0).Invoke(Sub() _arrButtons(0).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back.png"))
            _arrButtons(1).Invoke(Sub() _arrButtons(1).Enabled = True)
            _arrButtons(1).Invoke(Sub() _arrButtons(1).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png"))
            _arrButtons(2).Invoke(Sub() _arrButtons(2).Enabled = False)
            _arrButtons(2).Invoke(Sub() _arrButtons(2).Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_pay_dis.png"))
        End If
    End Sub

End Class

Public Class Data
    Public ConReACK As Byte() = {&H2, &H3, &H6, &H0, &HC2, &H82}
    Public ConReset As Byte() = {&H2, &H3, &H6, &H30, &H41, &HB3}
    Public ConGetSt As Byte() = {&H2, &H3, &H6, &H31, &HC8, &HA2}
    Public ConGetSc As Byte() = {&H2, &H3, &H6, &HDD, &H0, &H1D}
    Public ConReqSt As Byte() = {&H2, &H3, &H6, &H60, &HC4, &HE1}
    Public ConReqSt1 As Byte() = {&H2, &H3, &H6, &H60, &HE1}
    Public ConStPoll As Byte() = {&H2, &H3, &H6, &H33, &HDA, &H81}
    Public ConIdent As Byte() = {&H2, &H3, &H6, &H37, &HFE, &HC7}
    Public ConGetBT As Byte() = {&H2, &H3, &H6, &H41, &H4F, &HD1}
    Public ConStack As Byte() = {&H2, &H3, &H6, &H35, &HEC, &HE4}
    Public ConReturn As Byte() = {&H2, &H3, &H6, &H36, &H77, &HD6}
    Public ConHold As Byte() = {&H2, &H3, &H6, &H38, &H9, &H3F}
    Public ConExtBD As Byte() = {&H2, &H3, &H6, &H3A, &H1B, &H1C}
    Public ConEnBT As Byte() = {&H2, &H3, &HC, &H34, &HFF, &HFF, &HFF, &H0, &H0, &H0, &HB5, &HC1}
    Public ConEnBTEscrow As Byte() = {&H2, &H3, &HC, &H34, &HFF, &HFF, &HFF, &HFF, &HFF, &HFF, &HFE, &HF7}
    Public ConDiBT As Byte() = {&H2, &H3, &HC, &H34, &H0, &H0, &H0, &H0, &H0, &H0, &H17, &HC}
End Class
