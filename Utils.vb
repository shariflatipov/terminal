﻿Module Utils

    Public Function zip2str(ByVal dec As Byte()) As String
        Try
            Dim objMemStream As New IO.MemoryStream(dec)
            Dim objGZipStream As New System.IO.Compression.GZipStream(objMemStream, System.IO.Compression.CompressionMode.Decompress)
            Dim sizeBytes(3) As Byte
            objMemStream.Position = objMemStream.Length - 5
            objMemStream.Read(sizeBytes, 0, 4)
            Dim iOutputSize As Integer = BitConverter.ToInt32(sizeBytes, 0)
            objMemStream.Position = 0
            Dim UnZippedData1(iOutputSize - 1) As Byte
            objGZipStream.Read(UnZippedData1, 0, iOutputSize)
            objGZipStream.Dispose()
            objMemStream.Dispose()
            Return System.Text.Encoding.UTF8.GetString(UnZippedData1)
        Catch ex As Exception
            msg("Ошибка при расжатии строки: " & ex.Message)
            Return "1</d>"
        End Try
    End Function

    Public Function str2zip(ByVal str As String) As Byte()
        Try
            Dim encoding As New System.Text.UTF8Encoding()
            Dim UnZippedData As Byte() = encoding.GetBytes(str)
            Dim mem As New IO.MemoryStream
            Dim gz As New System.IO.Compression.GZipStream(mem, IO.Compression.CompressionMode.Compress)
            gz.Write(UnZippedData, 0, UnZippedData.Length)
            gz.Close()
            Dim bTmp2 As Byte() = mem.ToArray()
            mem.Close()
            'Dim sendByte(bTmp2.Length + 2) As Byte

            Dim sendByte(bTmp2.Length + 5) As Byte
            Dim l2 As Integer = Len((bTmp2.Length).ToString)
            Select Case l2
                Case 1
                    sendByte(0) = 0
                    sendByte(1) = 0
                    sendByte(2) = 0
                    sendByte(3) = 0
                    sendByte(4) = 0
                    sendByte(5) = bTmp2.Length
                Case 2
                    sendByte(0) = 0
                    sendByte(1) = 0
                    sendByte(2) = 0
                    sendByte(3) = 0
                    sendByte(4) = Mid((bTmp2.Length).ToString, l2 - 1, 1)
                    sendByte(5) = Mid((bTmp2.Length).ToString, l2, 1)
                Case 3
                    sendByte(0) = 0
                    sendByte(1) = 0
                    sendByte(2) = 0
                    sendByte(3) = Mid((bTmp2.Length).ToString, l2 - 2, 1)
                    sendByte(4) = Mid((bTmp2.Length).ToString, l2 - 1, 1)
                    sendByte(5) = Mid((bTmp2.Length).ToString, l2, 1)
                Case 4
                    sendByte(0) = 0
                    sendByte(1) = 0
                    sendByte(2) = Mid((bTmp2.Length).ToString, l2 - 3, 1)
                    sendByte(3) = Mid((bTmp2.Length).ToString, l2 - 2, 1)
                    sendByte(4) = Mid((bTmp2.Length).ToString, l2 - 1, 1)
                    sendByte(5) = Mid((bTmp2.Length).ToString, l2, 1)
                Case 5
                    sendByte(0) = 0
                    sendByte(1) = Mid((bTmp2.Length).ToString, l2 - 4, 1)
                    sendByte(2) = Mid((bTmp2.Length).ToString, l2 - 3, 1)
                    sendByte(3) = Mid((bTmp2.Length).ToString, l2 - 2, 1)
                    sendByte(4) = Mid((bTmp2.Length).ToString, l2 - 1, 1)
                    sendByte(5) = Mid((bTmp2.Length).ToString, l2, 1)
                Case 6
                    sendByte(0) = Mid((bTmp2.Length).ToString, 1, 1)
                    sendByte(1) = Mid((bTmp2.Length).ToString, l2 - 4, 1)
                    sendByte(2) = Mid((bTmp2.Length).ToString, l2 - 3, 1)
                    sendByte(3) = Mid((bTmp2.Length).ToString, l2 - 2, 1)
                    sendByte(4) = Mid((bTmp2.Length).ToString, l2 - 1, 1)
                    sendByte(5) = Mid((bTmp2.Length).ToString, l2, 1)
            End Select
            Array.Copy(bTmp2, 0, sendByte, 6, bTmp2.Length)
            Return sendByte
        Catch ex As Exception
            msg("Ошибка при сжатии строки: " & ex.Message)
        End Try
    End Function

    Public Function getLastState(ByVal device As String, ByVal status As Integer) As Boolean
        Dim dt As DataTable = GetTable("SELECT state FROM perif_status WHERE perif='" & device & "' ORDER BY date_change DESC LIMIT 1")
        Dim lastState As Integer = 0
        If dt.Rows.Count > 0 Then
            lastState = CInt(Trim(dt.Rows(0).Item("state").ToString))
        End If
        If lastState = status Then
            Return False
        Else
            Return True
        End If
    End Function
End Module
