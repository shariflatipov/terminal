﻿Public Class frmGroup
    Dim count As Integer = -1
    Dim isDetected As Boolean = False

    Private Sub frmGroup_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        'pbLogo.BackgroundImage = Image.FromFile(Application.StartupPath & "/skins/logo.png")
        AxShockwaveFlash1.Height = 200
        AxShockwaveFlash1.Width = 1500
        AxShockwaveFlash1.Stop()
        AxShockwaveFlash1.Movie = Application.StartupPath & "\skins\logoGroup.swf"
        AxShockwaveFlash1.Play()

        tblServices.Controls.Clear()
        fillGroups()
        fillServices()
        tblGo.Controls.Clear()
        'Dim btn As New PictureBox
        Try
            Dim btn As New Button
            With btn
                '.SizeMode = PictureBoxSizeMode.AutoSize
                .AutoSize = True
                .FlatStyle = FlatStyle.Flat
                .FlatAppearance.BorderSize = 0
                .FlatAppearance.MouseOverBackColor = Color.Transparent
                .FlatAppearance.MouseDownBackColor = Color.Transparent
                .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back.png")
                .Dock = DockStyle.None
                .BackColor = Color.Transparent
                AddHandler .Click, AddressOf Back_Click
                AddHandler .MouseDown, AddressOf Back_MouseDown
                AddHandler .MouseUp, AddressOf Back_MouseUp
            End With
            tblGo.Controls.Add(btn, 1, 0)
            lblStatus.Text = StatusMessage(langInt)
        Catch ex As Exception
            msg("Error: " & ex.ToString & " Error occured in: " & ex.TargetSite.Name)
        End Try
        Timer1.Start()
    End Sub

#Region "Построение интерфейса"

    'Выбор групповых операторов услуг
    Private Sub fillGroups()
        Dim dt As DataTable = GetTable("SELECT * FROM OpGroup WHERE parent=" & group_id & " AND enable=1 order by order_n")
        For i As Integer = 0 To dt.Rows.Count - 1
            Try
                Dim btn As New Button
                With btn
                    .AutoSize = True
                    .FlatStyle = FlatStyle.Flat
                    .FlatAppearance.BorderSize = 0
                    .FlatAppearance.MouseOverBackColor = Color.Transparent
                    .FlatAppearance.MouseDownBackColor = Color.Transparent
                    .Tag = dt.Rows(i).Item("id")
                    .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & .Tag & ".png")
                    .BackColor = Color.Transparent
                    If dt.Rows(i).Item("enable") = "1" Then
                        .Enabled = True
                    Else
                        .Enabled = False
                    End If

                    AddHandler .Click, AddressOf Groups_Click
                    AddHandler .MouseDown, AddressOf Groups_MouseDown
                    AddHandler .MouseUp, AddressOf Groups_MouseUp
                End With
                tblServices.Controls.Add(btn, 0, 0) ' column, row)
            Catch ex As Exception
                msg("Ошибка добавления группы операторов: " & ex.Message)
            End Try
        Next
    End Sub

    Private Sub fillServices()
        Dim dt As DataTable = GetTable("SELECT * FROM OpService WHERE type='" & group_id & "' order by order_n")
        For i As Integer = 0 To dt.Rows.Count - 1
            Try
                Dim btn As New Button
                With btn
                    '.SizeMode = PictureBoxSizeMode.AutoSize
                    .AutoSize = True
                    .FlatStyle = FlatStyle.Flat
                    .FlatAppearance.BorderSize = 0
                    .FlatAppearance.MouseOverBackColor = Color.Transparent
                    .FlatAppearance.MouseDownBackColor = Color.Transparent
                    .Tag = dt.Rows(i).Item("id_operator")
                    .Image = Image.FromFile(Application.StartupPath & "/skins/oper_logos/" & .Tag & ".png")
                    .BackColor = Color.Transparent
                    AddHandler .Click, AddressOf Services_Click
                    AddHandler .MouseDown, AddressOf Services_MouseDown
                    AddHandler .MouseUp, AddressOf Services_MouseUp
                End With
                tblServices.Controls.Add(btn, 0, 0) ' column, row)
            Catch ex As Exception
                msg("Ошибка добавления оператора: " & ex.Message)
            End Try
        Next
    End Sub

    Private Sub GoBack()

    End Sub

    Private Sub goMain()

    End Sub

#End Region

#Region "Процедуры"

    '''''''''''''''''''Services
    Private Sub Services_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        group_id = 0
        service_id = sender.tag
        GlobalVars.Terminal = 306
        Timer1.Stop()
        frmService.Show()
    End Sub

    Private Sub Services_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/oper_logos/" & sender.Tag & "_off.png")
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    Private Sub Services_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/oper_logos/" & sender.Tag & ".png")
        Catch ex As Exception
            msg("Error: " & ex.Message & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    '''''''''''''''''''Groups
    Private Sub Groups_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        last_group_id = group_id
        group_id = sender.tag

        Dim dt As DataTable = GetTable("SELECT * FROM OpGroup WHERE id=" & group_id)
        If dt.Rows(0).Item("show_child") = "False" Then 'Если стоит галка показывать дочерние то 
            group_name = dt.Rows(0).Item("name")
            GlobalVars.Terminal = 306
            Timer1.Stop()
            frmService.Show()
        Else
            count = -1
            tblServices.Controls.Clear()
            fillGroups()
            fillServices()
            isDetected = True
        End If

    End Sub

    Private Sub Groups_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & sender.Tag & "_off.png")
        Catch ex As Exception
            msg("Error: " & ex.ToString & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub


    Private Sub Groups_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/" & sender.Tag & ".png")
        Catch ex As Exception
            msg("Error: " & ex.ToString & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    '''''''''''''''''''Back
    Private Sub Back_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If last_group_id > 0 Then
            group_id = last_group_id
            last_group_id = 0
            count = -1
            tblServices.Controls.Clear()
            fillGroups()
            fillServices()
        Else
            GlobalVars.getCashCode = True
            GlobalVars.Terminal = 301
            Me.Close()
        End If
    End Sub

    Private Sub Back_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back_off.png")
        Catch ex As Exception
            msg("Error: " & ex.ToString & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    Private Sub Back_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_back.png")
        Catch ex As Exception
            msg("Error: " & ex.ToString & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    '''''''''''''''''''Main
    Private Sub Main_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        group_id = 0
        last_group_id = 0
        service_id = 0
        GlobalVars.getCashCode = True
        GlobalVars.Terminal = 301
        Me.Close()
    End Sub

    Private Sub Main_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main_off.png")
        Catch ex As Exception
            msg("Error: " & ex.ToString & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

    Private Sub Main_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        Try
            sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
        Catch ex As Exception
            msg("Error: " & ex.ToString & " Error occured in: " & ex.TargetSite.Name)
        End Try
    End Sub

#End Region

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        If isDetected Then
            isDetected = False
        Else
            Main_Click(sender, e)
        End If
    End Sub
End Class