﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text

Namespace POSPrinter2


    Public Class OposPrint
#Region "Константы"
        '''//////////////////////////////////////////////////////////////////
        ' "StatusUpdateEvent" Event: "Data" Parameter Constants
        '''//////////////////////////////////////////////////////////////////

        Const PTR_SUE_COVER_OPEN As Integer = 11
        Const PTR_SUE_COVER_OK As Integer = 12
        Const PTR_SUE_JRN_COVER_OPEN As Integer = 60
        Const PTR_SUE_JRN_COVER_OK As Integer = 61
        Const PTR_SUE_REC_COVER_OPEN As Integer = 62
        Const PTR_SUE_REC_COVER_OK As Integer = 63
        Const PTR_SUE_SLP_COVER_OPEN As Integer = 64
        Const PTR_SUE_SLP_COVER_OK As Integer = 65
        Const PTR_SUE_JRN_EMPTY As Integer = 21
        Const PTR_SUE_JRN_NEAREMPTY As Integer = 22
        Const PTR_SUE_JRN_PAPEROK As Integer = 23
        Const PTR_SUE_REC_EMPTY As Integer = 24
        Const PTR_SUE_REC_NEAREMPTY As Integer = 25
        Const PTR_SUE_REC_PAPEROK As Integer = 26
        Const PTR_SUE_SLP_EMPTY As Integer = 27
        Const PTR_SUE_SLP_NEAREMPTY As Integer = 28
        Const PTR_SUE_SLP_PAPEROK As Integer = 29
        Const PTR_SUE_JRN_CARTRIDGE_EMPTY As Integer = 41
        Const PTR_SUE_JRN_CARTRIDGE_NEAREMPTY As Integer = 42
        Const PTR_SUE_JRN_HEAD_CLEANING As Integer = 43
        Const PTR_SUE_JRN_CARTRIDGE_OK As Integer = 44
        Const PTR_SUE_REC_CARTRIDGE_EMPTY As Integer = 45
        Const PTR_SUE_REC_CARTRIDGE_NEAREMPTY As Integer = 46
        Const PTR_SUE_REC_HEAD_CLEANING As Integer = 47
        Const PTR_SUE_REC_CARTRIDGE_OK As Integer = 48
        Const PTR_SUE_SLP_CARTRIDGE_EMPTY As Integer = 49
        Const PTR_SUE_SLP_CARTRIDGE_NEAREMPTY As Integer = 50
        Const PTR_SUE_SLP_HEAD_CLEANING As Integer = 51
        Const PTR_SUE_SLP_CARTRIDGE_OK As Integer = 52
        Const PTR_SUE_IDLE As Integer = 1001

#End Region

        Public Function GetStatusString(ByVal data As Integer) As String

            Dim str As String = "UNKNOWN!"

            '''/ Status CODES ////
            Select Case data
                Case PTR_SUE_COVER_OPEN
                    str = "PTR_SUE_COVER_OPEN"
                    Exit Select
                Case PTR_SUE_COVER_OK
                    str = "PTR_SUE_COVER_OK"
                    Exit Select
                Case PTR_SUE_JRN_COVER_OPEN
                    str = "PTR_SUE_JRN_COVER_OPEN"
                    Exit Select
                Case PTR_SUE_JRN_COVER_OK
                    str = "PTR_SUE_JRN_COVER_OK"
                    Exit Select
                Case PTR_SUE_REC_COVER_OPEN
                    str = "PTR_SUE_REC_COVER_OPEN"
                    Exit Select
                Case PTR_SUE_REC_COVER_OK
                    str = "PTR_SUE_REC_COVER_OK"
                    Exit Select
                Case PTR_SUE_SLP_COVER_OPEN
                    str = "PTR_SUE_SLP_COVER_OPEN"
                    Exit Select
                Case PTR_SUE_SLP_COVER_OK
                    str = "PTR_SUE_SLP_COVER_OK"
                    Exit Select
                Case PTR_SUE_JRN_EMPTY
                    str = "PTR_SUE_JRN_EMPTY"
                    Exit Select
                Case PTR_SUE_JRN_NEAREMPTY
                    str = "PTR_SUE_JRN_NEAREMPTY"
                    Exit Select
                Case PTR_SUE_JRN_PAPEROK
                    str = "PTR_SUE_JRN_PAPEROK"
                    Exit Select
                Case PTR_SUE_REC_EMPTY
                    str = "PTR_SUE_REC_EMPTY"
                    Exit Select
                Case PTR_SUE_REC_NEAREMPTY
                    str = "PTR_SUE_REC_NEAREMPTY"
                    Exit Select
                Case PTR_SUE_REC_PAPEROK
                    str = "PTR_SUE_REC_PAPEROK"
                    Exit Select
                Case PTR_SUE_SLP_EMPTY
                    str = "PTR_SUE_SLP_EMPTY"
                    Exit Select
                Case PTR_SUE_SLP_NEAREMPTY
                    str = "PTR_SUE_SLP_NEAREMPTY"
                    Exit Select
                Case PTR_SUE_SLP_PAPEROK
                    str = "PTR_SUE_SLP_PAPEROK"
                    Exit Select
                Case PTR_SUE_JRN_CARTRIDGE_EMPTY
                    str = "PTR_SUE_JRN_CARTRIDGE_EMPTY"
                    Exit Select
                Case PTR_SUE_JRN_CARTRIDGE_NEAREMPTY
                    str = "PTR_SUE_JRN_CARTRIDGE_NEAREMPTY"
                    Exit Select
                Case PTR_SUE_JRN_HEAD_CLEANING
                    str = "PTR_SUE_JRN_HEAD_CLEANING"
                    Exit Select
                Case PTR_SUE_JRN_CARTRIDGE_OK
                    str = "PTR_SUE_JRN_CARTRIDGE_OK"
                    Exit Select
                Case PTR_SUE_REC_CARTRIDGE_EMPTY
                    str = "PTR_SUE_REC_CARTRIDGE_EMPTY"
                    Exit Select
                Case PTR_SUE_REC_CARTRIDGE_NEAREMPTY
                    str = "PTR_SUE_REC_CARTRIDGE_NEAREMPTY"
                    Exit Select
                Case PTR_SUE_REC_HEAD_CLEANING
                    str = "PTR_SUE_REC_HEAD_CLEANING"
                    Exit Select
                Case PTR_SUE_REC_CARTRIDGE_OK
                    str = "PTR_SUE_REC_CARTRIDGE_OK"
                    Exit Select
                Case PTR_SUE_SLP_CARTRIDGE_EMPTY
                    str = "PTR_SUE_SLP_CARTRIDGE_EMPTY"
                    Exit Select
                Case PTR_SUE_SLP_CARTRIDGE_NEAREMPTY
                    str = "PTR_SUE_SLP_CARTRIDGE_NEAREMPTY"
                    Exit Select
                Case PTR_SUE_SLP_HEAD_CLEANING
                    str = "PTR_SUE_SLP_HEAD_CLEANING"
                    Exit Select
                Case PTR_SUE_SLP_CARTRIDGE_OK
                    str = "PTR_SUE_SLP_CARTRIDGE_OK"
                    Exit Select
                Case PTR_SUE_IDLE
                    str = "PTR_SUE_IDLE"
                    Exit Select
            End Select

            Return str

        End Function

    End Class
End Namespace