﻿Imports System.Security.Cryptography

Public Class frmLogin
    Dim cmdNext As New PictureBox
    Dim OperMask As String
    Dim arrInfo(2) As String
    Dim arrLabel(2, 3) As String
    Public s1 As Double = 0
    Public s2 As Double = 0
    Public valuta As Integer()
    Public nomer As String

    Dim chekN As String = ""

    Public blnShift As Boolean = False
    Public intKeyLang As Integer = 2
    Dim labID As Integer
    Private ActiveGroup As Integer = 1
    Dim arrKey(60) As String
    Dim arrChar(2, 60)
    Dim txtActive As Windows.Forms.TextBox
    Public arrTextBoxes(1) As Windows.Forms.TextBox

    Private Sub frmLogin_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        pbLogo.BackgroundImage = Image.FromFile(Application.StartupPath & "/skins/logo.png")

        arrKey(0) = "btna"
        arrKey(1) = "btnb"
        arrKey(2) = "btnc"
        arrKey(3) = "btn_c"
        arrKey(4) = "btnd"
        arrKey(5) = "btne"
        arrKey(6) = "btnf"
        arrKey(7) = "btng"
        arrKey(8) = "btn_kh"
        arrKey(9) = "btnh"
        arrKey(10) = "btn_i"
        arrKey(11) = "btni"
        arrKey(12) = "btnj"
        arrKey(13) = "btnk"
        arrKey(14) = "btnl"
        arrKey(15) = "btnm"
        arrKey(16) = "btnn"
        arrKey(17) = "btno"
        arrKey(18) = "btn_o"
        arrKey(19) = "btnp"
        arrKey(20) = "btnr"
        arrKey(21) = "btns"
        arrKey(22) = "btn_s"
        arrKey(23) = "btnt"
        arrKey(24) = "btnu"
        arrKey(25) = "btn_u"
        arrKey(26) = "btnv"
        arrKey(27) = "btny"
        arrKey(28) = "btnz"
        arrKey(29) = "btnw"
        arrKey(30) = "btnq"
        arrKey(31) = "btnx"
        arrKey(32) = "btnnokta" '"."
        arrKey(33) = "btnsolshift"   'left shift
        arrKey(34) = "btnspace"   'space
        arrKey(35) = "btnsagshift"    'right shift 
        arrKey(36) = "btnenter"   'enter
        arrKey(37) = "btnvirgul"    'comma
        arrKey(38) = "btnbackspace"   'back space
        arrKey(39) = "btngz"  'left
        arrKey(40) = "btnzu"   'right
        arrKey(41) = ""   '  .
        arrKey(42) = "btn0"
        arrKey(43) = "btn1"
        arrKey(44) = "btn2"
        arrKey(45) = "btn3"
        arrKey(46) = "btn4"
        arrKey(47) = "btn5"
        arrKey(48) = "btn6"
        arrKey(49) = "btn7"
        arrKey(50) = "btn8"
        arrKey(51) = "btn9"



        arrChar(0, 0) = "ф"
        arrChar(0, 1) = "и"
        arrChar(0, 2) = "с"
        arrChar(0, 3) = "."
        arrChar(0, 4) = "в"
        arrChar(0, 5) = "у"
        arrChar(0, 6) = "а"
        arrChar(0, 7) = "п"
        arrChar(0, 8) = "х"
        arrChar(0, 9) = "р"
        arrChar(0, 10) = "э"
        arrChar(0, 11) = "ш"
        arrChar(0, 12) = "о"
        arrChar(0, 13) = "л"
        arrChar(0, 14) = "д"
        arrChar(0, 15) = "ь"
        arrChar(0, 16) = "т"
        arrChar(0, 17) = "щ"
        arrChar(0, 18) = ""
        arrChar(0, 19) = "з"
        arrChar(0, 20) = "к"
        arrChar(0, 21) = "ы"
        arrChar(0, 22) = "ж"
        arrChar(0, 23) = "е"
        arrChar(0, 24) = "г"
        arrChar(0, 25) = "ъ"
        arrChar(0, 26) = "м"
        arrChar(0, 27) = "н"
        arrChar(0, 28) = "я"
        arrChar(0, 29) = "ц"
        arrChar(0, 30) = "й"
        arrChar(0, 31) = "ч"
        arrChar(0, 32) = "ю"
        arrChar(0, 33) = "btnsolshift"
        arrChar(0, 34) = " "
        arrChar(0, 35) = "btnsagshift"
        arrChar(0, 36) = "btnenter"
        arrChar(0, 37) = "б"
        arrChar(0, 38) = "btnbackspace"
        arrChar(0, 39) = ""
        arrChar(0, 40) = ""
        arrChar(0, 41) = "btnnumlocknokta"
        arrChar(0, 42) = "0"
        arrChar(0, 43) = "1"
        arrChar(0, 44) = "2"
        arrChar(0, 45) = "3"
        arrChar(0, 46) = "4"
        arrChar(0, 47) = "5"
        arrChar(0, 48) = "6"
        arrChar(0, 49) = "7"
        arrChar(0, 50) = "8"
        arrChar(0, 51) = "9"

        arrChar(1, 0) = "ф"
        arrChar(1, 1) = "и"
        arrChar(1, 2) = "с"
        arrChar(1, 3) = "."
        arrChar(1, 4) = "в"
        arrChar(1, 5) = "у"
        arrChar(1, 6) = "а"
        arrChar(1, 7) = "п"
        arrChar(1, 8) = "х"
        arrChar(1, 9) = "р"
        arrChar(1, 10) = "э"
        arrChar(1, 11) = "ш"
        arrChar(1, 12) = "о"
        arrChar(1, 13) = "л"
        arrChar(1, 14) = "д"
        arrChar(1, 15) = "ӣ"
        arrChar(1, 16) = "т"
        arrChar(1, 17) = "ҳ"
        arrChar(1, 18) = ""
        arrChar(1, 19) = "з"
        arrChar(1, 20) = "к"
        arrChar(1, 21) = "ҷ"
        arrChar(1, 22) = "ж"
        arrChar(1, 23) = "е"
        arrChar(1, 24) = "г"
        arrChar(1, 25) = "ъ"
        arrChar(1, 26) = "м"
        arrChar(1, 27) = "н"
        arrChar(1, 28) = "я"
        arrChar(1, 29) = "қ"
        arrChar(1, 30) = "й"
        arrChar(1, 31) = "ч"
        arrChar(1, 32) = "ю"
        arrChar(1, 33) = "btnsolshift"
        arrChar(1, 34) = " "
        arrChar(1, 35) = "btnsagshift"
        arrChar(1, 36) = "btnenter"
        arrChar(1, 37) = "б"
        arrChar(1, 38) = "btnbackspace"
        arrChar(1, 39) = "ғ"
        arrChar(1, 40) = "ӯ"
        arrChar(1, 41) = "btnnumlocknokta"
        arrChar(1, 42) = "0"
        arrChar(1, 43) = "1"
        arrChar(1, 44) = "2"
        arrChar(1, 45) = "3"
        arrChar(1, 46) = "4"
        arrChar(1, 47) = "5"
        arrChar(1, 48) = "6"
        arrChar(1, 49) = "7"
        arrChar(1, 50) = "8"
        arrChar(1, 51) = "9"

        arrChar(2, 0) = "a"
        arrChar(2, 1) = "b"
        arrChar(2, 2) = "c"
        arrChar(2, 3) = ""
        arrChar(2, 4) = "d"
        arrChar(2, 5) = "e"
        arrChar(2, 6) = "f"
        arrChar(2, 7) = "g"
        arrChar(2, 8) = ""
        arrChar(2, 9) = "h"
        arrChar(2, 10) = ""
        arrChar(2, 11) = "i"
        arrChar(2, 12) = "j"
        arrChar(2, 13) = "k"
        arrChar(2, 14) = "l"
        arrChar(2, 15) = "m"
        arrChar(2, 16) = "n"
        arrChar(2, 17) = "o"
        arrChar(2, 18) = ""
        arrChar(2, 19) = "p"
        arrChar(2, 20) = "r"
        arrChar(2, 21) = "s"
        arrChar(2, 22) = ""
        arrChar(2, 23) = "t"
        arrChar(2, 24) = "u"
        arrChar(2, 25) = ""
        arrChar(2, 26) = "v"
        arrChar(2, 27) = "y"
        arrChar(2, 28) = "z"
        arrChar(2, 29) = "w"
        arrChar(2, 30) = "q"
        arrChar(2, 31) = "x"
        arrChar(2, 32) = "."
        arrChar(2, 33) = "btnsolshift"
        arrChar(2, 34) = " "
        arrChar(2, 35) = "btnsagshift"
        arrChar(2, 36) = "btnenter"
        arrChar(2, 37) = ","
        arrChar(2, 38) = "btnbackspace"
        arrChar(2, 39) = ""
        arrChar(2, 40) = ""
        arrChar(2, 41) = "btnnumlocknokta"
        arrChar(2, 42) = "0"
        arrChar(2, 43) = "1"
        arrChar(2, 44) = "2"
        arrChar(2, 45) = "3"
        arrChar(2, 46) = "4"
        arrChar(2, 47) = "5"
        arrChar(2, 48) = "6"
        arrChar(2, 49) = "7"
        arrChar(2, 50) = "8"
        arrChar(2, 51) = "9"
        setTextKeyboard(2, True)
        arrTextBoxes(0) = txtLogin
        arrTextBoxes(1) = txtPass

        txtLogin.Focus()

        tblGo.Controls.Clear()
        Dim btn2 As New PictureBox
        With btn2
            .SizeMode = PictureBoxSizeMode.AutoSize
            .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
            .Dock = DockStyle.Right
            .BackColor = Color.Transparent
            AddHandler .Click, AddressOf Main_Click
            AddHandler .MouseDown, AddressOf Main_MouseDown
            AddHandler .MouseUp, AddressOf Main_MouseUp
            AddHandler .MouseLeave, AddressOf Main_MouseLeave
        End With
        tblGo.Controls.Add(btn2, 0, 0)
        With cmdNext
            .SizeMode = PictureBoxSizeMode.AutoSize
            .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_next.png")
            .Dock = DockStyle.Left
            .BackColor = Color.Transparent
            .Name = "cmdNext"
            '.Enabled = False
            AddHandler .Click, AddressOf Next_Click
            AddHandler .MouseDown, AddressOf Next_MouseDown
            AddHandler .MouseUp, AddressOf Next_MouseUp
            AddHandler .MouseLeave, AddressOf Next_MouseLeave
        End With
        tblGo.Controls.Add(cmdNext, 1, 0)
        lblStatus.Text = StatusMessage(langInt)
    End Sub

    Private Sub btn_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btn0.Click, btn1.Click, btn2.Click, btn3.Click, btn4.Click, btn5.Click, btn6.Click, btn7.Click, btn8.Click, btn9.Click, btn_c.Click, btn_kh.Click, btni.Click, btnvirgul.Click, btn_s.Click, btn_u.Click, btna.Click, btnb.Click, btnc.Click, btnd.Click, btne.Click, btnf.Click, btng.Click, btnh.Click, btn_i.Click, btnj.Click, btnk.Click, btnl.Click, btnm.Click, btnn.Click, btnnokta.Click, btno.Click, btnp.Click, btnq.Click, btnr.Click, btns.Click, btnspace.Click, btnt.Click, btnu.Click, btnv.Click, btnw.Click, btnx.Click, btny.Click, btnz.Click, btngz.Click, btnzu.Click
        If txtActive Is Nothing Then Exit Sub
        'Try
        Dim key As Button = CType(sender, Button)
        whichbuttonpushed(key.Name)
        txtActive.Focus()
        txtActive.SelectionLength = 0
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1)
        '    Exit Sub
        'End Try
    End Sub

#Region "which button pushed function"
    Public Function whichbuttonpushed(ByVal _sender As String) As String

        Dim indArr As Integer = Array.IndexOf(arrKey, _sender)

        txtActive.AppendText(IIf(blnShift, UCase(arrChar(intKeyLang, indArr)), arrChar(intKeyLang, indArr)))
        btnbackspace.Enabled = True
        If blnShift Then
            blnShift = False
            setTextKeyboard(intKeyLang, blnShift)
            shiftrelease()
        End If
    End Function

    Private Sub shiftrelease()
        If blnShift Then
            btnsolshift.BackColor = Color.FromArgb(224, 224, 224)
        Else
            btnsolshift.BackColor = Color.LightYellow
        End If
    End Sub


    Private Sub txt_GotFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtLogin.GotFocus, txtPass.GotFocus
        Dim blnAddress As Boolean = False
        For i As Integer = 0 To 1
            If sender.name = arrTextBoxes(i).Name Then
                txtActive = arrTextBoxes(i)
                txtActive.BackColor = System.Drawing.Color.FromArgb(100, 100, 255)
            Else
                arrTextBoxes(i).BackColor = Color.White
            End If
        Next
        If Trim(txtActive.Text) = "" Then
            blnShift = True
            setTextKeyboard(intKeyLang, blnShift)
        Else
            blnShift = False
            setTextKeyboard(intKeyLang, blnShift)
        End If
        btnbackspace.Enabled = Not blnShift
        grbKeyBoard.Enabled = True
        shiftrelease()
    End Sub

    Private Sub setTextKeyboard(Optional ByVal intLang As Integer = 0, Optional ByVal shift As Boolean = True)
        intKeyLang = intLang
        Dim ctl As Control = grbKeyBoard  'Me.GetNextControl(Me, True) 'получаем первый контрол
        Do Until ctl Is Nothing
            ctl = Me.GetNextControl(ctl, True) 'следующий контрол

            If ctl.Name = "btnsolshift" Then
            ElseIf ctl.Name = "btnspace" Then
            ElseIf ctl.Name = "btnsagshift" Then
            ElseIf ctl.Name = "btnbackspace" Then

            ElseIf ctl.Name = "btn_lng" Then
                Select Case intLang
                    Case 0
                        ctl.Tag = 1
                        'btn_lng.Image = My.Resources.taj
                        ctl.Text = "TJ"
                    Case 1
                        ctl.Tag = 2
                        'btn_lng.Image = My.Resources.eng
                        ctl.Text = "EN"
                    Case 2
                        ctl.Tag = 0
                        'btn_lng.Image = My.Resources.rus
                        ctl.Text = "RU"
                End Select
                'Exit Do
            Else
                Dim indArr As Integer = Array.IndexOf(arrKey, ctl.Name)
                ctl.Text = IIf(blnShift, UCase(arrChar(intLang, indArr)), arrChar(intLang, indArr))
                If ctl.Name = btngz.Name Then Exit Do
            End If
        Loop
    End Sub

    Private Sub btnsolshift_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnsolshift.Click
        blnShift = Not blnShift
        setTextKeyboard(intKeyLang, blnShift)
        shiftrelease()
    End Sub

    Private Sub btnbackspace_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnbackspace.Click
        If txtActive Is Nothing Then Exit Sub
        If txtActive.Text = "" Then Exit Sub
        txtActive.Text = txtActive.Text.Substring(0, txtActive.Text.Count - 1)
        If txtActive.Text = "" Then
            blnShift = True
            setTextKeyboard(intKeyLang, blnShift)
            shiftrelease()
            btnbackspace.Enabled = False
        End If
    End Sub

#End Region

#Region "Процедуры"

    '''''''''''''''''''Main
    Private Sub Main_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If group_id > 0 Then frmGroup.Close()
        frmService.Close()
        group_id = 0
        last_group_id = 0
        service_id = 0
        GlobalVars.Terminal = 301
        GlobalVars.getCashCode = True
        Me.Close()
    End Sub

    Private Sub Main_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main_off.png")
    End Sub

    Private Sub Main_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
    End Sub

    Private Sub Main_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
    End Sub

    ''''''''''''Next
    Private Sub Next_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If txtLogin.Text = "" Or txtPass.Text = "" Then
            MsgBox("Не верный логин или пароль!!!")
        Else
            Dim dt As DataTable = GetTable("SELECT * FROM user WHERE login='" & txtLogin.Text & "'")
            If dt.Rows.Count > 0 Then
                Dim dbPass As String = dt.Rows(0).Item("passw")
                Dim salt As String = dbPass.Substring(0, 5)
                Dim pasw_hex As String = dbPass.Substring(dbPass.IndexOf("$") + 1)
                Dim salt2 As String = pasw_hex.Substring(0, 5)
                Dim cl_pasw_hex As String = getSHA1Hash(salt2 & txtPass.Text)
                Dim clPassSha1 As String = salt & salt2 & "$" & cl_pasw_hex
                If clPassSha1 = dbPass Then
                    frmInkass.Show(Me)
                Else
                    MsgBox("Не верный логин или пароль!!!")
                End If
            Else
                MsgBox("Не верный логин или пароль!!!")
            End If
        End If
    End Sub

    Private Sub Next_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_next_off.png")
    End Sub

    Private Sub Next_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_next.png")
    End Sub

    Private Sub Next_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_next.png")
    End Sub
#End Region

    Private Function getSHA1Hash(ByVal strToHash As String) As String
        Dim sha1Obj As New SHA1CryptoServiceProvider
        Dim bytesToHash() As Byte = System.Text.Encoding.ASCII.GetBytes(strToHash)

        bytesToHash = sha1Obj.ComputeHash(bytesToHash)

        Dim strResult As String = ""

        For Each b As Byte In bytesToHash
            strResult += b.ToString("x2")
        Next

        Return strResult
    End Function
End Class