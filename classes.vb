﻿Module classes

    'Public Class GlobalVar

    '    Public value As Integer = 10
    '    Public rst As Boolean = False

    '    Public terminal_id As String
    '    Public server_adr As String
    '    Public server_port As Integer
    '    Public ClientCertFileName As String
    '    Public KS_PATH As String
    '    Public KS_PASS As String
    '    Public TS_PATH As String
    '    Public TS_PASS As String

    '    Public inkass_id As String
    '    Public inkass_n As String
    '    Public inkass_code As String
    '    Public com_n As Integer

    '    Public agent_login As String
    '    Public agent_pass As String

    '    Public getCashCode As Boolean = True
    '    Public syncDateTime As Boolean = False
    '    Public getServises As Boolean = False
    '    Public getGroupServises As Boolean = False
    '    Public getTarifServises As Boolean = False
    '    Public getExistsServices As Boolean = False
    '    Public getSettings As Boolean = False
    '    Public getInkassUsers As Boolean = False
    '    Public getS As Boolean = False
    '    Public CashCodeSerial As String = ""
    '    Public billCount As Integer = 0
    '    Public billSumm As Integer = 0
    '    Public showInterfase As Boolean = False

    '    Public connected As Boolean = False

    '    Public doBlock As Boolean = False

    '    Public doUnBlock As Boolean = False
    '    Public doCommandId As Integer = 0

    '    Public TerminalAddress As String = ""
    '    Public callcenter As String = ""
    '    Public Organisation As String = ""

    '    Public stCashcode As String = ""
    '    Public stPrinter As String = ""
    '    Public stTerminal As String = ""

    '    Public LangId As Integer
    '    Public GroupId As Integer
    '    Public OperId As Integer
    '    Public OperMask As String = "(XXX-XX-XX)"
    '    Public NomerAb As String = ""
    '    Public TypeDB As String = "MySQL"
    '    'public Connection DataBase
    '    'public Statement st

    '    Public StatusMessage As String() = {"rus", "eng", "taj"}
    'End Class

    Public Class Info
        Public act As String
        Public login As String
        Public op_id As String
        Public num As String
        Public hash As String

        Public Sub setAct(ByVal act As String)
            Me.act = act
        End Sub

        Public Function getAct()
            Return Me.act
        End Function

        Public Sub setLogin(ByVal login As String)
            Me.login = login
        End Sub

        Public Function getLogin()
            Return Me.login
        End Function

        Public Sub setOpId(ByVal opId As String)
            Me.op_id = opId
        End Sub

        Public Function getOpId()
            Return Me.op_id
        End Function

        Public Sub setNumber(ByVal number As String)
            Me.num = number
        End Sub

        Public Function getNumber()
            Return Me.num
        End Function

        Public Sub setHash(ByVal hash As String)
            Me.hash = hash
        End Sub

        Public Function getHash()
            Return Me.hash
        End Function

        Public Function Prepare(ByVal login As String, ByVal password As String)
            Me.login = login
            Return login + password + num + op_id + act
        End Function
    End Class

    Public Class auth
        Public login As String
        Public password As String

        Public Sub set_login(ByVal n_login As String)
            login = n_login
        End Sub
        Public Function get_login() As String
            Return login
        End Function

        Public Sub set_pass(ByVal n_pass As String)
            password = n_pass
        End Sub
        Public Function get_pass() As String
            Return password
        End Function
    End Class

    Public Class SetDateTime

        Private Structure SYSTEMTIME
            Public Year As Short
            Public Month As Short
            Public DayOfWeek As Short
            Public Day As Short
            Public Hour As Short
            Public Minute As Short
            Public Second As Short
            Public Milliseconds As Short
        End Structure

        Private Declare Function SetLocalTime Lib "kernel32.dll" (ByRef lpSystemTime As SYSTEMTIME) As Boolean

        Public Function UpdateSystemTime(ByVal DateObject As Date) As Boolean
            Dim newDT As SYSTEMTIME
            'Populate the systemtime object with the information from the date object passed in
            With newDT
                .Year = DateObject.Year
                .Month = DateObject.Month
                .Day = DateObject.Day
                .DayOfWeek = DateObject.DayOfWeek
                .Hour = DateObject.Hour
                .Minute = DateObject.Minute
                .Second = DateObject.Second
                .Milliseconds = DateObject.Millisecond

            End With

            Dim blnResult As Boolean = SetLocalTime(newDT)

            Return blnResult
        End Function

    End Class

    Public Class Message
        Public act As String
        Public login As String
        Public passw As String
        Public body As String
        Public ver As String = "3.62"

        Public Function getAct() As String
            Return act
        End Function

        Public Function getVer() As String
            Return ver
        End Function
        Public Sub setVer(ByVal n_ver As String)
            ver = n_ver
        End Sub

        Public Sub setAct(ByRef n_act As String)
            act = n_act
        End Sub
        Public Function getLogin() As String
            Return login
        End Function
        Public Sub setLogin(ByVal n_login As String)
            login = n_login
        End Sub
        Public Function getPassw() As String
            Return passw
        End Function
        Public Sub setPassw(ByRef n_passw As String)
            passw = n_passw
        End Sub
        Public Function getBody() As String
            Return body
        End Function
        Public Sub setBody(ByVal n_body As String)
            body = n_body
        End Sub
    End Class

    Public Class stTerminal
        Public date1 As String
        Public terminal As String
        Public id As Integer

        Public Function getDate() As String
            Return date1
        End Function
        Public Sub setDate(ByVal n_date1 As String)
            date1 = n_date1
        End Sub
        Public Function getTerminal() As String
            Return terminal
        End Function
        Public Sub setTerminal(ByRef n_terminal As String)
            terminal = n_terminal
        End Sub
        Public Function getId() As Integer
            Return id
        End Function
        Public Sub setId(ByVal n_id As Integer)
            id = n_id
        End Sub
    End Class

    Public Class stLink
        Public date1 As String
        Public link As String
        Public id As Integer

        Public Function getDate() As String
            Return date1
        End Function
        Public Sub setDate(ByVal n_date1 As String)
            date1 = n_date1
        End Sub
        Public Function getLink() As String
            Return link
        End Function
        Public Sub setLink(ByVal n_link As String)
            link = n_link
        End Sub
        Public Function getId() As Integer
            Return id
        End Function
        Public Sub setId(ByVal n_id As Integer)
            id = n_id
        End Sub
    End Class

    Public Class MessageInput
        Public act As Integer
        Public hesh_id As String
        Public status As Integer
        Public body As String
        Public message_nomber As Integer
        Public packet_nomber As Integer
        Public packet_count As Integer

        Public Function getBody() As String
            Return body
        End Function

        Public Sub setBody(ByVal n_body As String)
            body = n_body
        End Sub

        Public Function getAct() As Integer
            Return act
        End Function

        Public Sub setAct(ByVal n_act As Integer)
            act = n_act
        End Sub

        Public Function getHesh_id() As String
            Return hesh_id
        End Function

        Public Sub setHesh_id(ByVal n_hesh_id As String)
            hesh_id = n_hesh_id
        End Sub

        Public Function getStatus() As Integer
            Return status
        End Function

        Public Sub setStatus(ByVal n_status As Integer)
            status = n_status
        End Sub
    End Class

    Public Class stCashCode
        Public date1 As String
        Public cash_code As String
        Public id As Integer

        Public Function getDate() As String
            Return date1
        End Function
        Public Sub setDate(ByVal n_date As String)
            date1 = n_date
        End Sub
        Public Function getCash_code() As String
            Return cash_code
        End Function
        Public Sub setCash_code(ByVal n_cash_code As String)
            cash_code = n_cash_code
        End Sub
        Public Function getId() As Integer
            Return id
        End Function
        Public Sub setId(ByVal n_id As Integer)
            id = n_id
        End Sub
    End Class

    Public Class stPrinter
        Public date1 As String
        Public printer As String
        Public id As Integer

        Public Function getDate() As String
            Return date1
        End Function
        Public Sub setDate(ByVal n_date As String)
            date1 = n_date
        End Sub
        Public Function getId() As Integer
            Return id
        End Function
        Public Sub setId(ByVal n_id As Integer)
            id = n_id
        End Sub
        Public Function getPrinter() As String
            Return printer
        End Function
        Public Sub setPrinter(ByVal n_printer As String)
            printer = n_printer
        End Sub
    End Class

    Public Class stCashCount
        Public date1 As String
        Public cash_count As Integer
        Public id As Integer

        Public Function getCash_count() As Integer
            Return cash_count
        End Function
        Public Sub setCash_count(ByVal n_cash_count As Integer)
            cash_count = n_cash_count
        End Sub

        Public Function getDate() As String
            Return date1
        End Function
        Public Sub setDate(ByVal n_date As String)
            date1 = n_date
        End Sub

        Public Function getId() As Integer
            Return id
        End Function
        Public Sub setId(ByVal n_id As Integer)
            id = n_id
        End Sub
    End Class

    Public Class stCashSumm
        Public date1 As String
        Public cash_summ As Integer
        Public id As Integer

        Public Function getDate() As String
            Return date1
        End Function
        Public Sub setDate(ByVal n_date As String)
            date1 = n_date
        End Sub
        Public Function getCash_summ() As Integer
            Return cash_summ
        End Function
        Public Sub setCash_summ(ByVal n_cash_summ As Integer)
            cash_summ = n_cash_summ
        End Sub
        Public Function getId() As Integer
            Return id
        End Function
        Public Sub setId(ByVal n_id As Integer)
            id = n_id
        End Sub
    End Class

    Public Class OpGroup
        Public code As String
        Public name As String
        Public order As Integer
        Public parent
        Public id As Integer
        Public show_child As Boolean

        Public Function getCode() As String
            Return code
        End Function

        Public Sub setCode(ByVal n_code As String)
            code = n_code
        End Sub
        Public Function getName() As String
            Return name
        End Function
        Public Sub setName(ByVal n_name As String)
            name = n_name
        End Sub
        Public Function getOrder() As Integer
            Return order
        End Function
        Public Sub setOrder(ByVal n_order As Integer)
            order = n_order
        End Sub
        Public Function getParent() As Integer
            Return parent
        End Function
        Public Sub setParent(ByVal n_parent)
            'If n_parent Is vbNull Then
            '    n_parent = 0
            'End If
            parent = n_parent
        End Sub
        Public Function getId() As Integer
            Return id
        End Function
        Public Sub setId(ByVal n_id As Integer)
            id = n_id
        End Sub

        Public Sub setShowChild(ByVal n_showChild As Boolean)
            show_child = n_showChild
        End Sub

        Public Function getShowChild()
            Return show_child
        End Function
    End Class

    Public Class test
        Public Name As String
        Public Sub setName(ByVal n_name As String)
            Name = n_name
        End Sub
        Public Function getName() As String
            Return Name
        End Function
    End Class

    Public Class OpService
        Public code As String
        Public name As String
        Public type As String
        Public state As String
        Public need_check As Boolean
        Public mask As String
        Public id As String
        Public order As String
        Public mask_length As Integer

        Public Function getCode() As String
            Return code
        End Function
        Public Sub setCode(ByVal n_code As String)
            code = n_code
        End Sub
        Public Function getName() As String
            Return name
        End Function
        Public Sub setName(ByRef n_name As String)
            name = n_name
        End Sub
        Public Function getNeed_check() As Boolean
            Return need_check
        End Function
        Public Sub setNeed_check(ByVal n_need_check As Boolean)
            need_check = n_need_check
        End Sub
        Public Function getMask() As String
            Return mask
        End Function
        Public Sub setMask(ByVal n_mask As String)
            mask = n_mask
        End Sub
        Public Function getId() As String
            Return id
        End Function
        Public Sub setId(ByVal n_id As String)
            id = n_id
        End Sub
        Public Function getOrder() As String
            Return order
        End Function
        Public Sub setOrder(ByVal n_order As String)
            order = n_order
        End Sub
        Public Function getType1() As String
            Return type
        End Function
        Public Sub setType(ByVal n_type As String)
            type = n_type
        End Sub
        Public Function getState() As String
            Return state
        End Function
        Public Sub setState(ByVal n_state As String)
            state = n_state
        End Sub
        Public Function getMaskLength()
            Return mask_length
        End Function
        Public Sub setMaskLength(ByVal n_maskLength As Integer)
            mask_length = n_maskLength
        End Sub
    End Class

    Public Class tarifPlan
        Public code As String
        Public date_begin As String
        Public name As String
        Public date_end As String
        Public tarif As String
        Public id As Integer

        Public Function getCode() As String
            Return code
        End Function
        Public Sub setCode(ByVal n_code As String)
            code = n_code
        End Sub
        Public Function getDate_begin() As String
            Return date_begin
        End Function
        Public Sub setDate_begin(ByVal n_date_begin As String)
            date_begin = n_date_begin
        End Sub
        Public Function getName() As String
            Return name
        End Function
        Public Sub setName(ByVal n_name As String)
            name = n_name
        End Sub
        Public Function getDate_end() As String
            Return date_end
        End Function
        Public Sub setDate_end(ByVal n_date_end As String)
            date_end = n_date_end
        End Sub
        Public Function getTarif() As String
            Return tarif
        End Function
        Public Sub setTarif(ByVal n_tarif As String)
            tarif = n_tarif
        End Sub
        Public Function getId() As Integer
            Return id
        End Function
        Public Sub setId(ByVal n_id As Integer)
            id = n_id
        End Sub
    End Class

    Public Class Tarif
        Public summa As Double
        Public summa_own As Double
        Public max As Double
        Public name As String
        Public op_service_id As Integer
        Public min As Double
        Public prc As Boolean
        Public arr As String
        Public id As Integer
        Public code As String
        Public ru_text As String
        Public tj_text As String
        Public en_text As String

        Public Function getRu_text() As String
            Return ru_text
        End Function
        Public Sub setRu_text(ByVal n_ru_text As String)
            ru_text = n_ru_text
        End Sub
        Public Function getTj_text() As String
            Return tj_text
        End Function
        Public Sub setTj_text(ByVal n_tj_text As String)
            tj_text = n_tj_text
        End Sub
        Public Function getEn_text() As String
            Return en_text
        End Function
        Public Sub setEn_text(ByVal n_en_text As String)
            en_text = n_en_text
        End Sub
        Public Function getSumma_own() As Double
            Return summa_own
        End Function
        Public Sub setSumma_own(ByVal n_summa_own As Double)
            summa_own = n_summa_own
        End Sub

        Public Function getCode() As String
            Return code
        End Function
        Public Sub setCode(ByVal n_code As String)
            code = n_code
        End Sub
        Public Function getSumma() As Double
            Return summa
        End Function
        Public Sub setSumma(ByVal n_summa As Double)
            summa = n_summa
        End Sub
        Public Function getMax() As Double
            Return max
        End Function
        Public Sub setMax(ByVal n_max As Double)
            max = n_max
        End Sub
        Public Function getName() As String
            Return name
        End Function
        Public Sub setName(ByVal n_name As String)
            name = n_name
        End Sub
        Public Function getOp_service_id() As Integer
            Return op_service_id
        End Function
        Public Sub setOp_service_id(ByVal n_op_service_id As Integer)
            op_service_id = n_op_service_id
        End Sub
        Public Function getMin() As Double
            Return min
        End Function
        Public Sub setMin(ByVal n_min As Double)
            min = n_min
        End Sub
        Public Function isPrc() As Boolean
            Return prc
        End Function
        Public Sub setPrc(ByVal n_prc As Boolean)
            prc = n_prc
        End Sub
        Public Function getArr() As String
            Return arr
        End Function
        Public Sub setArr(ByVal n_arr As String)
            arr = n_arr
        End Sub
        Public Function getId() As Integer
            Return id
        End Function
        Public Sub setId(ByVal n_id As Integer)
            id = n_id
        End Sub
    End Class

    Public Class TarifArr
        Public summa As Double
        Public max As Double
        Public id As Integer
        Public prc As Boolean
        Public min As Double
        Public parent As Boolean
        Public date_begin As String
        Public date_end As String

        Public Function getSumma() As Double
            Return summa
        End Function
        Public Sub setSumma(ByVal n_summa As Double)
            summa = n_summa
        End Sub
        Public Function getMax() As Double
            Return max
        End Function
        Public Function isParent() As Boolean
            Return parent
        End Function
        Public Sub setParent(ByVal n_parent As Boolean)
            parent = n_parent
        End Sub
        Public Function getDate_begin() As String
            Return date_begin
        End Function
        Public Sub setDate_begin(ByVal n_date_begin As String)
            date_begin = n_date_begin
        End Sub
        Public Function getDate_end() As String
            Return date_end
        End Function
        Public Sub setDate_end(ByVal n_date_end As String)
            date_end = n_date_end
        End Sub
        Public Sub setMax(ByVal n_max As Double)
            max = n_max
        End Sub
        Public Function getId() As Integer
            Return id
        End Function
        Public Sub setId(ByVal n_id As Integer)
            id = n_id
        End Sub
        Public Function isPrc() As Boolean
            Return prc
        End Function
        Public Sub setPrc(ByVal n_prc As Boolean)
            prc = n_prc
        End Sub
        Public Function getMin() As Double
            Return min
        End Function
        Public Sub setMin(ByVal n_min As Double)
            min = n_min
        End Sub
    End Class

    Public Class TerminalParam
        Public address_ru As String
        Public address_en As String
        Public address_tj As String
        Public terminal_n As String
        Public call_center As String
        Public terminal_block As Integer 'Блокировать ли терминал, если пропала связь с сервером; 1 - блокировать, 0 - не блокировать.


        Public Function getAddress_rus() As String
            Return address_ru
        End Function
        Public Sub setAddress_rus(ByVal n_address As String)
            address_ru = n_address
        End Sub

        Public Function getAddress_eng() As String
            Return address_en
        End Function
        Public Sub setAddress_eng(ByVal n_address As String)
            address_en = n_address
        End Sub

        Public Function getAddress_taj() As String
            Return address_tj
        End Function
        Public Sub setAddress_taj(ByVal n_address As String)
            address_tj = n_address
        End Sub

        Public Function getTerminal_n() As String
            Return terminal_n
        End Function
        Public Sub setTerminal_n(ByVal n_terminal_n As String)
            terminal_n = n_terminal_n
        End Sub

        Public Function getCall_center() As String
            Return call_center
        End Function
        Public Sub setCall_center(ByVal n_call_center As String)
            call_center = n_call_center
        End Sub

        Public Function getBlockTerminal() As String
            Return terminal_block
        End Function
        Public Sub setBlockTerminal(ByVal n_block_terminal As String)
            terminal_block = n_block_terminal
        End Sub

    End Class

    Public Class InkasUser
        Public id As Integer
        Public login As String
        Public passw As String

        Public Function getPassw() As String
            Return passw
        End Function
        Public Sub setPassw(ByVal n_passw As String)
            passw = n_passw
        End Sub
        Public Function getId() As Integer
            Return id
        End Function
        Public Sub setId(ByVal n_id As Integer)
            id = n_id
        End Sub
        Public Function getLogin() As String
            Return login
        End Function
        Public Sub setLogin(ByVal n_login As String)
            login = n_login
        End Sub
    End Class

    Public Class Platej
        Public id As Long
        Public id_uslugi As Long
        Public nomer As String
        Public summa As Double
        Public summa_zachis As Double
        Public status As Long
        Public date_create As String
        Public date_send As String
        Public id_inkas As String
        Public val1 As Int16
        Public val3 As Int16
        Public val5 As Int16
        Public val10 As Int16
        Public val20 As Int16
        Public val50 As Int16
        Public val100 As Int16
        Public val200 As Int16
        Public val500 As Int16
        Public hesh_id As String
        Public chekn As String
        Public summa_komissia As Double

        Public Function getSumma_komissia() As Double
            Return summa_komissia
        End Function
        Public Sub setSumma_komissia(ByVal n_summa_komissia As Double)
            summa_komissia = n_summa_komissia
        End Sub
        Public Function getChekn() As String
            Return chekn
        End Function
        Public Sub setChekn(ByVal n_chekn As String)
            chekn = n_chekn
        End Sub
        Public Function getVal1() As Int16
            Return val1
        End Function
        Public Sub setVal1(ByVal n_val1 As Int16)
            val1 = n_val1
        End Sub
        Public Function getVal3() As Int16
            Return val3
        End Function
        Public Sub setVal3(ByVal n_val3 As Int16)
            val3 = n_val3
        End Sub
        Public Function getVal5() As Int16
            Return val5
        End Function
        Public Sub setVal5(ByVal n_val5 As Int16)
            val5 = n_val5
        End Sub
        Public Function getVal10() As Int16
            Return val10
        End Function
        Public Sub setVal10(ByVal n_val10 As Int16)
            val10 = n_val10
        End Sub
        Public Function getVal20() As Int16
            Return val20
        End Function
        Public Sub setVal20(ByVal n_val20 As Int16)
            val20 = n_val20
        End Sub
        Public Function getVal50() As Int16
            Return val50
        End Function
        Public Sub setVal50(ByVal n_val50 As Int16)
            val50 = n_val50
        End Sub
        Public Function getVal100() As Int16
            Return val100
        End Function
        Public Sub setVal100(ByVal n_val100 As Int16)
            val100 = n_val100
        End Sub
        Public Function getVal200() As Int16
            Return val200
        End Function
        Public Sub setVal200(ByVal n_val200 As Int16)
            val200 = n_val200
        End Sub
        Public Function getVal500() As Int16
            Return val500
        End Function
        Public Sub setVal500(ByVal n_val500 As Int16)
            val500 = n_val500
        End Sub
        Public Function getId() As Long
            Return id
        End Function
        Public Sub setId(ByVal n_id As Long)
            id = n_id
        End Sub
        Public Function getId_uslugi() As Long
            Return id_uslugi
        End Function
        Public Sub setId_uslugi(ByVal n_idUslugi As Long)
            id_uslugi = n_idUslugi
        End Sub
        Public Function getNomer() As String
            Return nomer
        End Function
        Public Sub setNomer(ByVal n_nomer As String)
            nomer = n_nomer
        End Sub
        Public Function getSumma() As Double
            Return summa
        End Function
        Public Sub setSumma(ByVal n_summa As Double)
            summa = n_summa
        End Sub
        Public Function getSumma_zachis() As Double
            Return summa_zachis
        End Function
        Public Sub setSumma_zachis(ByVal n_summaZachis As Double)
            summa_zachis = n_summaZachis
        End Sub
        Public Function getStatus() As Long
            Return status
        End Function
        Public Sub setStatus(ByVal n_status As Long)
            status = n_status
        End Sub
        Public Function getDate_create() As String
            Return date_create
        End Function
        Public Sub setDate_create(ByVal n_dateCreate As String)
            date_create = n_dateCreate
        End Sub
        Public Function getDate_send() As String
            Return date_send
        End Function
        Public Sub setDate_send(ByVal n_dateSend As String)
            date_send = n_dateSend
        End Sub
        Public Function getId_inkas() As String
            Return id_inkas
        End Function
        Public Sub setId_inkas(ByVal n_id_inkas As String)
            id_inkas = n_id_inkas
        End Sub
        Public Function getHesh_id() As String
            Return hesh_id
        End Function
        Public Sub setHesh_id(ByVal n_heshId As String)
            hesh_id = n_heshId
        End Sub
    End Class

    Public Class MsgInkassation

        Public passw As String
        Public user As String
        Public date_inkass As String
        Public summa As Double
        Public inkass_id As String

        Public Function getPassw() As String
            Return passw
        End Function

        Public Sub setPassw(ByVal n_passw As String)
            passw = n_passw
        End Sub

        Public Function getUser() As String
            Return user
        End Function

        Public Sub setUser(ByVal n_user As String)
            user = n_user
        End Sub

        Public Function getDate_inkass() As String
            Return date_inkass
        End Function

        Public Sub setDate_inkass(ByVal n_date_inkass As String)
            date_inkass = n_date_inkass
        End Sub

        Public Function getSumma() As Double
            Return summa
        End Function

        Public Sub setSumma(ByVal n_summa As Double)
            summa = n_summa
        End Sub

        Public Function getInkass_id() As String
            Return inkass_id
        End Function

        Public Sub setInkass_id(ByVal n_inkass_id As String)
            inkass_id = n_inkass_id
        End Sub

    End Class

    Public Class command
        Public id As Integer
        Public act As Integer
        Public status As Integer
        Public date_from As String
        Public date_to As String
        Public script As String

        Public Function getStatus() As Integer
            Return status
        End Function


        Public Sub setStatus(ByVal n_status As Integer)
            status = n_status
        End Sub

        Public Function getId() As Integer
            Return id
        End Function

        Public Sub setId(ByVal n_id As Integer)
            id = n_id
        End Sub

        Public Function getAct() As Integer
            Return act
        End Function

        Public Sub setAct(ByVal n_act As Integer)
            act = n_act
        End Sub

        Public Function getDateFrom() As String
            Return date_from
        End Function

        Public Sub setDateFrom(ByVal n_DateFrom As String)
            date_from = n_DateFrom
        End Sub

        Public Function getDateTo() As String
            Return date_to
        End Function

        Public Sub setDateTo(ByVal n_DateTo As String)
            date_to = n_DateTo
        End Sub

        Public Function getScript() As String
            Return script
        End Function

        Public Sub setScript(ByVal n_Script As String)
            script = n_Script
        End Sub
    End Class

End Module