﻿Public Class frmBlock
    Dim cmdNext As New PictureBox
    Dim OperMask As String
    Dim arrInfo(2) As String
    Dim arrLabel(2, 3) As String
    Public s1 As Double = 0
    Public s2 As Double = 0
    Public valuta As Integer()
    Public nomer As String

    Dim chekN As String = ""


    Private Sub frmBlock_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        pbLogo.BackgroundImage = Image.FromFile(Application.StartupPath & "/skins/logo.png")
        lblStatus.Text = StatusMessage(langInt)
    End Sub

    Private Sub getChek()
        Dim tmsr(16) As String
        Dim strPrint As String = ""
        Dim date_time As String = Date.Now

        strPrint = strPrint & vbCrLf & GlobalVars.Organisation
        strPrint = strPrint & vbCrLf & set_50("-", "-", "-")
        strPrint = strPrint & vbCrLf & set_50("ИНН", "510004229", " ")
        strPrint = strPrint & vbCrLf & set_50("Терминал №", GlobalVars.terminal_id, " ")
        strPrint = strPrint & vbCrLf & set_50("Адрес терминала: ", "", " ")
        strPrint = strPrint & vbCrLf & set_50("", GlobalVars.TerminalAddress(0), " ")
        strPrint = strPrint & vbCrLf & set_50("Дата платежа", date_time, " ")
        strPrint = strPrint & vbCrLf & set_50("Чек №", chekN, " ")
        strPrint = strPrint & vbCrLf & set_50("Сервис", service_name, " ")
        strPrint = strPrint & vbCrLf & set_50("Счет №", nomer, " ")
        strPrint = strPrint & vbCrLf & set_50("Принято", s1 & " Сомони", " ")
        strPrint = strPrint & vbCrLf & set_50("К зачислению", (s1 - s2).ToString & " Сомони", " ")
        strPrint = strPrint & vbCrLf & set_50("Коммисия", s2.ToString + " Сомони", " ")
        strPrint = strPrint & vbCrLf & set_50("Call-Center", GlobalVars.callcenter, " ")
        strPrint = strPrint & vbCrLf & set_50(" ", " ", " ")
        strPrint = strPrint & vbCrLf & set_50("Спасибо за платеж!", "", " ")

        Dim pr As New PCPrint(strPrint)
        pr.Print()
    End Sub

    Public Function set_50(ByVal str1 As String, ByVal str2 As String, ByVal str3 As String) As String
        Dim str As String
        Dim len2 As Integer = str2.length
        For i As Integer = str1.Length To 31 - len2
            str1 = str1 + str3
        Next
        str = str1 + str2
        Return str
    End Function

#Region "Построение интерфейса"


#End Region

#Region "Процедуры"

    '''''''''''''''''''Main
    Private Sub Main_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If group_id > 0 Then frmGroup.Close()
        frmService.Close()
        group_id = 0
        last_group_id = 0
        service_id = 0
        Me.Close()
    End Sub

    Private Sub Main_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main_off.png")
    End Sub

    Private Sub Main_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
    End Sub

    Private Sub Main_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
    End Sub

    ''''''''''''Check
    Private Sub Check_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        getChek()
    End Sub

    Private Sub Check_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_check_off.png")
    End Sub

    Private Sub Check_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_check.png")
    End Sub

    Private Sub Check_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_check.png")
    End Sub

#End Region

End Class