﻿Imports PrinterStatus

Public Class frmInkass
    Dim cmdMain As New PictureBox
    Dim cmdCheck As New PictureBox
    Dim cmdRefresh As New PictureBox
    Dim cmdInkass As New PictureBox
    Dim cmdIncash As New PictureBox

    Dim OperMask As String
    Dim arrInfo(2) As String
    Dim arrLabel(2, 3) As String
    Public s1 As Double = 0
    Public s2 As Double = 0
    Public valuta As Integer()
    Public nomer As String
    Public tmsr(36) As String
    Dim chekN As String = ""
    Public login As String = "2222"
    Public pass As String = "1111"

    Private Sub frmInkass_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown
        pbLogo.BackgroundImage = Image.FromFile(Application.StartupPath & "/skins/logo.png")

        tblGo.Controls.Clear()
        Dim tblIncashment As New TableLayoutPanel
        With tblIncashment
            .RowCount = 0
            .ColumnCount = 2
            .Dock = DockStyle.Fill
        End With
        tblGo.Controls.Add(tblIncashment, 0, 0)
        With cmdMain
            .SizeMode = PictureBoxSizeMode.AutoSize
            .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
            .Dock = DockStyle.Right
            .BackColor = Color.Transparent
            AddHandler .Click, AddressOf Main_Click
            AddHandler .MouseDown, AddressOf Main_MouseDown
            AddHandler .MouseUp, AddressOf Main_MouseUp
            AddHandler .MouseLeave, AddressOf Main_MouseLeave
        End With
        tblIncashment.Controls.Add(cmdMain, 0, 0)

        With cmdInkass
            .SizeMode = PictureBoxSizeMode.AutoSize
            .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_incash.png")
            .Dock = DockStyle.Left
            .BackColor = Color.Transparent

            '.Enabled = False
            AddHandler .Click, AddressOf Cash_Click
            AddHandler .MouseDown, AddressOf Cash_MouseDown
            AddHandler .MouseUp, AddressOf Cash_MouseUp
            AddHandler .MouseLeave, AddressOf Cash_MouseLeave
        End With
        tblIncashment.Controls.Add(cmdInkass, 1, 0)

        With cmdCheck
            .SizeMode = PictureBoxSizeMode.AutoSize
            .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_check.png")
            .Dock = DockStyle.Left
            .BackColor = Color.Transparent
            .Dock = DockStyle.Left
            '.Enabled = False
            AddHandler .Click, AddressOf Check_Click
            AddHandler .MouseDown, AddressOf Check_MouseDown
            AddHandler .MouseUp, AddressOf Check_MouseUp
            AddHandler .MouseLeave, AddressOf Check_MouseLeave
        End With
        tblGo.Controls.Add(cmdCheck, 1, 0)



        'With cmdRefresh
        '    .SizeMode = PictureBoxSizeMode.AutoSize
        '    .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_refresh.png")
        '    .Dock = DockStyle.Left
        '    .BackColor = Color.Transparent
        '    .Name = "cmdNext"
        '    '.Enabled = False
        '    AddHandler .Click, AddressOf Refresh_Click
        '    AddHandler .MouseDown, AddressOf Refresh_MouseDown
        '    AddHandler .MouseUp, AddressOf Refresh_MouseUp
        '    AddHandler .MouseLeave, AddressOf Refresh_MouseLeave
        'End With
        'tblGo.Controls.Add(cmdRefresh, 1, 0)
        lblStatus.Text = StatusMessage(langInt)
        get_history()
        'get_stat()
        GlobalVars.Terminal = 304
    End Sub

    Private Sub get_history()
        'FROM_UNIXTIME(date_inkass, '%d.%m.%Y %H:%i')
        Dim sql As String = _
            "SELECT " & _
                " id, " & _
                " inkass_id as `Номер инкассации`, " & _
                " FROM_UNIXTIME(date_inkass) as `Дата инкассации`, " & _
                " user as `Инкассатор`, " & _
                " if(`status`=0,'Нет','Да') as `Отправлено`, " & _
                " summa as `Сумма`, " & _
                " (val1 + val3 + val5 + val10 + val20 + val50 + val100 + val200 + val500) as `Кол. купюр` " & _
            "FROM " & _
                " inkasso " & _
            " ORDER BY `Номер инкассации` DESC"
        Dim dt As DataTable = GetTable(sql)
        grdIncashment.DataSource = dt
        With grdIncashment
            .Columns(0).Visible = False
            .Columns(1).Width = 190
            .Columns(2).MinimumWidth = 200
            .Columns(2).AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill
            .Columns(3).Width = 155
            .Columns(4).Width = 175
            .Columns(5).Width = 120
            .Columns(5).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
            .Columns(6).Width = 100
            .Columns(6).DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleRight
        End With
    End Sub

    'Private Sub getChek()
    '    Dim tmsr(16) As String
    '    Dim strPrint As String = ""
    '    Dim date_time As String = Date.Now

    '    strPrint = strPrint & vbCrLf & GlobalVars.Organisation
    '    strPrint = strPrint & vbCrLf & set_50("-", "-", "-")
    '    strPrint = strPrint & vbCrLf & set_50("ИНН", "510004229", " ")
    '    strPrint = strPrint & vbCrLf & set_50("Терминал №", GlobalVars.terminal_id, " ")
    '    strPrint = strPrint & vbCrLf & set_50("Адрес терминала: ", "", " ")
    '    strPrint = strPrint & vbCrLf & set_50("", GlobalVars.TerminalAddress, " ")
    '    strPrint = strPrint & vbCrLf & set_50("Дата платежа", date_time, " ")
    '    strPrint = strPrint & vbCrLf & set_50("Чек №", chekN, " ")
    '    strPrint = strPrint & vbCrLf & set_50("Сервис", service_name, " ")
    '    strPrint = strPrint & vbCrLf & set_50("Счет №", nomer, " ")
    '    strPrint = strPrint & vbCrLf & set_50("Принято", s1 & " Сомони", " ")
    '    strPrint = strPrint & vbCrLf & set_50("К зачислению", (s1 - s2).ToString & " Сомони", " ")
    '    strPrint = strPrint & vbCrLf & set_50("Коммисия", s2.ToString + " Сомони", " ")
    '    strPrint = strPrint & vbCrLf & set_50("Call-Center", GlobalVars.callcenter, " ")
    '    strPrint = strPrint & vbCrLf & set_50(" ", " ", " ")
    '    strPrint = strPrint & vbCrLf & set_50("Спасибо за платеж!", "", " ")

    '    Dim pr As New PCPrint(strPrint)
    '    pr.Print()
    'End Sub

    Public Function set_50(ByVal str1 As String, ByVal str2 As String, ByVal str3 As String) As String
        Dim str As String
        Dim len2 As Integer = str2.Length
        For i As Integer = str1.Length To 31 - len2
            str1 = str1 + str3
        Next
        str = str1 + str2
        Return str
    End Function

    Public Sub get_stat()
        Dim op As String = "~"
        Dim date_time As String = Date.Now
        Dim sv As String = get_summa(GlobalVars.inkass_n)
        Dim arrVal() As String = sv.Split(New [Char]() {"~"})

        Dim nps As String = get_nopaysum(GlobalVars.inkass_n)
        If arrVal(0) = "0" Then
            For i As Integer = 0 To arrVal.Length - 1
                arrVal(i) = 0
                nps = 0
            Next
            cmdIncash.Enabled = False
            'cmdCheck.Enabled = False
        Else
            cmdIncash.Enabled = True
            'cmdCheck.Enabled = True
        End If
        'lblCheck.Font.setFont(New Font("Courier New", Font.PLAIN, 13))
        tmsr(0) = GlobalVars.Organisation
        tmsr(1) = set_50("Терминал №", GlobalVars.terminal_id, ".")
        tmsr(2) = set_50("Адрес:", GlobalVars.TerminalAddress(0), ".")
        tmsr(3) = set_50("Дата время:", date_time, ".")
        tmsr(4) = set_50("Номер инкасации:", GlobalVars.inkass_n, ".")
        tmsr(5) = set_50("Инкассатор:", login, ".")
        tmsr(6) = set_50(" ", " ", " ")
        tmsr(7) = set_table_r("", "", "")
        tmsr(8) = set_table50("Купюра", "Количество", "Сумма")
        tmsr(9) = set_table_r("", "", "")
        tmsr(10) = set_table50("1 сомони", " " & arrVal(1), " " & CInt(arrVal(1)))
        tmsr(11) = set_table_r("", "", "")
        tmsr(12) = set_table50("3 сомони", " " & arrVal(2), " " & CInt(arrVal(2)) * 3)
        tmsr(13) = set_table_r("", "", "")
        tmsr(14) = set_table50("5 сомони", " " & arrVal(3), " " & CInt(arrVal(3)) * 5)
        tmsr(15) = set_table_r("", "", "")
        tmsr(16) = set_table50("10 сомони", " " & arrVal(4), " " & CInt(arrVal(4)) * 10)
        tmsr(17) = set_table_r("", "", "")
        tmsr(18) = set_table50("20 сомони", " " & arrVal(5), " " & CInt(arrVal(5)) * 20)
        tmsr(19) = set_table_r("", "", "")
        tmsr(20) = set_table50("50 сомони", " " & arrVal(6), " " & CInt(arrVal(6)) * 50)
        tmsr(21) = set_table_r("", "", "")
        tmsr(22) = set_table50("100 сомони", " " & arrVal(7), " " & CInt(arrVal(7)) * 100)
        tmsr(23) = set_table_r("", "", "")
        tmsr(24) = set_table50("200 сомони", " " & arrVal(8), " " & CInt(arrVal(8)) * 200)
        tmsr(25) = set_table_r("", "", "")
        tmsr(26) = set_table50("500 сомони", " " & arrVal(9), " " & CInt(arrVal(9)) * 500)
        tmsr(27) = set_table_r("", "", "")
        Dim os As Integer = CInt(arrVal(1)) + CInt(arrVal(2)) + CInt(arrVal(3)) + CInt(arrVal(4)) + CInt(arrVal(5)) + CInt(arrVal(6)) + CInt(arrVal(7)) + CInt(arrVal(8)) + CInt(arrVal(9))
        tmsr(28) = set_table50("Общ. сумма:", " " & os, " " & arrVal(0))
        tmsr(29) = set_table_r("", "", "")
        tmsr(30) = set_table50("Необ. сум:", " ", " " & nps)
        tmsr(31) = set_table_r("", "", "")
        tmsr(32) = set_50(" ", " ", " ")
        tmsr(33) = set_50("Подпись кассира:", "_________", " ")
        tmsr(34) = set_50(" ", " ", " ")
        tmsr(35) = set_50("Подпись техник-инкассатора:", "_________", " ")
        lblCheck2.Text = ""
        For i As Integer = 0 To tmsr.Length - 1
            lblCheck2.Text = lblCheck2.Text & tmsr(i) & vbCrLf
        Next
    End Sub

    Public Function set_table50(ByVal str1 As String, ByVal str2 As String, ByVal str3 As String) As String
        Dim str As String = ""
        str = "|" & str1
        For i As Integer = 1 To 11 - str1.Length()
            str = str & " "
        Next
        str = str & "|" & str2
        For i As Integer = 1 To 10 - str2.Length()
            str = str & " "
        Next
        str = str & "|" & str3
        For i As Integer = 1 To 11 - str3.Length()
            str = str & " "
        Next
        str = str & "|"
        Return str
    End Function

    Public Function set_table_r(ByVal str1 As String, ByVal str2 As String, ByVal str3 As String) As String
        Dim str As String = ""
        str = "+" & str1
        For i As Integer = 1 To 11 - str1.Length()
            str = str & "-"
        Next
        str = str & "+" & str2
        For i As Integer = 1 To 10 - str2.Length()
            str = str & "-"
        Next
        str = str & "+" & str3
        For i As Integer = 1 To 11 - str3.Length()
            str = str & "-"
        Next
        str = str & "+"
        Return str
    End Function

    Private Function get_summa(ByVal id_inkass As String) As String
        Dim str As String = ""

        Dim dt As DataTable = GetTable("select ifnull(sum(summa),0), ifnull(sum(val1),0),ifnull(sum(val3),0),ifnull(sum(val5),0),ifnull(sum(val10),0),ifnull(sum(val20),0),ifnull(sum(val50),0),ifnull(sum(val100),0),ifnull(sum(val200),0),ifnull(sum(val500),0) from plateji where id_inkass='" & id_inkass & "'")
        If dt.Rows.Count > 0 Then
            For i As Integer = 0 To 9
                str = str & dt.Rows(0).Item(i) & "~"
            Next
        End If
        Return str
    End Function

    Private Function get_nopaysum(ByVal id_inkass As String) As String
        Dim str As String = ""

        Dim dt As DataTable = GetTable("select ifnull(sum(summa),0) as sum from plateji where id_inkass='" + id_inkass + "' and status=0")
        If (dt.Rows.Count > 0) Then
            str = dt.Rows(0).Item("sum")
        End If
        Return str
    End Function

    Public Sub get_check(ByVal id As Integer)
        If id > 0 Then
            Dim dt As DataTable = GetTable("SELECT inkass_id ,  FROM_UNIXTIME(date_inkass) as data_incash, user,  summa,  val1, val3, val5, val10, val20, val50, val100, val200, val500 FROM  inkasso  WHERE id=" & id)
            If dt.Rows.Count = 0 Then Exit Sub

            'Dim op As String = "~"
            Dim date_time As String = dt.Rows(0).Item("data_incash").ToString
            'Dim sv As String '= get_summa(GlobalVars.inkass_n)
            'Dim d() As Byte = dt.Rows(0).Item("data_incash")
            Dim arrVal(9) As String
            For i As Integer = 0 To 9
                arrVal(i) = dt.Rows(0).Item(i + 3)
            Next
            '= sv.Split(New [Char]() {op})

            Dim nps As String = get_nopaysum(dt.Rows(0).Item("inkass_id"))
            If arrVal(0) = "0" Then
                For i As Integer = 0 To arrVal.Length - 1
                    arrVal(i) = 0
                    nps = 0
                Next
                'cmdInkass.Enabled = False
                cmdCheck.Enabled = False
            Else
                'cmdInkass.Enabled = True
                cmdCheck.Enabled = True
            End If
            'lblCheck.Font.setFont(New Font("Courier New", Font.PLAIN, 13))
            tmsr(0) = GlobalVars.Organisation
            tmsr(1) = set_50("Терминал №", GlobalVars.terminal_id, ".")
            tmsr(2) = set_50("Адрес:", GlobalVars.TerminalAddress(0), ".")
            tmsr(3) = set_50("Дата время:", date_time, ".")
            tmsr(4) = set_50("Номер инкасации:", dt.Rows(0).Item("inkass_id"), ".")
            tmsr(5) = set_50("Инкассатор:", dt.Rows(0).Item("user"), ".")
            tmsr(6) = set_50(" ", " ", " ")
            tmsr(7) = set_table_r("", "", "")
            tmsr(8) = set_table50("Купюра", "Количество", "Сумма")
            tmsr(9) = set_table_r("", "", "")
            tmsr(10) = set_table50("1 сомони", " " & arrVal(1), " " & CInt(arrVal(1)))
            tmsr(11) = set_table_r("", "", "")
            tmsr(12) = set_table50("3 сомони", " " & arrVal(2), " " & CInt(arrVal(2)) * 3)
            tmsr(13) = set_table_r("", "", "")
            tmsr(14) = set_table50("5 сомони", " " & arrVal(3), " " & CInt(arrVal(3)) * 5)
            tmsr(15) = set_table_r("", "", "")
            tmsr(16) = set_table50("10 сомони", " " & arrVal(4), " " & CInt(arrVal(4)) * 10)
            tmsr(17) = set_table_r("", "", "")
            tmsr(18) = set_table50("20 сомони", " " & arrVal(5), " " & CInt(arrVal(5)) * 20)
            tmsr(19) = set_table_r("", "", "")
            tmsr(20) = set_table50("50 сомони", " " & arrVal(6), " " & CInt(arrVal(6)) * 50)
            tmsr(21) = set_table_r("", "", "")
            tmsr(22) = set_table50("100 сомони", " " & arrVal(7), " " & CInt(arrVal(7)) * 100)
            tmsr(23) = set_table_r("", "", "")
            tmsr(24) = set_table50("200 сомони", " " & arrVal(8), " " & CInt(arrVal(8)) * 200)
            tmsr(25) = set_table_r("", "", "")
            tmsr(26) = set_table50("500 сомони", " " & arrVal(9), " " & CInt(arrVal(9)) * 500)
            tmsr(27) = set_table_r("", "", "")
            Dim os As Integer = CInt(arrVal(1)) + CInt(arrVal(2)) + CInt(arrVal(3)) + CInt(arrVal(4)) + CInt(arrVal(5)) + CInt(arrVal(6)) + CInt(arrVal(7)) + CInt(arrVal(8)) + CInt(arrVal(9))
            tmsr(28) = set_table50("Общ. сумма:", " " & os, " " & arrVal(0))
            tmsr(29) = set_table_r("", "", "")
            tmsr(30) = set_table50("Необ. сум:", " ", " " & nps)
            tmsr(31) = set_table_r("", "", "")
            tmsr(32) = set_50(" ", " ", " ")
            tmsr(33) = set_50("Подпись кассира:", "_________", " ")
            tmsr(34) = set_50(" ", " ", " ")
            tmsr(35) = set_50("Подпись техник-инкассатора:", "_________", " ")
            lblCheck.Text = ""
            For i As Integer = 0 To tmsr.Length - 1
                lblCheck.Text = lblCheck.Text & tmsr(i) & vbCrLf
            Next
            cmdCheck.Enabled = True
        End If
    End Sub


#Region "Процедуры"

    '''''''''''''''''''Main
    Private Sub Main_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If group_id > 0 Then frmGroup.Close()
        frmService.Close()
        group_id = 0
        last_group_id = 0
        service_id = 0
        GlobalVars.Terminal = 301
        GlobalVars.getCashCode = True
        Me.Close()
    End Sub

    Private Sub Main_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main_off.png")
    End Sub

    Private Sub Main_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
    End Sub

    Private Sub Main_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_main.png")
    End Sub

    ''''''''''''Cash
    Private Sub Cash_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        grbIncashment.Visible = True
        cmdCheck.Enabled = False
        cmdInkass.Enabled = False
        cmdMain.Enabled = False
        grdIncashment.Enabled = False
        lblCheck2.Text = ""

        tblIncass.Controls.Clear()
        Dim cmdBack As New PictureBox
        With cmdBack
            .SizeMode = PictureBoxSizeMode.AutoSize
            .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_no.png")
            .Dock = DockStyle.Left
            .BackColor = Color.Transparent
            .Dock = DockStyle.Right
            '.Enabled = False
            AddHandler .Click, AddressOf Back_Click
            AddHandler .MouseDown, AddressOf Back_MouseDown
            AddHandler .MouseUp, AddressOf Back_MouseUp
            AddHandler .MouseLeave, AddressOf Back_MouseLeave
        End With
        tblIncass.Controls.Add(cmdBack, 0, 0)

        With cmdIncash
            .SizeMode = PictureBoxSizeMode.AutoSize
            .Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_yes.png")
            .Dock = DockStyle.Left
            .BackColor = Color.Transparent
            .Dock = DockStyle.Left
            .Tag = 2
            '.Enabled = False
            AddHandler .Click, AddressOf Yes_Click
            AddHandler .MouseDown, AddressOf Yes_MouseDown
            AddHandler .MouseUp, AddressOf Yes_MouseUp
            AddHandler .MouseLeave, AddressOf Yes_MouseLeave
        End With
        tblIncass.Controls.Add(cmdIncash, 1, 0)
        get_stat()
    End Sub

    Private Sub Yes_Click()

        Dim sv As String = get_summa(GlobalVars.inkass_n)
        Dim arrVal() As String = sv.Split(New [Char]() {"~"})

        Execute("insert into inkasso ( inkass_id, date_inkass, user, passw, summa, status,val1,val3,val5,val10,val20,val50,val100,val200,val500)" & _
                " values('" & GlobalVars.inkass_n & "',UNIX_TIMESTAMP(),'" & login & "','" & pass & "'," & arrVal(0) & ",0," & arrVal(1) & "," & arrVal(2) & "," & arrVal(3) & "," & arrVal(4) & "," & arrVal(5) & "," & arrVal(6) & "," & arrVal(7) & "," & arrVal(8) & "," & arrVal(9) & ")")
        GlobalVars.BillCount = 0 : GlobalVars.BillSumm = 0
        Execute("UPDATE settings SET value='" & GlobalVars.BillCount & "' WHERE variable='bill_count'")
        Execute("UPDATE settings SET value='" & GlobalVars.BillSumm & "' WHERE variable='bill_summ'")
        Execute("INSERT INTO perif_status (perif,state,date_change) values('cash_count','" & GlobalVars.BillCount & "',UNIX_TIMESTAMP())")
        Execute("INSERT INTO perif_status (perif,state,date_change) values('cash_summ','" & GlobalVars.BillSumm & "',UNIX_TIMESTAMP())")

        'Dim id_in As String = GlobalVars.inkass_id
        'Dim id_in_n As Integer = CInt(id_in) + 1

        Dim id_i As String = (CInt(GlobalVars.inkass_id) + 1).ToString()
        Dim co As Integer = Len(id_i.ToString)
        id_i = StrDup(4 - co, "0") & id_i.ToString
        GlobalVars.inkass_id = id_i


        Execute("UPDATE settings SET value='" & GlobalVars.inkass_id & "' WHERE variable='inkass_id'")
        GlobalVars.inkass_n = GlobalVars.terminal_id & GlobalVars.inkass_id
        Execute("UPDATE settings SET value='" & GlobalVars.inkass_n & "' WHERE variable='inkass_n'")


        get_history()
        grbIncashment.Visible = False
        cmdCheck.Enabled = True
        cmdInkass.Enabled = True
        cmdMain.Enabled = True
        grdIncashment.Enabled = True
    End Sub

    Private Sub Yes_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_yes_off.png")
    End Sub

    Private Sub Yes_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_yes.png")
    End Sub

    Private Sub Yes_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_yes.png")
    End Sub


    Private Sub Cash_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_incash_off.png")
    End Sub

    Private Sub Cash_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_incash.png")
    End Sub

    Private Sub Cash_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_incash.png")
    End Sub

    ''''''''''''Refresh
    Private Sub Refresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub Refresh_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_refresh_off.png")
    End Sub

    Private Sub Refresh_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_refresh.png")
    End Sub

    Private Sub Refresh_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_refresh.png")
    End Sub

    ''''''''''''Check
    Private Sub Check_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'getChek()

        Dim printer As CustomPrint
        printer = New CustomPrint

        Dim a As Integer

        'printer.setLeftMargin(50)
        'printer.setTopMargin(50)
        printer.setPrintFont(New Font("Courier", 8, FontStyle.Bold))
        a = printer.Print(tmsr) ' If there is no installed printers returns 210
        If (GlobalVars.stPrinter <> a.ToString) Then
            Dim sql1 As String = "INSERT INTO perif_status (perif,state,date_change,status) values('printer','" & a & "',UNIX_TIMESTAMP(),0)"
            Try
                Execute(sql1)
            Catch ex As Exception

            End Try
            GlobalVars.stPrinter = a
        End If

        If sender.tag = 2 Then
            'Dim pr As New PCPrint(lblCheck2.Text)
            'pr.Print()
        Else
            'Dim pr As New PCPrint(lblCheck.Text)
            'pr.Print()
            cmdCheck.Enabled = False
        End If
    End Sub

    Private Sub Check_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_check_off.png")
    End Sub

    Private Sub Check_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_check.png")
    End Sub

    Private Sub Check_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_check.png")
    End Sub

    ''''''''''''Back
    Private Sub Back_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        grbIncashment.Visible = False
        cmdCheck.Enabled = True
        cmdInkass.Enabled = True
        cmdMain.Enabled = True
        grdIncashment.Enabled = True
    End Sub

    Private Sub Back_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_no_off.png")
    End Sub

    Private Sub Back_MouseUp(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_no.png")
    End Sub

    Private Sub Back_MouseLeave(ByVal sender As Object, ByVal e As System.EventArgs)
        sender.Image = Image.FromFile(Application.StartupPath & "/skins/" & arrLang(langInt) & "/services/button_no.png")
    End Sub

    Private Sub grdIncashment_DataError(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewDataErrorEventArgs) Handles grdIncashment.DataError

    End Sub

    Private Sub grdIncashment_RowStateChanged(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewRowStateChangedEventArgs) Handles grdIncashment.RowStateChanged
        If e.StateChanged = DataGridViewElementStates.Selected Then
            get_check(e.Row.Cells(0).Value)
        End If
    End Sub

#End Region


End Class