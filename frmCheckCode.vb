﻿Public Class frmCheckCode
    Public maskDict As New Dictionary(Of String, frmService.StructOpservice)
    Private Sub frmCheckCode_Shown(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Shown
        Me.Controls.Clear() 'Очистим форму
        Me.Size = New Size(470, 190)
        Me.BackColor = Color.FromArgb(255, 255, 150)
        'Me.TransparencyKey = Me.BackColor
        Me.Opacity = 0.9 'Еффект прозрачности от (0,1)

        'Считываем все маски с глобального массива
        Dim lenMaskDict As Integer = maskDict.Count, i As Integer = 0
        Dim lbl() As Label
        ReDim lbl(lenMaskDict)

        Dim xPos As Integer = 40, yPos As Integer = 55
        Dim dstnHght As Integer = 30, dstnWdth As Integer = 80
        Dim lHght As Integer = 60, lWdth As Integer = 180

        Dim lblMain As New Label
        With lblMain
            .Text = SelectCodeMessage(langInt)
            .BackColor = Color.FromArgb(255, 255, 150)
            .ForeColor = Color.Navy
            .Font = New Font("Sans Serif", 30)
            '.Size = New Size(418, 46)
            .AutoSize = True
            .Location = New System.Drawing.Point(40, 9)
        End With
        Me.Controls.Add(lblMain)

        For Each mask In maskDict
            'Маски выводим в две колонки
            Dim numMask = mask.Key.Substring(0, mask.Key.IndexOf("*") - 1)
            lbl(i) = New Label
            With lbl(i)
                .Text = numMask
                .BackColor = Color.Navy
                .ForeColor = Color.White
                .Font = New Font("Sans Serif", 30)
                .Tag = mask.Key
                .TextAlign = ContentAlignment.MiddleCenter

                .Size = New Size(lWdth, lHght)
                .Location = New System.Drawing.Point(xPos, yPos)
                AddHandler .Click, AddressOf Lbl_Click
            End With
            Me.Controls.Add(lbl(i))
            i = i + 1
            If i Mod 2 = 1 Then
                xPos = xPos + lWdth + dstnWdth
            Else
                'Переход обратно на первую колонку
                yPos = yPos + lHght + dstnHght
                xPos = 40
            End If
        Next
        Me.Height += 20
        Me.Width += 40
    End Sub
    'Событие click на коде услуги
    Private Sub Lbl_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim tMask As String = sender.tag
        'frmService.lblNomer.Text = tMask
        frmService.txtNumber.Text = tMask
        frmService.msisdn = sender.text
        frmService.OperMask = frmService.getMaskFromString(tMask)
        frmService.selectedMask = tMask
        Me.Close()
    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick
        Me.Close()
    End Sub

End Class