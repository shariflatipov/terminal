﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmInkass
    Inherits System.Windows.Forms.Form

    'Форма переопределяет dispose для очистки списка компонентов.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Является обязательной для конструктора форм Windows Forms
    Private components As System.ComponentModel.IContainer

    'Примечание: следующая процедура является обязательной для конструктора форм Windows Forms
    'Для ее изменения используйте конструктор форм Windows Form.  
    'Не изменяйте ее в редакторе исходного кода.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.lblCheck = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.grdIncashment = New System.Windows.Forms.DataGridView()
        Me.lblStatus = New System.Windows.Forms.Label()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.pbLogo = New System.Windows.Forms.PictureBox()
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel()
        Me.tblGo = New System.Windows.Forms.TableLayoutPanel()
        Me.tblIncash = New System.Windows.Forms.TableLayoutPanel()
        Me.grbIncashment = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.lblCheck2 = New System.Windows.Forms.Label()
        Me.tblIncass = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.grdIncashment, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.tblGo.SuspendLayout()
        Me.grbIncashment.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Enabled = True
        Me.Timer1.Interval = 2000
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 2
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.0!))
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.lblCheck, 1, 0)
        Me.TableLayoutPanel6.Controls.Add(Me.GroupBox1, 0, 0)
        Me.TableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(3, 53)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 1
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 388.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(1182, 388)
        Me.TableLayoutPanel6.TabIndex = 3
        '
        'lblCheck
        '
        Me.lblCheck.AutoEllipsis = True
        Me.lblCheck.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblCheck.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblCheck.ForeColor = System.Drawing.Color.Indigo
        Me.lblCheck.Location = New System.Drawing.Point(889, 0)
        Me.lblCheck.Name = "lblCheck"
        Me.lblCheck.Size = New System.Drawing.Size(290, 388)
        Me.lblCheck.TabIndex = 0
        Me.lblCheck.Text = "Чек инкассации"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.grdIncashment)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Font = New System.Drawing.Font("Palatino Linotype", 20.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Indigo
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(880, 382)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "История инкассаций"
        '
        'grdIncashment
        '
        Me.grdIncashment.AllowUserToAddRows = False
        Me.grdIncashment.AllowUserToDeleteRows = False
        Me.grdIncashment.BackgroundColor = System.Drawing.Color.LightYellow
        Me.grdIncashment.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.grdIncashment.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grdIncashment.Location = New System.Drawing.Point(3, 40)
        Me.grdIncashment.MultiSelect = False
        Me.grdIncashment.Name = "grdIncashment"
        Me.grdIncashment.ReadOnly = True
        Me.grdIncashment.RowHeadersVisible = False
        Me.grdIncashment.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.LightYellow
        Me.grdIncashment.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.Color.Yellow
        Me.grdIncashment.RowTemplate.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Navy
        Me.grdIncashment.RowTemplate.Height = 40
        Me.grdIncashment.RowTemplate.ReadOnly = True
        Me.grdIncashment.RowTemplate.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.grdIncashment.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.grdIncashment.Size = New System.Drawing.Size(874, 339)
        Me.grdIncashment.TabIndex = 0
        '
        'lblStatus
        '
        Me.lblStatus.AutoSize = True
        Me.lblStatus.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblStatus.Font = New System.Drawing.Font("Palatino Linotype", 15.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblStatus.ForeColor = System.Drawing.Color.Indigo
        Me.lblStatus.Location = New System.Drawing.Point(3, 0)
        Me.lblStatus.Name = "lblStatus"
        Me.lblStatus.Size = New System.Drawing.Size(1182, 50)
        Me.lblStatus.TabIndex = 1
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.pbLogo, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 0, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 220.0!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1194, 780)
        Me.TableLayoutPanel2.TabIndex = 9
        '
        'pbLogo
        '
        Me.pbLogo.BackColor = System.Drawing.Color.Transparent
        Me.pbLogo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TableLayoutPanel2.SetColumnSpan(Me.pbLogo, 2)
        Me.pbLogo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pbLogo.Location = New System.Drawing.Point(3, 3)
        Me.pbLogo.Name = "pbLogo"
        Me.pbLogo.Size = New System.Drawing.Size(1188, 214)
        Me.pbLogo.TabIndex = 11
        Me.pbLogo.TabStop = False
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.lblStatus, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.TableLayoutPanel6, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.tblGo, 0, 2)
        Me.TableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(3, 223)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 3
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(1188, 554)
        Me.TableLayoutPanel3.TabIndex = 8
        '
        'tblGo
        '
        Me.tblGo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.tblGo.ColumnCount = 2
        Me.tblGo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75.0!))
        Me.tblGo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.0!))
        Me.tblGo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblGo.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tblGo.Controls.Add(Me.tblIncash, 0, 0)
        Me.tblGo.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblGo.Location = New System.Drawing.Point(3, 447)
        Me.tblGo.Name = "tblGo"
        Me.tblGo.RowCount = 1
        Me.tblGo.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblGo.Size = New System.Drawing.Size(1182, 104)
        Me.tblGo.TabIndex = 4
        '
        'tblIncash
        '
        Me.tblIncash.ColumnCount = 2
        Me.tblIncash.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblIncash.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblIncash.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblIncash.Location = New System.Drawing.Point(3, 3)
        Me.tblIncash.Name = "tblIncash"
        Me.tblIncash.RowCount = 1
        Me.tblIncash.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tblIncash.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 98.0!))
        Me.tblIncash.Size = New System.Drawing.Size(880, 98)
        Me.tblIncash.TabIndex = 0
        '
        'grbIncashment
        '
        Me.grbIncashment.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbIncashment.BackColor = System.Drawing.Color.Transparent
        Me.grbIncashment.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.grbIncashment.Controls.Add(Me.TableLayoutPanel1)
        Me.grbIncashment.Font = New System.Drawing.Font("Palatino Linotype", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbIncashment.ForeColor = System.Drawing.Color.Red
        Me.grbIncashment.Location = New System.Drawing.Point(264, 133)
        Me.grbIncashment.Name = "grbIncashment"
        Me.grbIncashment.Size = New System.Drawing.Size(705, 596)
        Me.grbIncashment.TabIndex = 1
        Me.grbIncashment.TabStop = False
        Me.grbIncashment.Text = "Выполнить инкассацию?"
        Me.grbIncashment.Visible = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 307.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lblCheck2, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.tblIncass, 0, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(3, 29)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 95.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(699, 564)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'lblCheck2
        '
        Me.lblCheck2.AutoEllipsis = True
        Me.lblCheck2.BackColor = System.Drawing.Color.Transparent
        Me.lblCheck2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.lblCheck2.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.lblCheck2.Font = New System.Drawing.Font("Courier New", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(204, Byte))
        Me.lblCheck2.ForeColor = System.Drawing.Color.Indigo
        Me.lblCheck2.Location = New System.Drawing.Point(199, 0)
        Me.lblCheck2.Name = "lblCheck2"
        Me.lblCheck2.Size = New System.Drawing.Size(301, 469)
        Me.lblCheck2.TabIndex = 1
        Me.lblCheck2.Text = "Чек инкассации"
        Me.lblCheck2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'tblIncass
        '
        Me.tblIncass.BackColor = System.Drawing.Color.Transparent
        Me.tblIncass.ColumnCount = 2
        Me.TableLayoutPanel1.SetColumnSpan(Me.tblIncass, 3)
        Me.tblIncass.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblIncass.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblIncass.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tblIncass.Location = New System.Drawing.Point(3, 472)
        Me.tblIncass.Name = "tblIncass"
        Me.tblIncass.RowCount = 1
        Me.tblIncass.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tblIncass.Size = New System.Drawing.Size(693, 89)
        Me.tblIncass.TabIndex = 2
        '
        'frmInkass
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1194, 780)
        Me.Controls.Add(Me.grbIncashment)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Name = "frmInkass"
        Me.Text = "frmInkass"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.grdIncashment, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        CType(Me.pbLogo, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.TableLayoutPanel3.PerformLayout()
        Me.tblGo.ResumeLayout(False)
        Me.grbIncashment.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblStatus As System.Windows.Forms.Label
    Friend WithEvents tblGo As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pbLogo As System.Windows.Forms.PictureBox
    Friend WithEvents lblCheck As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents grdIncashment As System.Windows.Forms.DataGridView
    Friend WithEvents tblIncash As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents grbIncashment As System.Windows.Forms.GroupBox
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblCheck2 As System.Windows.Forms.Label
    Friend WithEvents tblIncass As System.Windows.Forms.TableLayoutPanel
End Class
