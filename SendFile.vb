﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.Text
'Imports System.Drawing;
Imports System.Net
Imports System.Net.Security
Imports System.Security
Imports System.IO
Imports System.Collections.Specialized


Public Class SendFile
    Public Function HttpUploadFile(ByVal url As String, ByVal file As String, ByVal paramName As String, ByVal contentType As String, ByVal nvc As NameValueCollection, ByVal login As String, ByVal pass As String)
        Try
            Dim boundary As String = "---------------------------" & DateTime.Now.Ticks.ToString("x")
            Dim boundarybytes As Byte() = System.Text.Encoding.ASCII.GetBytes(vbCrLf & "--" & boundary & vbCrLf)
            '(HttpWebRequest)
            Dim wr As HttpWebRequest = WebRequest.Create(url)
            wr.ContentType = "multipart/form-data; boundary=" & boundary
            wr.Method = "POST"
            wr.KeepAlive = True
            wr.Credentials = GetCredential(url, login, pass)

            Dim rs As Stream = wr.GetRequestStream()

            Dim formdataTemplate As String = "Content-Disposition: form-data; name=""{0}""" & vbCrLf & vbCrLf & "{1}"

            For Each key In nvc.Keys
                rs.Write(boundarybytes, 0, boundarybytes.Length)
                Dim formitem As String = String.Format(formdataTemplate, key, nvc(key))
                Dim formitembytes As Byte() = System.Text.Encoding.UTF8.GetBytes(formitem)
                rs.Write(formitembytes, 0, formitembytes.Length)
            Next
            rs.Write(boundarybytes, 0, boundarybytes.Length)

            Dim headerTemplate As String = "Content-Disposition: form-data; name=""{0}""; filename=""{1}""" & vbCrLf & "Content-Type: {2}" & vbCrLf & vbCrLf
            Dim header As String = String.Format(headerTemplate, paramName, file, contentType)
            Dim headerbytes As Byte() = System.Text.Encoding.UTF8.GetBytes(header)
            rs.Write(headerbytes, 0, headerbytes.Length)

            Dim fileStream As FileStream = New FileStream(file, FileMode.Open, FileAccess.Read)
            Dim buffer(4096) As Byte
            Dim bytesRead As Integer = 0
            'While ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) <> 0)
            bytesRead = fileStream.Read(buffer, 0, buffer.Length)
            While (bytesRead > 0)
                rs.Write(buffer, 0, bytesRead)
                bytesRead = fileStream.Read(buffer, 0, buffer.Length)
            End While
            fileStream.Close()

            Dim trailer As Byte() = System.Text.Encoding.ASCII.GetBytes(vbCrLf & "--" & boundary & "--" & vbCrLf)
            rs.Write(trailer, 0, trailer.Length)
            rs.Close()

            Dim wresp As WebResponse = Nothing
            Try
                wresp = wr.GetResponse()
                Dim stream2 As Stream = wresp.GetResponseStream()
                Dim reader2 As StreamReader = New StreamReader(stream2)
                msg("На сервер отправлен архивный файл логов: " & file & "; Размером(Байт): " & FileLen(file))
                Return reader2.ReadToEnd()
            Catch ex As Exception
                If Not IsNothing(wresp) Then
                    wresp.Close()
                    wresp = Nothing
                End If
                msg("Ошибка при отправки файла: " & ex.Message)
                Return "0"
            Finally
                wr = Nothing
            End Try
        Catch ex As Exception
            msg("Ошибка в функции HttpUploadFile: " & ex.Message)
        End Try
    End Function

    Private Function GetCredential(ByVal url As String, ByVal login As String, ByVal pass As String) As CredentialCache
        ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3
        ServicePointManager.ServerCertificateValidationCallback = New RemoteCertificateValidationCallback(AddressOf AcceptAllCertifications)
        Dim CredentialCache As CredentialCache = New CredentialCache()
        CredentialCache.Add(New System.Uri(url), "Basic", New NetworkCredential(login, pass))
        Return CredentialCache
    End Function

    Public Function AcceptAllCertifications(ByVal sender As Object, ByVal certification As Cryptography.X509Certificates.X509Certificate, ByVal chain As Cryptography.X509Certificates.X509Chain, ByVal sslPolicyErrors As SslPolicyErrors) As Boolean
        Return True
    End Function

    Public Sub zip_folder(ByVal zfolder As String, ByVal zfile As String, ByVal pass As String)
        ' архивирование заданной папки (zfolder) в заданный файл (zfile)
        Dim proc As System.Diagnostics.Process = New System.Diagnostics.Process()
        proc.StartInfo.FileName = Application.StartupPath & "\7z.exe" 'Path.GetDirectoryName(Application.ExecutablePath) + "\\7z.exe"
        If (File.Exists(proc.StartInfo.FileName)) Then
            ' если строка заканчивается на символ "\" то добавляем "*.*"
            If (zfolder.Substring(zfolder.Length - 1) = "\") Then
                zfolder = zfolder & "*.*"
            End If
            Dim arg As String = "a -y -p" & pass & " " & Chr(34) & zfile & Chr(34) & " " & Chr(34) & zfolder & Chr(34)
            proc.StartInfo.Arguments = arg
            proc.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden
            proc.Start()
            proc.WaitForExit()
            msg("Создан архивный файл - " & zfile & ", для отправки на сервер.")
            'msg("zip_folder:  файл 7z создан ==" & arg)
        Else
            msg("Файл " & proc.StartInfo.FileName & " не найден")
        End If
    End Sub

    Public Function Main(ByVal args As String())
        Try
            Dim nvc As NameValueCollection = New NameValueCollection()
            nvc.Add("id", "TR")
            nvc.Add("btn-submit-photo", "Upload")
            HttpUploadFile("https://192.168.0.251:8443/api/file/", "D:\tarifs.sny", "file", "image/jpeg", nvc, "001009", "jdhY3uKs")
            Return True
        Catch ex As Exception
            msg("Ошибка отправки файла. " & ex.Message)
            Return False
        End Try
    End Function
End Class