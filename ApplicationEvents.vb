﻿Imports System.ServiceProcess

Namespace My


    ' Для MyApplication имеются следующие события:
    ' 
    ' Startup: возникает при запуске приложения перед созданием начальной формы.
    ' Shutdown: возникает после закрытия всех форм приложения. Это событие не происходит при прерывании работы приложения из-за ошибки.
    ' UnhandledException: возникает, если в приложение обнаруживает необработанное исключение.
    ' StartupNextInstance: возникает при запуске приложения, допускающего одновременное выполнение только одной копии, если это приложение уже активно. 
    ' NetworkAvailabilityChanged: возникает при изменении состояния подключения: при подключении или отключении.

    Partial Friend Class MyApplication
        Private Sub MyApplication_Shutdown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shutdown
            msg("Завершается приложение..." & e.ToString)
        End Sub

        Private Sub MyApplication_Startup(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs) Handles Me.Startup
            msg("Запуск терминала...")
            Try
                Dim myService As ServiceController = New ServiceController
                Dim srvResult As String
                myService.ServiceName = "MySQL"
                srvResult = myService.Status.ToString
                If srvResult.Equals("Stopped") Then
                    myService.Start()
                End If





            Catch ex As Exception
                msg(ex.ToString & " Error ocurred in: " & ex.TargetSite.Name)
            End Try
        End Sub

        Private Sub MyApplication_UnhandledException(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs) Handles Me.UnhandledException
            msg("Критическая ошибка: " & e.Exception.Message & "Error occured in: " & e.Exception.TargetSite.ToString())
            e.ExitApplication = False
        End Sub
    End Class
End Namespace

